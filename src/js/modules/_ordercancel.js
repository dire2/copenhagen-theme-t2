(function(Core){
	Core.register('module_ordercancel', function(sandbox){
		var $this = null,
			$modalDeferred = null, //상위 모듈에서 보낸 $.Deferred
			args = null,
			refundAmountComponent = null,
			checkboxAllCounter = 0,
			isCheckboxAll = false,
			isAbleCancel = false,
			isPartialOrderCancel = false,
			quantitySelectComponent = null,
			checkboxComponent = null,
			isToApp = false,
			isPaymentType = false,
			arrItemParentOrderItemId = [];


		var checkboxCalculator = function(isValue){
			if(isCheckboxAll){
				if(isValue){
					checkboxAllCounter++;
					if((checkboxComponent.length - 1) <= checkboxAllCounter){
						isCheckboxAll = false;
						Method.submitCalculator();
					}
				}else{
					checkboxAllCounter--;
					if(checkboxAllCounter <= 0){
						isCheckboxAll = false;
						Method.emptyRefundPayment();
					}
				}
			}else{
				if(isValue){
					checkboxAllCounter++;
				}else{
					checkboxAllCounter--;
				}
				Method.submitCalculator();
			}
		}

		var arrIndexOfs = function(key, arr){
			var arrIndex = [];
			for(var i=0; i<arr.length; i++){
				if(arr[i] === key){
					arrIndex.push(i);
				}
			}
			return arrIndex;
		}

		var checkboxAllValidateChk = function(){
			var isChecked = true;
			var $itemCheckbox = $this.find('.item-checkbox');
			for(var i=0; i < $itemCheckbox.length; i++){
				if(!$itemCheckbox.eq(i).find('input[type=checkbox]').prop('checked')){
					isChecked = false;
					break;
				}
			}
			return isChecked;
		}

		var Method = {
			moduleInit:function(){
				args = arguments[0];
				$this = $(this);

				//취소할 아이템
				var items = $this.find('.return-reason-item');
				var orderStatusFormData = sandbox.utils.getQueryParams($this.find('#order-status-form').serialize().replace(/\+/g, ' '));
				var arrOrderItemId = [];

				isToApp = (args['data-appcard-cancel']['isAppCard'] === 'true') ? true : false;

				//주문에드온이 있다면 항상 전체취소만 가능하고
				//product 에드온이 있다면 애드온인 상품은 상위 상품에 귀속되어 전체취소만 가능하다.
				var isOrderAddon = (function(){
					var isAddon = false;
					$this.find('.return-reason-item').each(function(i){
						arrOrderItemId.push($(this).attr('data-orderItemId'));
						arrItemParentOrderItemId.push($(this).attr('data-parentOrderItemId'));
						if($(this).attr('data-isAddon') === 'true'){
							isAddon = true;
						}
					});
					return isAddon;
				})();

				//결제수단이 적립금이면 전체취소만 가능
				isPaymentType = (orderStatusFormData.paymentType === 'CUSTOMER_CREDIT') ? true : false;

				isAbleCancel = (orderStatusFormData.isAble === 'true') ? true : false;
				isAblePartial = (orderStatusFormData.isAblePartial === 'true') ? true : false;
				isRefundAccount = (orderStatusFormData.isRefundAccount === 'true') ? true : false;

				refundAmountComponent = new Vue({
					el:'#refund-amount',
					data:{
						"isAbleCancel":isAbleCancel,
						"isRefundAccount":isRefundAccount,
						"isPaymentType":isPaymentType,
						"isAblePartial":(isOrderAddon || isToApp || isPaymentType) ? false : isAblePartial,
						"refundPayments":[{
							"paymentType":{"type":null,"friendlyType":null,"isFinalPayment":false},
							"orgPaymentAmount":{"amount":0,"currency":"KRW"},
							"paymentAmount":{"amount":0,"currency":"KRW"},
							"shippingAmount":{"amount":0,"currency":"KRW"},
							"taxAmount":{"amount":0,"currency":"KRW"},
							"totalAmount":{"amount":0,"currency":"KRW"}
						}],
						"refundAccountNeed":(isAbleCancel && isRefundAccount) ? true:false,
						"ableEntireVoid":(isAbleCancel && isAblePartial) ? true:false,
						"fulfillmentCharge":{"amount":0,"currency":"KRW"}
					},
					created:function(){
						this.$nextTick(function(){
							if(this.refundAccountNeed){
								$this.find('.refund-account-container').addClass('need-refund-account');
							}

							checkboxComponent = sandbox.getComponents('component_checkbox', {context:$this}, function(i){
								var INDEX = i;
								this.addEvent('change', function(val){
									var $that = $(this);
									var $quantityWrap = $that.closest('.return-reason-item').find('.quantity-wrap');
									if($(this).val() === 'all'){
										//체크박스 전체선택 / 해제
										isCheckboxAll = true;
										if(val){
											$this.find('.item-checkbox').each(function(){
												if(!$(this).hasClass('checked')){
													$(this).find('> label').trigger('click');
												}
											});
										}else{
											$this.find('.item-checkbox.checked > label').trigger('click');
										}
									}else{
										if(val){
											$quantityWrap.addClass('active');
										}else{
											$quantityWrap.removeClass('active');
										}
										$quantityWrap.find('input[type=hidden]').prop('disabled', !val);
										$quantityWrap.find('select[name=quantity]').prop('disabled', !val);
										$this.find('.all-checkbox').find('input[type=checkbox]').prop('checked', checkboxAllValidateChk());

										//상품 사은품 check
										var orderItemId = $that.closest('.return-reason-item').attr('data-orderitemid');
										var maxQuantity = $that.closest('.return-reason-item').attr('data-item-quantity');
										var selectQuantity = $that.closest('.return-reason-item').find('select[name=quantity]').val();

										if(maxQuantity == 1 || maxQuantity == selectQuantity){
											Method.addOnCancel(val, orderItemId);
										}
										// arrIndexOfs(arrOrderItemId[INDEX - 1], arrItemParentOrderItemId).forEach(function(value, index, arr){
										// 	$this.find('.return-reason-item').eq(value).find('input[type=hidden]').prop('disabled', !val);
										// });
										checkboxCalculator(val);
									}
								});
							});

							quantitySelectComponent = sandbox.getComponents('component_select', {context:$this}, function(){
								this.addEvent('change', function(val, $selected){
									if($(this).attr('name') === 'refundBank'){
										console.log($selected.attr('data-bankcode-key'));
										$(this).closest('.select-box').find('input[name="refundBankCode"]').val($selected.attr('data-bankcode-key'));
									}else if($(this).attr('name') === 'reasonType'){
										console.log(val);
									}else{
										$(this).closest('.select-box').siblings().filter('input[name=quantity]').val(val);
										//수량 변경시 상품사은품 Check
										var allCancel = $(this).parents('li').attr('data-item-quantity') == val;
										var orderItemId = $(this).parents('.return-reason-item').attr('data-orderitemid');
										Method.addOnCancel(allCancel, orderItemId);
										Method.submitCalculator();
									}
								});
							});

							//isAbleCancel이 true이고, checkboxComponent가 undefined일떄 계산기를 실행한다.
							//ropis일때는 계산기 로직을 제외한다. 분명 나중에 다시 수정하게 됨.
							if(isAbleCancel && !checkboxComponent){
								Method.submitCalculator();
							}

							$this.find('[data-cancel-order]').click(function(e){
								e.preventDefault();
								if(!$(this).hasClass('disabled')){
									// Method.submitCancelOrder();
									// 앱카드 취소일경우 먼저 앱처리(단말기 카드취소) 후에 submitCancelOrder 한다.
									if(isToApp){
										Method.appCardCancel();
									}else{
										Method.submitCancelOrder();
									}
								}
							});
						});
					},
					watch:{
						refundAccountNeed:function(){
							/*if(this.refundAccountNeed === true){
								$this.find('.refund-account-container').addClass('need-refund-account');
							}else{
								$this.find('.refund-account-container').removeClass('need-refund-account');
							}*/
						}
					},
					methods:{
						isPartialVoid:function(groupCancellable, itemCancellabel, index, reverse){
							//isAbleCancel이 false이면 취소가 불가능한 주문이다.
							if(this.isAbleCancel){
								//isAblePartial이 false이면 무조건 전체취소만 가능하다.
								if(this.isAblePartial){
									//itemCancellabel이 false이면 전체취소만 가능하다.
									if(itemCancellabel){
										if(items.length <= 1 && items.attr('data-item-quantity') <= 1){
											return false;
										}else{
											return true;
										}
									}else{
										return false;
									}
								}else{
									return false;
								}

								if(arrItemParentOrderItemId[index-1]){
									if(reverse){
										return true;
									}else{
										return false;
									}
								}
							}else{
								if(reverse){
									return true;
								}else{
									return false;
								}
							}
						},
						isOrderPartialVoid:function(reverse){
							//isAbleCancel이 false일때는 취소불가능
							//isAbleCancel이 true 이고, isAblePartial이 true일때 부분취소가능
							//isAbleCancel이 true 이고, isAblePartial이 false일 전체취소만가능
							if(this.isAbleCancel){
								if(this.isAblePartial){
									if(items.length <= 1 && items.attr('data-item-quantity') <= 1){
										return false;
									}else{
										return true;
									}
								}else{
									return false;
								}
							}else{
								if(reverse){
									return true;
								}else{
									return false;
								}
							}
						},
						rtnCause:function(){
							if(this.isAbleCancel){
								if(this.isAblePartial){
									if(items.length <= 1 && items.attr('data-item-quantity') <= 1){
										return '하단의 취소 버튼을 클릭하시면 취소가 완료됩니다.';
									}else{
										return '주문을 취소하실 상품과 수량을 선택해 주세요.';
									}
								}else{
									if(this.isPaymentType) {
										return '마일리지를 사용한 주문으로 전체취소만 가능합니다. ';
									}
									return '해당 주문은 전체취소만 가능합니다.';
									//return decodeURIComponent(orderStatusFormData['restrict-partial']);
									console.log(decodeURIComponent(orderStatusFormData['restrict-partial']));
								}
							}else{
								return decodeURIComponent(orderStatusFormData['never-cause']);
							}
						},
						rtnPaymentType:function(paymentType){
							var label = '';
							switch(paymentType){
								case 'CUSTOMER_CREDIT' :
									label = '적립금';
									break;
								case 'GIFT_CARD' :
									label = '기프트카드';
									break;
								case 'CREDIT_CARD' :
									label = '신용카드';
									break;
								case 'MOBILE' :
									label = '휴대폰소액결제';
									break;
								case 'BANK_ACCOUNT' :
									label = '실시간계좌이체';
									break;
							}
							return label;
						},
						price:function(amount){
							return sandbox.utils.price(amount);
						}
					}
				});
			},
			addOnCancel:function(val, orderItemId){

				//에드온상품이 있으면, arrItemParentOrderItemId를 비교하여 같이 취소될수있도록 처리한다.
				var orderItemIDs = orderItemId;
				var parentorderitemid;
				var addonIndex = arrItemParentOrderItemId.indexOf(orderItemIDs);
				if(addonIndex > -1) {
					$(".return-reason-list").find('li.return-reason-item').each(function(){
						parentorderitemid = $(this).attr('data-parentorderitemid');
						if(parentorderitemid == orderItemId){
							$(this).find('input[type=hidden]').prop('disabled', !val);
							$(this).find('select').prop('disabled', !val);
						}
					});
				}
			},
			submitCalculator:function(){
				if(!args['data-module-ordercancel'].orderId){
					UIkit.notify('orderId가 없습니다.', {timeout:3000,pos:'top-center',status:'danger'});
					return;
				}

				var formData = $this.find('#cancel-items-form').serialize();
				var url = sandbox.utils.contextPath + '/account/order/partial-cancel-calculator/' + args['data-module-ordercancel'].orderId;
				var transFormData = sandbox.utils.getQueryParams(formData);
				var isFormDataValidateChk = (transFormData.hasOwnProperty('orderItemId') && transFormData.hasOwnProperty('quantity')) ? true:false;

				if(isAbleCancel && isFormDataValidateChk){
					sandbox.utils.ajax(url, 'POST', formData, function(data){
						var data = sandbox.rtnJson(data.responseText, true);
						var result = data['result'];
						if(data['result'] && data['ro']){
							refundAmountComponent.refundPayments = [];
							for(var i=0; i<data['ro']['refundPayments'].length; i++){
								if(data['ro']['refundPayments'][i].paymentType.type !== 'COD'){
									refundAmountComponent.refundPayments.push(data['ro']['refundPayments'][i]);
								}
							}

							//refundAmountComponent.refundAccountNeed = data['ro']['refundAccountNeed'];
							refundAmountComponent.ablePartialVoid = data['ro']['ablePartialVoid'];
							refundAmountComponent.fulfillmentCharge = data['ro']['fulfillmentCharge'];
							refundAmountComponent.ableEntireVoid = data['ro']['ableEntireVoid'];

							//cancelBtn enabled
							$this.find('[data-cancel-order]').removeClass('disabled');
						}else{
							$modalDeferred.reject(data['errorMsg']);
						}
					}, false, false, 100);
				}else{
					Method.emptyRefundPayment();
				}
			},
			emptyRefundPayment:function(){
				//계산할 금액 없음
				refundAmountComponent.refundPayments = [];

				//cancelBtn disabled
				$this.find('[data-cancel-order]').addClass('disabled');
			},
			refundAccountValidateChk:function(){
				//refundAccount validate check
				var $deferred = $.Deferred(),
					data = '';

				if(refundAmountComponent.refundAccountNeed){
					//validateChk
					var $form = $this.find('#refund-account-form');
					sandbox.validation.init( $form );
					sandbox.validation.validate( $form );

					if(sandbox.validation.isValid( $form )){
						$deferred.resolve($this.find('#refund-account-form').serialize());
					}else{
						$deferred.reject();
					}
				}else{
					$deferred.resolve();
				}

				return $deferred.promise();
			},
			submitCancelOrder:function(){
				if(!args['data-module-ordercancel'].orderId){
					UIkit.notify('orderId가 없습니다.', {timeout:3000,pos:'top-center',status:'danger'});
					return;
				}

				Method.refundAccountValidateChk().then(function(data){
					//cancel reason validateChk
					var defer = $.Deferred();
					var $form = $this.find('#cancel-reason-form');
					var currentData = sandbox.utils.getQueryParams(data, 'array');

					if($form.length > 0){
						sandbox.validation.init( $form );
						sandbox.validation.validate( $form );
						if(sandbox.validation.isValid( $form )){
							defer.resolve(sandbox.utils.getQueryParams($form.serialize(), 'array').concat(currentData).join('&'));
						}else{
							defer.reject();
						}
					}else{
						defer.resolve();
					}
					return defer.promise();
				}).then(function(data){
					//orderCancel Item validate check
					var defer = $.Deferred();
					var cancelItemsData = $this.find('#cancel-items-form').serialize();
					var transFormData = sandbox.utils.getQueryParams(cancelItemsData);
					var currentData = sandbox.utils.getQueryParams(data, 'array');
					var isFormDataValidateChk = (transFormData.hasOwnProperty('orderItemId') && transFormData.hasOwnProperty('quantity')) ? true:false;
					if(isAbleCancel && isFormDataValidateChk){
						defer.resolve(sandbox.utils.getQueryParams(cancelItemsData, 'array').concat(currentData).join('&'));
					}else{
						defer.reject('취소할 상품을 선택해주세요.');
					}
					return defer.promise();
				}).then(function(data){
					//submitCancelOrder confirm check
					var defer = $.Deferred();
					var message = (refundAmountComponent.ableEntireVoid) ? '취소 하시겠습니까?' : '선택한 상품을 취소하시겠습니까?';
					UIkit.modal.confirm(message, function(){
						defer.resolve(data);
					}, function(){
						defer.reject();
					},{
						labels: {'Ok': '확인', 'Cancel': '취소'}
					});
					return defer.promise();
				}).then(function(data){
					//submitCancelOrder async
					var orderCancelApi = (refundAmountComponent.ableEntireVoid) ? '/account/order/cancel/' : '/account/order/partial-cancel/';
					var url = sandbox.utils.contextPath + orderCancelApi + args['data-module-ordercancel'].orderId;
					return sandbox.utils.promise({
						url:url,
						method:'POST',
						data:data
					});
				}).then(function(data){
					//part of submitCancelOrder complate
					//var data = sandbox.rtnJson(data.responseText, true);
					var marketingType = '';
					//if(data !== false){
						if( data['result'] == true ){
							var marketingData = _GLOBAL.MARKETING_DATA();
							if( marketingData.useGa == true ){

								var cancelItemsData = $this.find('#cancel-items-form').serialize();
								var transFormData = sandbox.utils.getQueryParams(cancelItemsData);

								var itemList = [];
								var $orderList = $this.find('.item-checkbox.checked').parents('.return-reason-item');
								var isArrayChk = Array.isArray(transFormData.orderItemId);

								$.each( $orderList, function( index, data ){
									if(isArrayChk){
										if($(this).data("orderitemid") == transFormData.orderItemId[index]){
											itemList.push({
												model : $(this).find("[data-model]").data("model"),
												name : $(this).find("[data-name]").data("name"),
												quantity : transFormData.quantity[index],
												price : $(this).find("[data-price]").data("price")
											});
										}
									}else{
										if($(this).data("orderitemid") == transFormData.orderItemId){
											itemList.push({
												model : $(this).find("[data-model]").data("model"),
												name : $(this).find("[data-name]").data("name"),
												quantity : transFormData.quantity,
												price : $(this).find("[data-price]").data("price")
											});
										}
									}
								});

								//결제수단이 여러개일 경우를 대비한 취소금액 Sum작업
								var _refundPayments = refundAmountComponent.refundPayments.length;
								var resultTotalAmount = 0;
								for(var i=0; i < _refundPayments; i++){
									var amountSum = refundAmountComponent.refundPayments[i].totalAmount.amount - refundAmountComponent.refundPayments[i].shippingAmount.amount;
									resultTotalAmount = resultTotalAmount + amountSum;
								}

								gtag('event', 'refund', {
									"transaction_id": args['data-module-ordercancel'].orderId,
									"value": resultTotalAmount,
									"items": itemList
								});
							}
							if(!isToApp) $modalDeferred.resolve(data);
						}else{
							if(!isToApp) $modalDeferred.reject(data['errorMsg']);
						}
					// }else{
					// 	if(!isToApp) $modalDeferred.reject();
					// }
				}).fail(function(error){
					if(error) UIkit.modal.alert(error);
				});
			},
			appCardCancel:function(){
				/* assist admin에서 앱카드 당일 취소시 사용 */
				/*
					finpay app에 보내야하는값
				{
					aditInfo:"orderId=7352" //주문ID  orderId, cartId
					cardCashSe:'CARD',
					delngSe:"0", //0:취소, 1:승인
					instlmtMonth:"0", //할부개월수
					splpc:"119000", //결제금액, 공급가
					taxxpt:"0", //면세액
					uscMuf:"CTM0 + 7352", // orderId, cartId
					vat:"11900", //부과세 10%
					callbackScript:"Core.getModule('module_order_payment').callBackFdk", app callBackFunc
				}
				*/

				UIkit.modal.confirm('app cart 취소 하시겠습니까?', function(){
					var arrQueryParams = [
						'aditInfo=orderId%3D' + args['data-module-ordercancel'].orderId,
						'cardCashSe=CARD',
						'delngSe=0',
						'instlmtMonth=0',
						'splpc=' + args['data-appcard-cancel'].totalAmount,
						'taxxpt=0',
						'uscMuf=CTM0' + args['data-module-ordercancel'].orderId,
						'vat='+ parseInt(args['data-appcard-cancel'].totalAmount) * 0.1,
						'callbackScript=Core.getModule("module_ordercancel").callBackCancelFdk'
					];
					window.location.href = "seamless://pay=ok&mode=req&"+arrQueryParams.join('&');
				},{},{
					labels: {'Ok': '확인', 'Cancel': '취소'}
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-ordercancel]',
					attrName:['data-module-ordercancel', 'data-appcard-cancel'],
					moduleName:'module_ordercancel',
					handler:{context:this, method:Method.moduleInit}
				});
			},
			destroy:function(){
				$modalDeferred = null;
				console.log('destroy orderCancel module');
			},
			setDeferred:function(defer){
				$modalDeferred = defer;
			},
			callBackCancelFdk:function(resp){
				/* finpay 결제취소 콜백 */

				UIkit.modal.alert(resp);
				//Method.submitCancelOrder();
				//var respObj = Method.parsingResponseFdk(decodeURIComponent(resp));
				/*if(respObj.setleSuccesAt == "X"){
				 	alert(respObj.setleMssage);
				 	sandbox.setLoadingBarState(false);
					var cartId = Method.$that.find("input[name='cartId']").val();
					location.href = sandbox.utils.contextPath + '/checkout/request/'+ cartId;
				}else{
					var url = sandbox.utils.contextPath + Method.$that.find("input[name=payData]").data("m-redirect-url");
					var orderId = Method.$that.find('input[name="cartId"]').val();
					var params = "?" + resp + "&orderId=" + orderId;
					location.href = url + params;
				}*/
			}
		}
	});
})(Core);
