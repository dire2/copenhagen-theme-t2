(function(Core){
	Core.register('module_social_login', function(sandbox){
		var Method = {
			moduleInit:function(){
				$this = $(this);
				sandbox.getComponents('component_select', {context:$this}, function(){
					this.addEvent('change', function(val){
						Method.submitFormByName( val );
					});
				});

				$(this).find('[data-social-btn]').on('click', function(e){
					e.preventDefault();
					var type = $(this).data('social-btn');
					Method.submitFormByName( type );
				})


			},
			submitFormByName:function(name){
				var $form = $this.find('form[name="' + name + '"]');
				var url = sandbox.utils.url.getUri(sandbox.utils.url.getCurrentUrl());	
				var locationHref = window.location.href;
				var locationPathname = window.location.pathname;

				if (!_.isEmpty(url.queryParams.successUrl)){
					locationHref = url.queryParams.successUrl;
				} else if (locationPathname == '/login' || locationPathname == '/register' || locationPathname == '/login/forgotUsername' || locationPathname == '/login/forgotPassword') {
                    locationHref = url.url.replace(locationPathname, '');
                }
                if ( locationHref.indexOf( 'http' ) == -1){
                    locationHref = url.protocol + '//' + url.host + sandbox.utils.contextPath + locationHref;
                }
				if( $form ){
					$form.append('<input type="hidden" name="state" value="'+ locationHref.replace(/&/g, '%26') +'" />');
					$form.submit();
				}
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-social-login]',
					attrName:'data-module-social-login',
					moduleName:'module_social_login',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
