(function(Core){
	'use strict';

	Core.register('module_shipping_address_write', function(sandbox){
		var $this, args, endPoint;

		var Method = {
			moduleInit:function(){
				$this = $(this);
				args = arguments[0];
				endPoint = Core.getComponents('component_endpoint');

				var arrComponents = [];
				sandbox.getComponents('component_textfield', {context:$this}, function(){
					this.addEvent('focusout', function(){
						var value = $(this).val();
						if($(this).hasClass('fullName')){
							$this.find('#firstname').val(value);
							$this.find('#lastName').val(value);
						}
					});

					arrComponents.push(this);
				});
				sandbox.getComponents('component_searchfield', {context:$this, resultTemplate:'#address-find-list', isModify:args.isModify}, function(){
					this.addEvent('resultSelect', function(data){
						this.getInputComponent().setValue($(data).data('city') + ' ' + $(data).data('doro'));
						$this.find('#address1').val($(data).data('city') + ' ' + $(data).data('doro')); // 도로명 주소
						$this.find('#postcode').val($(data).data('zip-code5'));

						//상세주소 입력창으로 이동
						$this.find('#address2').focus();
					});

					arrComponents.push(this);
				});

				$this.find('button[type=submit]').off().on('click', function(e){
					e.preventDefault();

					var count = 0;
					$.each(arrComponents, function(i){
						if(!this.getValidateChk()){
							this.setErrorLabel();
						}else{
							count++;
						}
					});

					if(arrComponents.length === count){
						sandbox.setLoadingBarState(true);
						if( args.isModify == "true" ){
							endPoint.call('updateProfile', 'address book:edit shipping');
						}else{
							endPoint.call('updateProfile', 'address book:add shipping');
						}
						$this.find('form').submit();
					} 
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-shipping-address-write]',
					attrName:'data-module-shipping-address-write',
					moduleName:'module_shipping_address_write',
					handler:{context:this, method:Method.moduleInit}
				});
			},
			destroy:function(){
				$this = null;
				args = null;
			}
		}
	});
})(Core);
