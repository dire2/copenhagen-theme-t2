(function(Core){
	'use strict';

	Core.register('module_text_banner', function(sandbox){
		var Method = {
			moduleInit:function(){
				var $this = $(this);
				var $banner = $this.find('.slick-slider');

				var defaultOption = {
					autoplay : true,
					prevArrow : '<button type="button" class="icon-rotate-h display-small-up slick-prev slick-arrow" title="Go to Previous"><svg class="icon"><use xlink:href="#icon-arrow"></use></svg></button>',
          nextArrow : '<button type="button" class="display-small-up slick-next slick-arrow" title="Go to Next"><svg class="icon"><use xlink:href="#icon-arrow"></use></svg></button>',
				}


				$banner.slick(defaultOption);
				//$banner.addClass('slick-initialized');

				// $(this).find('.bxslider-controls .btn-next').on('click', function(e) {
				// 	e.preventDefault();
				// 	slider.goToNextSlide();
				// });
				// $(this).find('.bxslider-controls .btn-prev').on('click', function(e) {
				// 	e.preventDefault();
				// 	slider.goToPrevSlide();
				// });
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-text-banner]',
					attrName:'data-module-text-banner',
					moduleName:'module_text_banner',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
