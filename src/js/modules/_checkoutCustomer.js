(function(Core){
	Core.register('module_order_customer', function(sandbox){
		var args = {};
		var Method = {
			moduleInit:function(){
				var $this = $(this);
				args = arguments[0];
				sandbox.validation.init( $this.find('#order_info') );

				if( $this.find('input[name="isAlreadyRegistered"]').length > 0){
					UIkit.modal.confirm('이미 회원 가입된 아이디 입니다. 로그인 하시겠습니까?', function(){
						window.location.replace(sandbox.utils.contextPath + '/login?successUrl=/checkout');
					}, function(){},
					{
						labels: {'Ok': '로그인', 'Cancel': '비회원 주문'}
					});

				}
			}
		}


		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-order-customer]',
					attrName:'data-module-order-customer',
					moduleName:'module_order_customer',
					handler:{context:this, method:Method.moduleInit}
				});
			},
			getOrderCustomerInfo:function(){
				return {
					name:args.name,
					phoneNum:args.phoneNum,
					emailAddress:args.emailAddress
				}
			}
		}
	});
})(Core);
