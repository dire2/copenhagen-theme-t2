(function(Core){
	var Quantity = function(){
		'use strict';

		var $this, $input, $plusBtn, $minusBtn, $msg, currentQty = 1, maxLen = 1, args, minNum = 1;
		var pattern = /[^0-9]/g;
		var setting = {
			selector:'[data-component-quantity]',
			input:'.label',
			plusBtn:'.plus',
			minusBtn:'.minus',
			attrName:'data-component-quantity',
			msg:'.msg'
		}

		var Closure = function(){}
		Closure.prototype = {
			setting:function(){
				var opt = Array.prototype.slice.call(arguments).pop();
				$.extend(setting, opt);
				return this;
			},
			getQuantityByCheckLimit:function( quantity ){
				var maxQuantity = (args.maxQuantity != 'null') ? args.maxQuantity : 100; // 최대수량 100
				var minQuantity  = (args.maxQuantity != 'null') ? args.maxQuantity : 100; // 최대수량 100 parseInt(args.minNum || 1);
				if( quantity < minQuantity){
					quantity = minQuantity;
				}else if( quantity > maxQuantity ){
					quantity = maxQuantity
				}

				return quantity;
			},
			init:function(){
				var _self = this;
				args = arguments[0];

				$this = $(setting.selector);
				$input = $this.find(setting.input);
				$plusBtn = $this.find(setting.plusBtn);
				$minusBtn =  $this.find(setting.minusBtn);
				$msg = $this.find(setting.msg);
				maxLen = (args.maxQuantity != 'null') ? args.maxQuantity : 100; // 최대수량 100
				currentQty = $input.val();
				minNum = parseInt(args.minNum || 1);

				$plusBtn.on('click', function(e){
					e.preventDefault();

					currentQty++;

					$input.val(currentQty);
					$input.trigger('focusout');
				});

				$minusBtn.on('click', function(e){
					e.preventDefault();

					currentQty--;

					if(currentQty <= minNum) currentQty = minNum;
					$input.val(currentQty);
					$input.trigger('focusout');
				});

				$input.on({
					'keyup':function(e){
						var val = $input.val();
						if(pattern.test(val)){
							$input.val(val.replace(pattern, ''));
						}
					},
					'focusout':function(){
						currentQty = $(this).val();
						if(currentQty <= minNum) currentQty = minNum;

						if(parseInt(currentQty) > parseInt(maxLen)){
							$msg.text(maxLen + args.msg);
							currentQty = maxLen;
						}else{
							$msg.text('');
						}

						$(this).val(currentQty);
						_self.fireEvent('change', this, [currentQty]);
					}
				});

				return this;
			},
			getQuantity:function(){
				return currentQty;
			},
			setQuantity:function(quantity){
				var qty = this.getQuantityByCheckLimit(quantity);
				$input.val(qty);
				currentQty = qty;
			},
			setMaxQuantity:function(quantity){
				//console.log(quantity);
				if(args.maxQuantity == 'null' && quantity != null){
					maxLen = quantity;
				}else if(args.maxQuantity != 'null'){
					if(quantity != null){
						if(quantity < args.maxQuantity){
							maxLen = quantity;
						}else{
							maxLen = args.maxQuantity;
						}
					}else{
						maxLen = args.maxQuantity;
					}
				}else if(quantity == null){
					maxLen = 100;
				}

				if(quantity == 0){
					$msg.text(args.quantityStateMsg);
				}else{
					$msg.text('');
				}
			}
		}

		Core.Observer.applyObserver(Closure);
		return new Closure();
	}

	Core.Components['component_quantity'] = {
		constructor:Quantity,
		attrName:'data-component-quantity'
	}
})(Core);