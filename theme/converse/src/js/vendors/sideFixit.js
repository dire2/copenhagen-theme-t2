function fixitModule(){
    var Method = {
        $reszieEvent : 'orientationchange' in window ? 'orientationchange' : 'resize',
        $wrap : $('#wrapper'),
        $header : $('.header-general'),
        $container : null,
        $placeholder : null,
        $fixit : null,
        $containerClass : '.fixit-container',
        $placeholderClass : '.fixit-placeholder',
        $fixitClass : '.fixit-element',
        $active : 'fixit-active',
        $bottom : 'fixit-bottom',
        $docked : 'fixit-docked',
        $frozen : 'fixit-frozen',
        $scroll : 0,
        moduleInit : function(){
            Method.$container = $(Method.$containerClass);
            Method.$placeholder = Method.$container.find(Method.$placeholderClass);
            Method.$fixit = Method.$container.find(Method.$fixitClass);

            if (!Method.$container.length) return false;
            window.addEventListener('scroll', Method.fixitModule);
            window.addEventListener(Method.$reszieEvent, Method.fixitModule);
        },
        fixitDocked : function(w){
            Method.$fixit.css({
                'top': 'auto',
            }).removeClass(Method.$bottom).addClass(Method.$docked);
            Method.$fixit.width(w).addClass(Method.$active);
        },
        fixitClear : function(){
            Method.$placeholder.removeAttr('style');
            Method.$fixit.removeClass(Method.$bottom).removeClass(Method.$docked).removeClass(Method.$active).removeAttr('style');
        },
        fixitModule : function(){
            if ('m' != Core.getModule('module_gnb').deviceMedia()) {
                Method.$container.parent().removeAttr('style');
                var win = $(window),
                    wh = win.height(),
                    st = win.scrollTop(),
                    sb = st + wh,
                    ht = Method.$header.offset().top,
                    ch = Method.$container.outerHeight(),
                    ct = Method.$container.offset().top,
                    cb = ct + ch,
                    fw = Method.$fixit.width(),
                    fh = Method.$fixit.outerHeight(),
                    ft = ct - ht,
                    fb = ct + fh,
                    vh = wh - ft,
                    of = fh < vh,
                    ot = of ? ft : wh - fh,
                    isDown = Method.$scroll < st,
                    isDocked = cb < st + fh + ot,
                    isActive = (of || !isDown) ? (st > ht) : (sb > fb);
                    Method.$scroll = st;
                if (fh >= ch) {
                    Method.$container.parent().css('height', fh);
                } else if (isDocked) {
                    Method.$placeholder.css('height', fh);
                    Method.fixitDocked(fw);
                } else if (isActive) {
                    if (of) {
                        Method.$fixit.css({
                            'top': ft,
                        }).removeClass(Method.$docked).removeClass(Method.$active);
                        Method.$fixit.width(fw).addClass(Method.$active);
                    } else {
                        var active = Method.$fixit.hasClass(Method.$active),
                            frozen = Method.$fixit.hasClass(Method.$frozen),
                            docked = Method.$fixit.hasClass(Method.$docked),
                            bottom = Method.$fixit.hasClass(Method.$bottom);

                        if (isDown) {
                            if (!active && !frozen) {
                            Method.$fixit.css({
                                'top': 'auto',
                            }).addClass(Method.$bottom);
                            Method.$fixit.width(fw).addClass(Method.$active);
                            }
                            if (active && !bottom) {
                            Method.$fixit.css({
                                'top': st + ft - ct
                            }).removeClass(Method.$active).addClass(Method.$frozen);
                            }
                            if (frozen && sb > Method.$fixit.offset().top + fh) {
                            Method.$fixit.css({
                                'top': 'auto',
                            }).removeClass(Method.$frozen).addClass(Method.$bottom);
                            Method.$fixit.width(fw).addClass(Method.$active);
                            }
                        } else {
                            if (docked) {
                                if (cb - fh > st + ft) {
                                    Method.$fixit.css({
                                    'top': ft,
                                    }).removeClass(Method.$docked).removeClass(Method.$frozen).removeClass(Method.$active);
                                    Method.$fixit.width(fw).addClass(Method.$active);
                                }
                            }
                            if (bottom) {
                                Method.$fixit.css({
                                    'top': Method.$fixit.offset().top - ct
                                }).removeClass(Method.$active).removeClass(Method.$bottom).addClass(Method.$frozen);
                            }
                            if (frozen && st < Method.$fixit.offset().top - ft) {
                                Method.$fixit.css({
                                    'top': ft,
                                }).removeClass(Method.$frozen);
                                Method.$fixit.width(fw).addClass(Method.$active);
                            }
                        }
                    }
                } else {
                Method.fixitClear();
                }
            } else {
                Method.$container.parent().removeAttr('style');
                Method.$scroll = 0;
                Method.fixitClear();
            }
        }
    }
    Method.moduleInit();
    return {
        reInit : function(){
            Method.$container.parent().removeAttr('style');
            Method.$scroll = 0;
            Method.fixitClear();
            Method.fixitModule();
        }
    }
}

fixitModule();