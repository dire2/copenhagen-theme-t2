(function(Core){
	var WishListBtn = function(){
		'use strict';

		var $this, args, endPoint;
		var setting = {
			selectorWrap:'[data-component-wishlistbtn]'
		}

		var Closure = function(){}
		Closure.prototype = {
			setting:function(){
				var opt = Array.prototype.slice.call(arguments).pop();
				$.extend(setting, opt);
				return this;
			},
			init:function(){
				var _self = this;
				args = arguments[0];
				$this = $(setting.selector).find('.wish-action');

				/* wishlist */
				$this.click(function(e){
					e.preventDefault();

					var el = $(this),
						product = el.closest('.product-tile'),
						parent = product.find('.product-wish'),
						tileMain = product.find('.product-tile-main');

					var query = {
						productId:args.productId
					}
					Core.getModule('module_header').reDirect().setModalHide(true).setLogin(function(data){
						Core.Utils.ajax(args.api, 'GET', query, function(data){
							var jsonData = Core.Utils.strToJson(data.responseText, true) || {};
							if(jsonData.hasOwnProperty('error')){
								//console.log(jsonData);
								UIkit.notify(jsonData.error, {timeout:3000,pos:'top-center',status:'warning'});
							}else{
								if (parent.hasClass('set-changed')) return false;

								parent.addClass('set-changed');
								el.removeClass('set-active').siblings().addClass('set-active');
								//console.log(jsonData.isWishListChk);
								if(jsonData.isWishListChk){
									//console.log(args.addMsg);
									tileMain.addClass('set-wish-success');

									setTimeout(function(){
									  parent.removeClass('set-changed');
									  tileMain.removeClass('set-wish-success');
									  _self.addWish();
									},426);

				          			//UIkit.notify(args.addMsg, {timeout:3000,pos:'top-center',status:'success'});
								}else{
									setTimeout(function(){
										parent.removeClass('set-changed');
									},426);
									//console.log(args.removeMsg);
									//UIkit.notify(args.removeMsg, {timeout:3000,pos:'top-center',status:'warning'});
								}

								// if( _.isFunction( marketingAddWishList )){
								// 	marketingAddWishList();
								// }
							}
						}, null, true)
					})




					// if(args.hasOwnProperty('orderItemId')){
					// 	query['orderItemId'] = args.orderItemId;
					// }

					// Core.getModule('module_header').reDirect().setModalHide(true).setLogin(function(data){
					// 	Core.Utils.ajax(args.api, 'GET', query, function(data){
					// 		var jsonData = Core.Utils.strToJson(data.responseText, true) || {};
					// 		if(jsonData.hasOwnProperty('error')){
					// 			UIkit.notify(jsonData.error, {timeout:3000,pos:'top-center',status:'warning'});
					// 		}else{
					// 			if(jsonData.isWishListChk){
					// 				_self.find('i').addClass('icon-wishlist_full');
					// 				UIkit.notify(args.addMsg, {timeout:3000,pos:'top-center',status:'success'});
					// 				//endPoint.call('addToWishlist', query);
					// 			}else{
					// 				_self.find('i').removeClass('icon-wishlist_full');
					// 				//endPoint.call('removeToWishlist', query);
					// 				UIkit.notify(args.removeMsg, {timeout:3000,pos:'top-center',status:'warning'});
					// 			}
					//
					// 			if( _.isFunction( marketingAddWishList )){
					// 				marketingAddWishList();
					// 			}
					// 		}
					// 	});
					// });
				});

				return this;
			},
			addWish:function(){
				var wish = $('.header-favorites');
				wish.addClass('set-changed');
				setTimeout(function(){
					wish.removeClass('set-changed');
				},426);
			}
		}

		Core.Observer.applyObserver(Closure);
		return new Closure();
	}

	Core.Components['component_wishlistbtn'] = {
		constructor:WishListBtn,
		reInit:true,
		attrName:'data-component-wishlistbtn'
	}
})(Core);
