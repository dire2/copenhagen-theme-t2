(function(Core){
	var Toggle = function(){
		'use strict';

		var $this, $btn;
		var setting = {
			selector:'[data-component-toggle]',
			btn:'.toggle-label'
		}

		var Closure = function(){}
		Closure.prototype = {
			setting:function(){
				var opt = Array.prototype.slice.call(arguments).pop();
				$.extend(setting, opt);
				return this;
			},
			init:function(){
				var _self = this;
				$this = $(setting.selector).find('.toggle-label');
        		$this.on('click', function(){
					var el = $(this),
						box = el.closest('.toggle-box'),
						active = 'active',
						isSolo = box.data('component-toggle') == 'solo';
					if (box.hasClass(active)){
						box.removeClass(active);
						box.children('.toggle-label').removeClass(active);
						box.children('.toggle-content').slideUp(213, function(){
							$(this).removeClass(active).removeAttr('style');
						});
					} else {
						box.addClass(active);
						box.children('.toggle-label').addClass(active);
						box.children('.toggle-content').hide().slideDown(213, function(){
							$(this).addClass(active).removeAttr('style');
						});
						isSolo && box.siblings('.active').find('.toggle-label').trigger('click');
					}
        		});
				return this;
			}
		}

		Core.Observer.applyObserver(Closure);
		return new Closure();
	}

	Core.Components['component_toggle'] = {
		constructor:Toggle,
    reInit:true,
		attrName:'data-component-toggle'
	}
})(Core);
