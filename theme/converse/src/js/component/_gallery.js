(function(Core){
   var Gallery = function(){

      var $this, $thumbWrap, $galleryWrap, $zoomWrap, $zoomthumbWrap, args, currentIndex=0, arrThumbList=[], arrThumbListDetail=[], iScroll, sliderComponent, zoomSliderComponent, endPoint;
      var setting = {
         selector:'[data-component-gallery]',
         galleryModal:'#gallery-photo-swipe',
         galleryBtn:'[data-gallery-zoom]',
         thumbWrapper:'.product-thumb',
         zoomthumbWrap: '.zoom-product-gallery-thumb .zoom-product-thumb',
         galleryWrapper:'.product-gallery',
         zoomWrapper:'.pdp-gallery-fullview',
         zoomAppender:'.gallery-images',
         thumbContainer:'.thumb-wrap',
         thumbList:'.thumb-list',
         zoombtn:'.zoom-btn'
      }

      var galleryOptions = {
         history:false,
         focus:false,
         showAnimationDuration:0,
         hideAnimationDuration:0,
         index:0
      };


      var Closure = function(){}
      Closure.prototype.setting = function(){
         var opt = Array.prototype.slice.call(arguments).pop();
         $.extend(setting, opt);
         return this;
      }

      Closure.prototype.init = function(){
         var _self = this;

         args = arguments[0];
         $this = $(setting.selector);
         $thumbWrap = $this.find(setting.thumbWrapper);
         $galleryWrap = $this.find(setting.galleryWrapper);
         $zoomWrap = $this.find(setting.zoomWrapper);
         $zoomthumbWrap = $this.find(setting.zoomthumbWrap);
         endPoint = Core.getComponents('component_endpoint');

         var arrList = [];
         $thumbWrap.find('.thumb-list').each(function(i){
            var data = Core.Utils.strToJson($(this).attr('data-thumb'), true);
            var imgUrl = $(this).find('img').attr('src').replace(/\?[a-z]+/g, '');
            var pushIS = true;

            data.thumbUrl = imgUrl;

            /* 중복 이미지 처리 */
            for(var i=0; i < arrList.length; i++){
               if(arrList[i].thumbSort === data.thumbSort && arrList[i].thumbUrl === data.thumbUrl){
                  pushIS = false;
                  return;
               }
            }

            if(pushIS){
               arrList.push(data);
               arrThumbList.push(data);
               //상품상세 6개 갤러리
               arrThumbListDetail.push(data);
            }
         });

         $thumbWrap.on('click', 'li', function(e){
            e.preventDefault();

            var index = $(this).index();
            $(this).addClass('active').siblings().removeClass('active');
            sliderComponent.goToSlide(index);
            zoomSliderComponent.goToSlide(index);
            galleryOptions.index = index;
                endPoint.call('pdpImageClick', {index : index});
         });

         $galleryWrap.on('click', setting.zoombtn, function(){
            $('html').addClass('uk-modal-page');
            $('body').css('paddingRight', 15);
            $zoomWrap.addClass('show');
			zoomSliderComponent.reloadSlider(currentIndex);

         });

         $zoomWrap.find('.pdp-gallery-fullview-close').click(function(){
            if($('#quickview-wrap').length <= 0){
               $('html').removeClass('uk-modal-page');
               $('body').removeAttr('style');
            }
            $zoomWrap.removeClass('show');
         });

         this.setThumb(args.sort);
         return this;
      }

      Closure.prototype.setThumb = function(sort){
         var _self = this;
         var appendTxt = '';
         var count = 0;
         var count2 = 0;
         var sortType = sort || args.sort;
         var arrThumbData = arrThumbList.filter(function(item, index, array){
            if(item.thumbSort === sortType || item.thumbSort === 'null'){
               count++;
               return item;
            }
         });
         var detailArrThumbData = arrThumbListDetail.filter(function(item, index, array){
             if(item.thumbSort === sortType || item.thumbSort === 'null'){
                count2++;
                if(count2 < 7) {
                    return item;
                 }
             }
          });

         var thumbTemplate = Handlebars.compile($("#product-gallery-thumb").html())(arrThumbData);
         var zoomthumbTemplate = Handlebars.compile($("#product-gallery-thumb-fullzoom").html())(arrThumbData);

         var detailThumbTemplate = Handlebars.compile($("#product-gallery-thumb-detail").html())(detailArrThumbData);
         var galleryTemplate = Handlebars.compile($("#product-gallery-swipe").html())(arrThumbData);
         var zoomTemplate = Handlebars.compile($('#product-gallery-zoom').html())(arrThumbData);

         $thumbWrap.find('.thumb-list').eq(0).addClass('active');
         $("#detail_Thumb").empty().append(detailThumbTemplate);
         $galleryWrap.empty().append(galleryTemplate);
         $zoomWrap.find(setting.zoomAppender).empty().append(zoomTemplate);


         /*if(args.thumbType === 'left'){
            $thumbWrap.empty().append(thumbTemplate).css({
               'minHeight':'100%'
               //'height':($thumbWrap.find(setting.thumbList).eq(0).height() + parseInt(args.between)) * count
               //'height':(($thumbWrap.find(setting.thumbList).eq(0).height()>0 ? $thumbWrap.find(setting.thumbList).eq(0).height() : 83) + parseInt(args.between)) * count
            });
         }*/

         $thumbWrap.empty().append(thumbTemplate);
         $zoomthumbWrap.empty().append(zoomthumbTemplate);

         /*if(args.thumbType === 'bottom'){
            $thumbWrap.find(setting.thumbContainer).css({
               'width':($thumbWrap.find(setting.thumbList).eq(0).outerWidth(true) + parseInt(args.between)) * count
            });
         }*/

         if(sliderComponent) sliderComponent.destroySlider();
         sliderComponent = Core.getComponents('component_slider', {context:$this, selector:'.gallery-swipe'}, function(){
            this.addEvent('slideAfter', function($slideElement, oldIndex, newIndex){
         zoomSliderComponent.reloadSlider(newIndex);
         $thumbWrap.find('li').eq(newIndex).addClass('active').siblings().removeClass('active');
			   currentIndex = newIndex;
            });

            /*this.addEvent('onInit', function(){
               if(!Core.Utils.mobileChk){
                  iScroll = new IScroll($thumbWrap[0], {
                     scrollX:(args.thumbType === 'bottom') ? true : false,
                     scrollY:(args.thumbType === 'bottom') ? false : true
                  });
               }
            });*/
        });

		if(zoomSliderComponent) zoomSliderComponent.destroySlider();
		zoomSliderComponent = Core.getComponents('component_slider', {context:$this, selector:'.zoom-swipe'}, function(){
			this.addEvent('slideAfter', function($slideElement, oldIndex, newIndex){
			  sliderComponent.reloadSlider(newIndex);
        $thumbWrap.find('li').eq(newIndex).addClass('active').siblings().removeClass('active');
			   currentIndex = newIndex;
            });
		});
      }

      Core.Observer.applyObserver(Closure);
      return new Closure();
   }

   Core.Components['component_gallery'] = {
      constructor:Gallery,
      attrName:'data-component-gallery'
   }
})(Core);
