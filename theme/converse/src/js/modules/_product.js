(function(Core){
	Core.register('module_product', function(sandbox){
		var currentFirstOptValue = '';
		var currentQuantity = 1;
		var itemAttributes = '';
		var miniOptionIS = false;
		var objProductOption = {};
		var minOffsetTop = 30;
		var maxOffsetTop = 0;
		var args = null;
		var $this;
		var imgCurrentIndex;
		var categoryId = '';
		var categoryName = '';
		var productId = '';
		var skuId = '';
		var isQuickView = false;
		var isQuantity = true;
		var productOption;
		var quantity;
		var endPoint;
		var privateId;
		var currentSkuData;
		var pickupModal;
		var itemRequest;

		var quantityCheck = function(inventoryType, maxQty){
			var obj = {isQuantity:false, maxQty:0}
			if(inventoryType !== 'UNAVAILABLE'){
				if(inventoryType === 'CHECK_QUANTITY'){
					obj.isQuantity = (maxQty > 0) ? true : false;
					obj.maxQty = maxQty;
				}else if(inventoryType === 'ALWAYS_AVAILABLE'){
					obj.isQuantity = true;
					obj.maxQty = null;
				}
			}else{
				obj.isQuantity = false;
				obj.maxQty = 0;
			}

			return obj;
		}

		var defaultSkuSetup = function(productOptComponents){
			var skuData, quantityState;
			if(!productOptComponents) return;
			if(quantity){
				if(Array.isArray(productOptComponents)){
					$.each(productOptComponents, function(i){
						skuData = this.getDefaultSkuData()[0];
						quantityState = quantityCheck(skuData.inventoryType, skuData.quantity);
						quantity[i].setMaxQuantity(quantityState.maxQty);
						isQuantity = quantityState.isQuantity;
					});
				}else{
					skuData = productOptComponents.getDefaultSkuData()[0];
					quantityState = quantityCheck(skuData.inventoryType, skuData.quantity);
					quantity.setMaxQuantity(quantityState.maxQty);
					isQuantity = quantityState.isQuantity;
				}
			}
		}

		var Method = {
			moduleInit:function(){
				$this = $(this);
				args = arguments[0];
				categoryId = sandbox.utils.getQueryParams(location.href).categoryid;
				categoryName = args.categoryName;
				productId = args.productId;
				privateId = args.privateId;
				referCategoryId = args.categoryId;
				endPoint = Core.getComponents('component_endpoint');

				var $dim = $('[data-miniOptionDim]');
				var guideModal = UIkit.modal('#guide', {modal:false});
				var commonModal = UIkit.modal('#common-modal', {modal:false});
				var pickUpModal = UIkit.modal('#pickup-modal', {modal:false});
				var miniCartModule = sandbox.getModule('module_minicart');
				//var pickUpModal = UIkit.modal('#pickup-popup', {modal:false});
				var gallery = sandbox.getComponents('component_gallery', {context:$this});
				var $infoHeightWrap = $('[data-info-height]');


				var addonProductGroup = {};
				var addonComponents = sandbox.getComponents('component_addon_product_option', {context:$(document)}, function(i){
					var INDEX = i;
					var key = this.getAddonId();

					if(!addonProductGroup.hasOwnProperty(key)){
						addonProductGroup[key] = [];
						INDEX = i;
					}else{
						INDEX++;
					}
					addonProductGroup[key].push(this);

					this.addEvent('addToAddOnItem', function(privateId, $selected){
						for(var i=0; i<addonProductGroup[key].length; i++){
							if(i != INDEX){
								addonProductGroup[key][i].setTrigger($selected.val(), privateId);
							}
						}
					});

					this.addEvent('itemDelete', function(privateId){
						for(var i=0; i<addonProductGroup[key].length; i++){
							if(i != INDEX){
								addonProductGroup[key][i].removeItems();
							}
						}
					});
				});

				quantity = sandbox.getComponents('component_quantity', {context:$this}, function(i){
					var INDEX = i;

					this.addEvent('change', function(qty){
						for(var i=0;i<quantity.length;i++){
							if(i !== INDEX){
								quantity[i].setQuantity(qty);
							}
						}
					});
				});


				var currentOptValueId = '';
				productOption = sandbox.getComponents('component_product_option', {context:$(document)}, function(i){ //product Option select components
					var CURRENT_INDEX = i;
					var INDEX = 0;
					var _self = this;
					var key = this.getProductId();


					if(!objProductOption.hasOwnProperty(key)){
						objProductOption[key] = [];
						INDEX = 0;
					}else{
						INDEX++;
					}

					objProductOption[key].push(this);

					this.addEvent('changeFirstOpt', function(firstOptName, optionName, productId, value, valueId, id){
						if(currentOptValueId != valueId){
							currentOptValueId = valueId;

							for(var i=0; i<objProductOption[productId].length; i++){
								if(i != INDEX){
									skuId = '';
									objProductOption[productId][i].setTrigger(optionName, value, valueId);
								}

								if(optionName !== 'COLOR'){
									objProductOption[productId][i].getValidateChk();
								}
							}

							// if(optionName === 'COLOR'){
							// 	gallery.setThumb(value);
							// }
						}

						//console.log( "changeFirstOpt");
						if( _.isFunction(marketingUpdateProductInfo)){
							marketingUpdateProductInfo();
						}
					});

					this.addEvent('skuComplete', function(skuOpt){
						currentSkuData = skuOpt
						if(quantity){
							var quantityState = quantityCheck(skuOpt.inventoryType, skuOpt.maxQty);
							isQuantity = quantityState.isQuantity;
							skuId = skuOpt.id;

							if(args.isDefaultSku !== 'true'){
								if(Array.isArray(quantity)){
									quantity[CURRENT_INDEX].setQuantity(1);
									quantity[CURRENT_INDEX].setMaxQuantity(quantityState.maxQty);
								}else{
									quantity.setQuantity(1);
									quantity.setMaxQuantity(quantityState.maxQty);
								}
							}
						}

						//console.log( "skuComplete");
						if( _.isFunction(marketingUpdateProductInfo)){
							marketingUpdateProductInfo();
						}
					});
				});

				/* 상품평 탭이동 */
				$(".review_go").click(function(){
					var review_tabMenu = $(".detail-review_view");
					$('.detail-cont-inner').removeClass('show');
					review_tabMenu.trigger('click');

					$('html, body').animate({
							scrollTop: $($(this).attr('href')).offset().top
					}, 300);
				});

				/* 픽업 신청 버튼 클릭시 비로그인이면 로그인모달  */
				$(".pickup_btn").click(function(e){
					if(!Core.getModule('module_header').getIsSignIn()){
						e.preventDefault();
						var this_url = $(this).attr('href');
						Core.getModule('module_header').setLogin(function(){
							Core.Loading.show();
							_.delay(function(){
								window.location.assign(this_url);
							}, 500);
						});
					}
				});

				//매장픽업 바로구매시
				$('.reservation-btn').click(function(e){
				e.preventDefault();
					var productUrl = $(this).parents('.info-wrap_product').find('.title-wrap a').attr('href');
					Core.Utils.ajax(productUrl, 'GET', {reservationview:true}, function(data){
						var domObject = $(data.responseText).find('#reservation-wrap');
						$('#pickup-modal').find('.contents').empty().append(domObject[0].outerHTML);
						Core.moduleEventInjection(domObject[0].outerHTML);
						pickUpModal.show();
					});
				});

				/* isDefaultSku - true  ( option이 없는 경우 )  */
				if(args.isDefaultSku === 'true') defaultSkuSetup(productOption);

				/* cart Update */
				$('[data-add-item]').each(function(i){
					var INDEX = i;

					//매장픽업 바로구매시
					// $(this).parents('#post-choice-container').find('.reservation-btn').click(function(e){
					// 	e.preventDefault();
					// 	var optionChk = Method.moduleValidate(INDEX); //옵션 체크
					// 	var productUrl = $(this).parents('.info-wrap_product').find('.title-wrap a').attr('href');
					//
					// 	optionChk.then(function(){
					// 		Core.Utils.ajax(productUrl, 'GET', {reservationview:true}, function(data){
					// 			var domObject = $(data.responseText).find('#reservation-wrap');
					// 			$('#pickup-modal').find('.contents').empty().append(domObject[0].outerHTML);
					// 			Core.moduleEventInjection(domObject[0].outerHTML);
					// 			pickUpModal.show();
					// 		});
					// 	});
					//
					// });

					//매장픽업 제외 - 장바구니, 바로구매 등
					$(this).find('.btn-link').click(function(e){
						e.preventDefault();
						var _self = $(this);
						var addToCartPromise = Method.moduleValidate(INDEX);

						addToCartPromise.then(function(qty){
							var $form = _self.closest('form');
							itemRequest = BLC.serializeObject($form);
							itemRequest['productId'] = productId;
							if(referCategoryId!=null){
								itemRequest['categoryId'] = referCategoryId;
							}
							itemRequest['quantity'] = qty;

							/* 애드온 상품 추가 */
							var $deferred = $.Deferred();
							var addonProductIndex = 0;
							if(addonComponents){
								for(var key in addonProductGroup){
									if(addonProductGroup[key][0].getValidateChk()){
										var childItems = addonProductGroup[key][0].getChildAddToCartItems();
									    for(var i=0; i<childItems.length; i++){
									        for(var key in childItems[i]){
												itemRequest['childAddToCartItems['+addonProductIndex+'].'+key] = childItems[i][key];
									        }
									    }
										addonProductIndex++;
									}else{
										$deferred.reject();
									}
								}
								$deferred.resolve(itemRequest);
							}else{
								$deferred.resolve(itemRequest);
							}

							return $deferred.promise();
						}).then(function(itemRequest){
							var $form = _self.closest('form');
							var actionType = _self.attr('action-type');
							var url = _self.attr('href');

							/*****************************************************************
								유입 channel sessionStorage
								 - channel : 유입된 매체 식별 이름
								 - pid : 상품 식별 code ( productId, style Code, UPC.... )

								사이트 진입시 URL에 channel, pid 가 있을때 매출을 체크 한다.
								channel 만 있을경우에는 모든 상품을 channel 매출로 인지하고
								channel과 pid 둘다 있을경우 해당 상품만 channel 매출로 인지한다.
							*****************************************************************/

							if(sandbox.sessionHistory.getHistory('channel')){
								if(sandbox.sessionHistory.getHistory('pid')){
									if(sandbox.sessionHistory.getHistory('pid') === privateId){
										itemRequest['itemAttributes[channel]'] = sandbox.sessionHistory.getHistory('channel');
									}
								}else{
									itemRequest['itemAttributes[channel]'] = sandbox.sessionHistory.getHistory('channel');
								}
							}

							switch(actionType){
								case 'externalLink' :
									//외부링크
									window.location.href = url;
									break;
								default :
									BLC.ajax({
										url:url,
										type:"POST",
										dataType:"json",
										data:itemRequest
									}, function(data, extraData){
										if(commonModal.active) commonModal.hide();
										if(data.error){
											UIkit.modal.alert(data.error);
										}else{
											var cartData = $.extend( data, {productId : productId, quantity : itemRequest.quantity, skuId : skuId, categoryName : categoryName });
											if(actionType === 'add'){
												miniCartModule.update( function( callbackData ){
													if( callbackData != null ){
														cartData.cartId = callbackData.cartId
													}
													endPoint.call('addToCart', cartData );
												});
											}else if(actionType === 'modify'){
												var url = Core.Utils.url.removeParamFromURL( Core.Utils.url.getCurrentUrl(), $(this).attr('name') );
												Core.Loading.show();
												endPoint.call( 'cartAddQuantity', cartData );
												_.delay(function(){
													window.location.assign( url );
												}, 500);
											}else if(actionType === 'redirect'){
												Core.Loading.show();
												endPoint.call( 'buyNow', cartData );
												if (_GLOBAL.DEVICE.IS_KAKAO_INAPP && !_GLOBAL.CUSTOMER.ISSIGNIN){
													sandbox.getModule('module_kakao_in_app').submit('/checkout');
												}else{
													_.delay(function(){
														window.location.assign( sandbox.utils.contextPath + '/checkout' );
													}, 500);
												}
											}
										}
									});
									break;
							}
						}).fail(function(msg){
							if(!$("#common-modal").hasClass('quickview')){
								if(commonModal.active) commonModal.hide();
							}
							if(msg !== '' && msg !== undefined){
								UIkit.notify(msg, {timeout:3000,pos:'top-center',status:'warning'});
							}
						});
					});
				});

				//모바일일때 무조건 미니옵션보이게
				if(Core.Utils.mobileChk) {
					miniOptionIS = true;
					$('.mini-option-wrap').stop().animate({bottom:0}, 200);
				}

				//scrollController
				var scrollArea = sandbox.scrollController(window, document, function(percent){
					var maxOffsetTop = this.getScrollTop($('footer').offset().top);
					var maxHeight = this.setScrollPer(maxOffsetTop);

					//모바일 스크롤이 맨아래일때만 숨기기
					if(!Core.Utils.mobileChk) {
						if(percent < minOffsetTop && miniOptionIS){
							miniOptionIS = false;
							$('.mini-option-wrap').stop().animate({bottom:-81}, 200);
							$('.mini-option-wrap').find('.info-wrap_product').removeClass('active');
							$dim.removeClass('active');
						}else if(percent >= minOffsetTop && percent <= maxOffsetTop && !miniOptionIS){
							miniOptionIS = true;
							$('.mini-option-wrap').stop().animate({bottom:0}, 200);
						}else if(percent > maxOffsetTop && miniOptionIS){
							miniOptionIS = false;
							$('.mini-option-wrap').stop().animate({bottom:-81}, 200);
							$('.mini-option-wrap').find('.info-wrap_product').removeClass('active');
							$dim.removeClass('active');
						}
					}
				}, 'miniOption');

				$('.minioptbtn').click(function(e){
					e.preventDefault();
					$('.mini-option-wrap').find('.info-wrap_product').addClass('active');
					$dim.addClass('active');
				});

				$('.mini-option-wrap').on('click', '.close-btn', function(e){
					e.preventDefault();
					$('.mini-option-wrap').find('.info-wrap_product').removeClass('active');
					$dim.removeClass('active');
				});

				//입고알림
				$('.sms-service-btn').click(function(e){
					e.preventDefault();
					$('.restock-add-wrap').find('.info-wrap_product').addClass('active');
					$('[data-restockAddDim]').addClass('active');
					$(".sms-apply").show();
					$(".sms-complete").hide();
				});

				// 매장픽업 선택 시 에는 입고알림 숨김
				// page 로드시 매장픽업 기본선택에 따른 hide
				if($('.post-pickup').hasClass('uk-active')){
					$(".stock-wrap").hide();
				}else{
					//click event
					$(".post-choice li").click(function(){
						if($(this).hasClass('post-pickup')){
							$(".stock-wrap").hide()
						}else{
							$(".stock-wrap").show()
						}
					});
				}

				$('.restock-add-wrap').on('click', '.close-btn', function(e){
					e.preventDefault();
					$('.restock-add-wrap').find('.info-wrap_product').removeClass('active');
					$('[data-restockAddDim]').removeClass('active');
				});

				//guide option modal
				$this.find('.option-guide').on('click', function(e){
					e.preventDefault();
					guideModal.show();
				});


				$('.uk-quickview-close').click(function(e){
					guideModal.hide();
					isQuickView = true;
				});

				guideModal.off('.uk.modal.product').on({
					'hide.uk.modal.product':function(){
						if(isQuickView){
							setTimeout(function(){
								$('html').addClass('uk-modal-page');
								$('body').css('paddingRight',15);
							});
						}
					}
				});

				//연관상품 노출[상세,퀵뷰에서만]
				if($('div[data-module-product-detail],div[data-module-product]').closest('article').hasClass('contents')) {
					$('div[data-module-product-detail]').closest('article').find(".related").show();

					$('section').each(function(e){
						if($(this).hasClass("pt_category")) {
							$('div[data-module-product]').closest('article').find(".related").show();
						}
					});
				}
			},
			moduleValidate:function(index){
				var INDEX = index;
				var deferred = $.Deferred();
				var validateChk = (args.isDefaultSku === 'true') ? true : false;
				var qty = 0;

				if(args.isDefaultSku === 'false'){
					validateChk = sandbox.utils.getValidateChk(productOption, '사이즈를 선택해 주세요.');
					if('m' == Core.getModule('module_gnb').deviceMedia() && !validateChk){
						$('html, body').animate({
							scrollTop: $($(".product-information")).offset().top
						}, 300);
					}
				}

				if(Array.isArray(quantity)){
					qty = quantity[INDEX].getQuantity();
				}else{
					qty = quantity.getQuantity();
				}

				if(validateChk && isQuantity && qty != 0){
					deferred.resolve(qty);
					// $('.mini-option-wrap').find('.info-wrap_product').removeClass('active');
					// $('[data-miniOptionDim]').removeClass('active');
				}else if(!isQuantity || qty == 0){
					deferred.reject(args.errMsg);
				}else{
					deferred.reject();
				}

				return deferred.promise();
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-product]',
					attrName:'data-module-product',
					moduleName:'module_product',
					handler:{context:this, method:Method.moduleInit}
				});
				//console.log( "product init");
				if( _.isFunction(marketingUpdateProductInfo)){
					marketingUpdateProductInfo();
				}
			},
			destroy:function(){
				$this = null;
				args = [];
				productOption = null;
				quantity = null;
			},
			getItemRequest:function(){
				return itemRequest;
			},
			getSkuData:function(){
				return currentSkuData;
			}
		}
	});
})(Core);
