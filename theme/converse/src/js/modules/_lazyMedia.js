(function(Core){
	'use strict';
	Core.register('module_lazymedia', function(sandbox){

		var Method = {
            video : [],
            count : 0,
			moduleInit: function(){
                var $this = $(this),
                    $url = $this.data('moduleLazymedia'),
                    $type = this.tagName.toLowerCase(),
                    $count = $('img[data-module-lazymedia]').length;

                !$url.length && ($url = $this.attr('src'));

                if ($type == 'img') {
                    var src = $this.attr('src').replace('?browse','?gallery');
                    var dump = $('<img class="dump-lazy-media hidden" />').attr('src', src).appendTo('body');
                    dump.load(function(){
                        $this.replaceWith( $(this).removeClass('hidden').removeClass('dump-lazy-media') );
                        if (!$('.dump-lazy-media').length) {}
                    }).ready(function(){
                        Method.count++;
                        if ( Method.count == $count ) {
                            Method.video.forEach(function(video){
                                video.target.attr('src', video.src);
                                video.target.load();
                                video.target.on('play',function(){
                                    video.target.parent().addClass('pointer-none');
                                    video.target.parent().next().removeClass('hidden');
                                })
                                video.target[0].play();
                                if (video.target[0].paused) {
                                    video.target.parent().next().addClass('hidden');
                                    video.target.parent().removeClass('pointer-none');
                                }
                            });
                        }
                    });
                }
                if ($type == 'video') {
                    Method[$type].push({
                        target : $this,
                        src : $url
                    });
                }
            }
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-lazymedia]',
					attrName:'data-module-lazymedia',
					moduleName:'module_lazymedia',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
