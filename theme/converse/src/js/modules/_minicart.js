(function(Core){
	'use strict';
	Core.register('module_minicart', function(sandbox){
		var args;
		var endPoint;
		var Method = {
			$that:null,
			$closeBtn:null,

			moduleInit:function(){
				var $this = $(this);
				Method.$that = $this;
				args = arguments[0];
				endPoint = Core.getComponents('component_endpoint');

				$this.on('click', '[data-remove-item]',  function(e){
					e.preventDefault();
					var model = $(this).parents("[data-product-item]").find("input[name='model']").val();
					var name = $(this).parents("[data-product-item]").find("[data-name]").data("name");
					var category = $(this).parents("[data-product-item]").find("[data-category]").data("category");
					Method.removeItem( $(this).attr('href'), model, name, category);
				});

				$this.on('click', '[data-checkout-btn]', function(e){
					e.preventDefault();
					endPoint.call('checkoutSubmit',{});
					if (_GLOBAL.DEVICE.IS_KAKAO_INAPP && !_GLOBAL.CUSTOMER.ISSIGNIN) {
						sandbox.getModule('module_kakao_in_app').submit('/checkout');
					} else {
						location.href = $(this).attr('href');
					}
				});

				$this.on('click', '[data-toggle-close]', function(e){
					e.preventDefault();
					Method.hide();
				});

				$this.on('click', 'body', function(e){
					e.preventDefault();
					Method.hide();
				});



				// Method.$toggleClose.on('click', function(){
				// 	Method.hide();
				// });
			},
			show:function(){
				//UIkit.offcanvas arguments type : selector:string, option:object
				//UIkit.offcanvas.show('#minicart', {target:'#minicart', mode:'slide', overlay:'true'});
				setTimeout(function(){
					$('.minicart-dropdown').addClass('active');
				},213)
			},
			hide:function(){
				//uikit 사용으로 hide는 필요없는 상황
				//UIkit.offcanvas.hide('#minicart');
				$('.minicart-dropdown').removeClass('active');
			},

			update:function( callback ){
				var obj = {
					'mode':'template',
					'templatePath':'/cart/partials/miniCart',
					'resultVar':'cart',
					'cache':new Date().getTime()
				}

				sandbox.utils.ajax(sandbox.utils.contextPath + '/processor/execute/cart_state', 'GET', obj, function(data){
					//console.log( data );
					var isActive = Method.$that.find('.minicart-dropdown').hasClass('active');
					var miniCart = $(data.responseText);
					isActive && miniCart.addClass('active');
					Method.$that.empty().append(miniCart);

					var miniCartItem = $(args.miniCartCnt);
					var itemSize = $($(data.responseText)[1]).val();
					var cartId = $($(data.responseText)[2]).val();

					miniCartItem.attr('icon-text-attr', itemSize);

					if( itemSize == 0 ){
						miniCartItem.addClass('empty');
					}else{
						miniCartItem.removeClass('empty');
					}

					if( callback ){
						callback( { cartId : cartId} )
					}

					UIkit.modal('#quickview-modal').hide();
					
					Method.show();
				});
			},
			removeItem:function( url, model, name, category ){
				// error 체크와 ajax 로딩 처리 추가 되야 함
				UIkit.modal.confirm("상품을 삭제 할까요?", function(){
					sandbox.utils.ajax(url, 'GET', {}, function(data){
						var param = sandbox.utils.url.getQueryStringParams( url );
						param.model = model;
						param.name = name;
						param.category = category;
						endPoint.call( 'removeFromCart', param );
						Method.update();
					});
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-minicart]',
					attrName:'data-module-minicart',
					moduleName:'module_minicart',
					handler:{context:this, method:Method.moduleInit}
				});
			},
			show:Method.show,
			hide:Method.hide,
			update:Method.update
		}
	});
})(Core);
