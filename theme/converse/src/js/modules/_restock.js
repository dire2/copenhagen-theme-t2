(function(Core){
	'use strict';
	Core.register('module_restock', function(sandbox){
		var args;
		var endPoint;
		var isAllValidate = false;
		var isInputRadioVal = false;
		var isInputTextVal = false;
		var isInputCheckVal = false;
		var Method = {
			$that:null,
			$closeBtn:null,
			$modalPolicy : null,
			$restockClass : '.modal-restock',
			$agreePolicyClass : '.agree-checkbox',
			$policyClass : '.modal-restock-policy',

			moduleInit:function(){
				var $this = $(this);
				Method.$that = $this;
				Method.$modalRestock = $(Method.$restockClass);
				Method.$modalPolicy = $(Method.$policyClass);
				Method.$modalSuccess = UIkit.modal("#modal-request-restock-success",{modal: false, center: true});
				Method.$agreePolicy = $(Method.$agreePolicyClass).find('input');
				Method.restockPolicyPopup = UIkit.modal("#modal-restock-policy",{modal: false, center: true});

				args = arguments[0];
				endPoint = Core.getComponents('component_endpoint');

				if($this.find('.variation-size.nonActive').length == $this.find('.variation-size').length){
					//UIkit.modal.alert('입고 알림 신청이 가능한 사이즈가 없습니다.');
					$("#restockAdd").attr('disabled', true);
				}

				Method.$modalRestock.find(Method.$agreePolicyClass).off('click'+Method.$agreePolicyClass).on('click'+Method.$agreePolicyClass,function(){
					if (!Method.$agreePolicy.is(':checked')){
						Method.modalPolicyShow();
					} else {
						Method.$agreePolicy.prop('checked',false);
					}
					return false;
        		});
				$this.on('click', '#restockAdd',  function(e){
					e.preventDefault();
					if(Method.getValidateAllChk()){
						Method.add();
					}
				});
				$this.on('click', '#btn-restock-delete',  function(e){
					e.preventDefault();
					var alarm_id = $(this).siblings("input[id='hidden-restock-id']").val();
					if(typeof(alarm_id) != "undefined" && alarm_id != "" && isNaN(alarm_id) == false){
						UIkit.modal.confirm('삭제 하시겠습니까?', function(){
							Core.Utils.ajax(sandbox.utils.contextPath + '/restock/remove', 'GET',{'id':alarm_id}, function(data) {
								var data = $.parseJSON( data.responseText );
								if( data.result ){
									UIkit.modal.alert( "정상적으로 삭제 되었습니다." ).on('hide.uk.modal', function() {
										location.reload();
									});
								}else{
									UIkit.notify(data.errorMsg, {timeout:3000,pos:'top-center',status:'danger'});
								}
							},true);
						});
					}else{
						UIkit.modal.alert( "삭제 실패하였습니다. 짐시 후 다시 시도 해주세요." ).on('hide.uk.modal',null);
					}
				});
				$this.on('change', 'input',  function(e){
					Method.getValidateAllChk();
				});
				$this.on('keyup','#phone', function(e){
					Method.getValidateAllChk();
				});
			},
			modalPolicyShow : function(){
				Method.restockPolicyPopup.show();
				Method.$modalPolicy.on('click'+ Method.$policyClass, '.agree-policy-action', function(){
					Method.$agreePolicy.prop('checked',true);
					Method.restockPolicyPopup.find('.uk-close').trigger('click');
					Method.getValidateAllChk();
					return false;
				});
			},
			add:function(){
				var obj = {
					'id' : '',
					'skuId' : '',
					'messageType' : '',
					'target' : ''
				}

				obj.messageType = 'KAKAO';
				obj.skuId = $("#restockSkuId").val();
				obj.target = $("#phone").val();

				Core.Utils.ajax(sandbox.utils.contextPath + '/restock/add', 'GET',obj, function(data) {
					var data = $.parseJSON( data.responseText );
					if( data.result ){
						// $(".sms-apply").hide();
						// $(".sms-complete").show();
						//
						Method.$that.find('.uk-modal-close').trigger('click');
						Method.$modalSuccess.show();
					}else{
						UIkit.notify(data.errorMsg, {timeout:3000,pos:'top-center',status:'danger'});
					}
				},true);
			},
			getValidateChk:function(val){
				var isValidate = false;
				isValidate = $(val).parsley().validate();
				return isValidate;
			},
			getValidateAllChk:function(){
				var isColorCheck = false;
				var isSizeCheck = false;
				var isPhoneCheck = false;
				var isLicenseCheck = false;
				var regExp = /^01([0|1|6|7|8|9]{1})([0-9]{3,4})([0-9]{4})$/;
				var phone = $("input[name='phone']").val();

				//상품 색상 check
				// $(".request-restock-section").find("input[type='radio'][name='COLOR']").each(function(){
				// 	if($(this).attr("checked")){
				// 		isColorCheck = true;
				// 	}
				// });
				// if(!isColorCheck){
				// 	$(".restock-option-error-msg:first").addClass('active');
				// }else{
				// 	$(".restock-option-error-msg:first").removeClass('active');
				// }
				//상품 사이즈 check
				$(".request-restock-section").find("input[type='radio'][name='SIZE']").each(function(){
					if($(this).parents('label').hasClass("checked")){
						isSizeCheck = true;
					}
				});
				if(!isSizeCheck){
					$(".restock-option-error-msg").addClass('active');
				}else{
					$(".restock-option-error-msg").removeClass('active');
				}
				//휴대폰 번호 check
				if(phone != ""){
					var regText = /^[0-9]+$/.test(phone) ? '필수 입력 항목입니다.' : '숫자만 입력 가능합니다.';
					if(regExp.test(phone)){
						isPhoneCheck = true;
						$(".restock-sms-error-msg").removeClass('active');
					}else{
						$(".restock-sms-error-msg").text(regText).addClass('active');
					}
				}else{
					$(".restock-sms-error-msg").text(regText).addClass('active');
				}
				//개인정보처리방침 check
				if($("#chb-restock-agreement").prop("checked")){
					isLicenseCheck = true;
					$(".restock-agreement-error-msg").removeClass('active');
				}else{
					$(".restock-agreement-error-msg").addClass('active');
				}

				return (isSizeCheck && isPhoneCheck && isLicenseCheck);
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-restock]',
					attrName:'data-module-restock',
					moduleName:'module_restock',
					handler:{context:this, method:Method.moduleInit}
				});
			},
			show:Method.show,
			hide:Method.hide,
			update:Method.update
		}
	});
})(Core);
