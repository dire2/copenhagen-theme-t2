(function(Core){
	'use strict';
	Core.register('module_spatial', function(sandbox){

		var Method = {
			moduleInit: function(){
                var $this = $(this);
                $this.find('.spatial-tag').on('click',function(){
                    $this.toggleClass('toggle-active');
                })
            }
		}
		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-spatial]',
					attrName:'data-module-spatial',
					moduleName:'module_spatial',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
