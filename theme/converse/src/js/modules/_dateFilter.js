(function(Core){
	'use strict';

	Core.register('module_date_filter', function(sandbox){
		var Method = {
			$that:null,
			moduleInit:function(){
				var $this = $(this);
				Method.$that = $this;
				Method.$start = Method.$that.find('#start-date');
				Method.$end = Method.$that.find('#end-date');
				$this.find('[data-date-list] li a').on('click', function(){
					var value = $(this).data('date').split(',');
					if( value != '' ){
						Method.searchSubmit( moment().add(value[1], value[0]).add(1, 'd').format('YYYYMMDD'), moment().format('YYYYMMDD'), 'type'+$(this).parents('li').index() );
					}else{
						Method.searchSubmit();
					}
				});

				$this.find('[data-search-btn]').on('click', function(){
					if( Method.getValidateDateInput() ){
						var start = Method.$start.val().toString();
						var end = Method.$end.val().toString();

						//alert( start );
						//alert( moment(start, 'YYYYMMDD') );
						//alert( moment(start, 'YYYY.MM.DD').format('YYYYMMDD'));
						Method.searchSubmit( moment(start, 'YYYY.MM.DD').format('YYYYMMDD'), moment(end, 'YYYY.MM.DD').format('YYYYMMDD'), 'detail' );
					}else{
						UIkit.modal.alert( '기간을 선택해 주세요' );
					}
				});

				// 초기화
				$this.find('[data-reset-btn]').on('click', Method.reset);

				// uikit datepicker module 적용
				$this.find('input.date').each( function(){
					var dateinput = $(this);
					if (dateinput.val().length) {
						dateinput.val(moment(dateinput.val(), 'YYYYMMDD').format('YYYY.MM.DD')).change();
					}
					var datepicker = UIkit.datepicker(dateinput, {
						maxDate : true,
						format : 'YYYY.MM.DD'
					});
					switch (dateinput.attr('id')) {
						case Method.$start.attr('id') : Method.$start.ukdatepicker = datepicker;
						case Method.$end.attr('id') : Method.$end.ukdatepicker = datepicker;
					}

					//Method.datepicker.push(datepicker);
					datepicker.on( 'hide.uk.datepicker', function(){
						dateinput.trigger('focusout');
						Method.updateDateInput();
					})
				})
				Method.updateDateInput();
			},

			// 앞보다 뒤쪽 날짜가 더 뒤면 두값을 서로 변경
			updateDateInput:function(){
				var format = 'YYYY.MM.DD';
				var today = moment().format(format);
				var start = String(Method.$start.val());
				var end = String(Method.$end.val());
				
				if( $.trim(start).length && $.trim(end).length ){
					var dateterm = [start, end];
					dateterm = dateterm.sort();
					start = dateterm[0];
					end = dateterm[1];
					Method.$start.val( start );
					Method.$end.val( end );
				}
				if ($.trim(end).length) {
					Method.$start.ukdatepicker.options.minDate = moment(end).subtract(6, 'M').add(1, 'd').format(format);
					Method.$start.ukdatepicker.options.maxDate = end;
					Method.$start.ukdatepicker.update();
				}

				if ($.trim(start).length) {
					var after = moment(start).add(6, 'M').subtract(1, 'd').format(format);
					Method.$end.ukdatepicker.options.minDate = start;
					Method.$end.ukdatepicker.options.maxDate = [today, after].sort()[0];
					Method.$end.ukdatepicker.update();
				}
				// 같다면
				//var isSame = moment(Method.$start.val()).isSame(Method.$end.val());
				// 작다면
				//var isBefore = moment(Method.$start.val()).isBefore(Method.$end.val());
				// 크다면
				/*
				var isAfter = startDate.isAfter(endDate);

				if( isAfter ){
					var temp = Method.$end.val();
					Method.$end.val( Method.$start.val() );
					Method.$start.val( temp );
				}
				*/
			},
			getValidateDateInput:function(){
				var start = String(Method.$start.val());
				var end = String(Method.$end.val());

				if( moment( start, 'YYYY.MM.DD' ).isValid() && moment( end, 'YYYY.MM.DD' ).isValid() ){
					return true;
				}
				return false;
			},
			searchSubmit:function( start, end, type ){
				var url = sandbox.utils.url.getCurrentUrl();
				url = sandbox.utils.url.removeParamFromURL( url, 'dateType' );

				// 전체 검색
				if(_.isUndefined( start )){
					url = sandbox.utils.url.removeParamFromURL( url, 'stdDate' );
					url = sandbox.utils.url.removeParamFromURL( url, 'endDate' );
				}else{
					var opt = {
						stdDate : start,
						endDate : end,
						dateType : type
					}

					url = sandbox.utils.url.appendParamsToUrl( url, opt )
				}
				console.log(url);

				window.location.href = url;

			},
			reset:function(){
				//Method.$start.val('').trigger('focusout');
				//Method.$end.val('').trigger('focusout');
				window.location.href = location.pathname;
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-date-filter]',
					attrName:'data-module-date-filter',
					moduleName:'module_date_filter',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
