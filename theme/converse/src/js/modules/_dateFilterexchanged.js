(function(Core){
	'use strict';

	Core.register('module_exchange_filter', function(sandbox){
		var Method = {
			$that:null,
			moduleInit:function(){
				var $this = $(this);
				Method.$that = $this;
				Method.$start = Method.$that.find('#start-date');
				Method.$end = Method.$that.find('#end-date');

				$this.find('[data-date-list] a').on('click', function(){
					var value = $(this).data('date').split(',');
					if( value != '' ){
						Method.searchSubmit( moment().add(value[1], value[0]).add(1, 'd').format('YYYYMMDD'), moment().format('YYYYMMDD'), 'type'+$(this).index() );
					}else{
						Method.searchSubmit();
					}
				});

				$this.find('[data-search-btn]').on('click', function(){
					if( Method.getValidateDateInput() ){
						var start = Method.$start.val().toString();
						var end = Method.$end.val().toString();
						Method.searchSubmit( moment(start, 'YYYY.MM.DD').format('YYYYMMDD'), moment(end, 'YYYY.MM.DD').format('YYYYMMDD'), 'detail' );
					}else{
						UIkit.modal.alert( '기간을 선택해 주세요' );
					}
				});

				// 초기화
				$this.find('[data-reset-btn]').on('click', Method.reset);

				// uikit datepicker module 적용
				$this.find('input[class="date"]').each( function(){
					if( !moment($(this).val(), 'YYYY.MM.DD').isValid() ){
						$(this).val('');
					}
					if( $.trim( $(this).val() ) != ''){
						$(this).val( moment($(this).val(), 'YYYYMMDD').format('YYYY.MM.DD'));
					}
					var datepicker = UIkit.datepicker($(this), {
						maxDate : true,
						format : 'YYYY.MM.DD'
					});

					datepicker.on( 'hide.uk.datepicker', function(){
						$(this).trigger('focusout');
						Method.updateDateInput();
					})
				})
			},

			// 앞보다 뒤쪽 날짜가 더 뒤면 두값을 서로 변경
			updateDateInput:function(){
				var start = String(Method.$start.val());
				var end = String(Method.$end.val());

				if( $.trim( start ) == '' || $.trim( end ) == ''  ){
					return;
				}

				var isAfter = moment(start, 'YYYY.MM.DD').isAfter(moment(end, 'YYYY.MM.DD'));

				if( isAfter ){
					var temp = Method.$end.val();
					Method.$end.val( Method.$start.val() );
					Method.$start.val( temp );
				}
			},
			getValidateDateInput:function(){
				var start = String(Method.$start.val());
				var end = String(Method.$end.val());

				if( moment( start, 'YYYY.MM.DD' ).isValid() && moment( end, 'YYYY.MM.DD' ).isValid() ){
					return true;
				}
				return false;
			},
			searchSubmit:function( start, end, type ){
				var url = sandbox.utils.url.getCurrentUrl();
				url = sandbox.utils.url.removeParamFromURL( url, 'dateType' );

				// 전체 검색
				if(_.isUndefined( start )){
					url = sandbox.utils.url.removeParamFromURL( url, 'stdDate' );
					url = sandbox.utils.url.removeParamFromURL( url, 'endDate' );
				}else{
					var opt = {
						stdDate : start,
						endDate : end,
						dateType : type
					}

					url = sandbox.utils.url.appendParamsToUrl( url, opt )
				}

				window.location.href = url;

			},
			reset:function(){
				Method.$start.val('').trigger('focusout');
				Method.$end.val('').trigger('focusout');
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-exchange-filter]',
					attrName:'data-module-exchange-filter',
					moduleName:'module_exchange_filter',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
