(function(Core){
	'use strict';
	Core.register('module_review_write', function(sandbox){
		var $deferred = null;
		var imgCount = 0;
		var removeId = null;
		var maxCount = 5;
		var currentStarCount = 0;
		var starCountIS = false;
		var fileLoad = null;
		var arrDescription = ['별로에요.', '그저 그래요.', '나쁘지 않아요.', '마음에 들어요.', '좋아요! 추천합니다.'];
		var imgTemplate = '<span class="preview-up-img hidden"><img src="{{imgUrl}}" alt="{{imgAlt}}" /></span>';
		var inputHiddenTemplate = '<input type="hidden" name="fileList[{{id}}].fullUrl" class="file-{{id}}" value={{fullUrl}} /><input type="hidden" name="fileList[{{id}}].fileName" class="file-{{id}}" value={{fileName}} />';
		var Method = {
			$reszieEvent : 'orientationchange' in window ? 'orientationchange' : 'resize',
      $tiggerClass : '.reviews-modal-trigger',
      $modalClass : '.modal-reviews-submit',
      $progressClass : '.review-submit-progress',
      $stepWrapperClass : '.review-submit-steps',
      $stepClass : '.review-submit-step',
      $activeClass : '.active',
      $completeClass : '.complete',
      $active : 'active',
      $complete : 'complete',
      $stepCount : 0,
      $modal : null,
			moduleInit:function(){
				var $this = $(this);
				var $form = $this.find('#review-write-form');
				var $thumbNailWrap = $this.find('.thumbnail-wrap');
				var $textArea = sandbox.getComponents('component_textarea', {context:$this});

				Method.$modal = $(Method.$modalClass);
          Method.$stepCount = Method.$modal.find(Method.$stepClass).not('.step-intro').length;
          Method.$modal.on('click' + Method.$stepClass + Method.$complete, Method.$stepClass + Method.$completeClass, function(){
            var step = $(this);
            step.removeClass(Method.$complete).addClass(Method.$active).nextAll().removeClass(Method.$complete).removeClass(Method.$active);
            step.find('input').prop('checked',false);
            step.nextAll().find('input').prop('checked',false);
            Method.progress(step);
          });
          Method.startStep();
          Method.rateStep();
          Method.rangeStep();
          Method.mediaStep();
          Method.textAreaStep();

				/*  상품리뷰 rating default 상태 변경 */
				// if(marketingData.isProduct){
				// 	$this.find('.rating-star a').addClass('active');
				// };
				// $this.find('input[name=rating]').val(80);
				// $this.find('input[name=starCount]').val(4);
				// $this.find('.rating-description').text(arrDescription[4]);
				// starCountIS = true;
				// currentStarCount=4;
        Method.image = new Image();
        Method.image.addEventListener('load', function(e){
          Method.drawImage(document.querySelector('.review-submit-media-canvas').getContext("2d"), Method.image);
        },false);
				fileLoad = sandbox.getComponents('component_file', {context:$this}, function(){
					var _self = this;
					this.addEvent('error', function(msg){
						UIkit.notify(msg, {timeout:3000,pos:'top-center',status:'warning'});
					});

					this.addEvent('upload', function(fileName, fileUrl){
            Method.image.src = fileUrl;
            
						var step = $('.step-media');
            step.addClass('set-media-added');
            step.find('.step-title').addClass('set-collapsed');

						$thumbNailWrap.empty().append(sandbox.utils.replaceTemplate(imgTemplate, function(pattern){
							switch(pattern){
								case 'imgUrl' :
									return fileUrl;
									break;
								case 'imgAlt' :
									return fileName;
									break;
							}
						}));

						imgCount++;
						_self.setCurrentIndex(imgCount);
					});
        });

				$this.find('.rating-star a').click(function(e) {
					e.preventDefault();
					var index = $(this).index() + 1;
					$(this).parent().children('a').removeClass('active');
					$(this).addClass('active').prevAll('a').addClass('active');

					$this.find('input[name=rating]').val(index*20);
					$this.find('input[name=starCount]').val(index);
					$this.find('.rating-description').text(arrDescription[index-1]);

					starCountIS = true;
				});

				if(currentStarCount >= 0){
					$this.find('.rating-star a').eq(currentStarCount).trigger('click');
				}

        if ($thumbNailWrap.find('img').length) {
          Method.image.src = $thumbNailWrap.find('img').attr('src');
          var step = $('.step-media');
          step.addClass('set-media-added');
          step.find('.step-title').addClass('set-collapsed');
        }
        
        $this.find('button[type=submit]').click(function(e){
					e.preventDefault();

					$thumbNailWrap.children().each(function(i){
						var $this = $(this);
						$form.append(sandbox.utils.replaceTemplate(inputHiddenTemplate, function(pattern){
							switch(pattern){
								case 'id' :
									return i;
									break;
								case 'fileName' :
									return $this.find('img').attr('alt');
									break;
								case 'fullUrl' :
									return $this.find('img').attr('src');
									break;
							}
						}));
					});

					var itemRequest = BLC.serializeObject($form);
					sandbox.utils.ajax($form.attr('action'), $form.attr('method'), itemRequest, function(res){
						var data = sandbox.rtnJson(res.responseText);

						if(data.error || !data){
							if($deferred) $deferred.reject(data.error);
							else UIkit.notify(data.error, {timeout:3000,pos:'top-center',status:'danger'});
						}else{
							if($deferred) $deferred.resolve(data);
							else location.href = data.redirectUrl;
						}
					}, true);
        });
        
			},
      startStep : function(){
        var trigger = '.review-submit-start';
        Method.$modal.off('click'+trigger).on('click'+trigger, trigger, function(){
          var introStep = $(Method.$stepClass + '.step-intro'),
              next = introStep.next();
          introStep.removeClass(Method.$active).addClass('complete-hidden');
          next.addClass(Method.$active);
          Method.progress(next);
          $(Method.$modalClass).find('.uk-modal-dialog').addClass('step-progress-active');
          return false;
        });
      },
      rateStep : function(){
        var trigger = '.review-submit-rating-radio';
        Method.$modal.off('change'+trigger,trigger).on('change'+trigger,trigger,function(){
          var item = $(this).closest('.review-submit-rating-item');
					var item_value = item.find('input[name=review_submit_rating]').val();
          item.siblings().removeClass('set-checked');
          item.addClass('set-checked').prevAll().addClass('set-checked');
          item.closest(Method.$stepClass).addClass(Method.$complete);


					Method.$modal.find('input[name=rating]').val(item_value*20);
					Method.$modal.find('input[name=starCount]').val(item_value);

          setTimeout(function(){
            Method.nextStep();
          },426);
        });
      },
      rangeStep : function(){
        var trigger = '.review-submit-range-input';
        Method.$modal.off('change'+trigger).on('change'+trigger,trigger,function(){
          var item = $(this).closest('.review-submit-range-item');
          item.siblings().removeClass('set-checked');
          item.addClass('set-checked');
          item.closest('.review-submit-step-preview').find('.step-title').text(item.text());
          item.closest(Method.$stepClass).addClass(Method.$complete);
          setTimeout(function(){
            Method.nextStep();
          },426);
        });
      },
      mediaStep : function(){
        var triggerFile = '.review-submit-media-file';
        var triggerSkip = '.review-submit-sign-skip';
        var triggerNext = '.review-submit-sign-next';
        Method.$modal.off('click' + triggerSkip).on('click' + triggerSkip, triggerSkip, function(){
          $(this).closest(Method.$stepClass).addClass(Method.$complete);
          setTimeout(function(){
            Method.nextStep();
          },426);
        });
        Method.$modal.off('click' + triggerNext).on('click' + triggerNext, triggerNext, function(){
          $(this).closest(Method.$stepClass).addClass(Method.$complete);
          setTimeout(function(){
            Method.nextStep();
          },426);
        });
      },
      textAreaStep : function(){
        var triggerInput = '.review-submit-descr',
          trigger = '.review-submit-descr-trigger-container',
          triggerTarget = '.review-submit-descr-trigger-target',
          subtitleIntro = Method.$modal.find('.review-submit-descr-subtitle-intro'),
          buttonSubmit = Method.$modal.find('.button-submit');
        Method.$modal.off('keyup' + triggerInput).on('keyup' + triggerInput, triggerInput, function(){
          var input = $(this),
          data = input.data(),
          count = input.val().length;
          input.height(1).height( input.prop('scrollHeight') );
          Method.$modal.find(data.characterCounter).text(data.characterLimit - count);
          if (count){
            if (!input.parent(triggerTarget).length) {
              subtitleIntro.addClass('set-collapsed');
              buttonSubmit.removeClass('set-invisible');
            }
            setTimeout(function(){
              Method.$modal.find('.review-submit-descr-trigger-container').removeClass('set-invisible');
            },426);
          } else {
            if (!input.parent(triggerTarget).length) {
              subtitleIntro.removeClass('set-collapsed');
              buttonSubmit.addClass('set-invisible');
            }
          }
        });
        Method.$modal.off('keydown' + triggerInput).on('keydown' + triggerInput, triggerInput, function(){
          var input = $(this),
          data = input.data();
          input.height(1).height( input.prop('scrollHeight') );
        });
        Method.$modal.off('focusin' + triggerInput).on('focusin' + triggerInput, triggerInput, function(){
          var input = $(this),
          parent = input.parent(triggerTarget);
          if (parent.length) {
            parent.addClass('active');
            parent.prev().addClass('active');
          }
        });
        Method.$modal.off('focusout' + triggerInput).on('focusout' + triggerInput, triggerInput, function(){
          var input = $(this),
          parent = input.parent(triggerTarget);
          if (parent.length && !input.val().length) {
            parent.removeClass('active');
            parent.prev().removeClass('active');
          }
        });
        $(window).on(Method.$reszieEvent + triggerInput,function(){
          Method.$modal.find(triggerInput).trigger('keyup' + triggerInput);
        }).trigger(Method.$reszieEvent + triggerInput);
        Method.textAreaInit(triggerInput);
      },
      textAreaInit : function(triggerInput){
        Method.$modal.find(triggerInput).each(function(){
          var input = $(this),
            data = input.data(),
            count = input.val().length;
          input.trigger('focusin' + triggerInput).trigger('focusout' + triggerInput)
          Method.$modal.find(data.characterCounter).text(data.characterLimit - count);
        });
      },
      nextStep : function(){
        var before = $(Method.$stepClass + Method.$activeClass),
            next = before.next(),
            wrapper = $(Method.$stepWrapperClass);
        before.removeClass(Method.$active).addClass(Method.$complete);
        next.addClass(Method.$active);
        Method.progress(next);
        setTimeout(function(){
          wrapper.animate({'scrollTop': wrapper.prop('scrollHeight')},852);
        },426);
      },
      drawImage : function(ctx, img, x, y, w, h, offsetX, offsetY) {
        console.log(img);
        if (arguments.length === 2) {
            x = y = 0;
            w = ctx.canvas.width;
            h = ctx.canvas.height;
        }
        offsetX = typeof offsetX === "number" ? offsetX : 0.5;
        offsetY = typeof offsetY === "number" ? offsetY : 0.5;
        if (offsetX < 0) offsetX = 0;
        if (offsetY < 0) offsetY = 0;
        if (offsetX > 1) offsetX = 1;
        if (offsetY > 1) offsetY = 1;
        var iw = img.width,
            ih = img.height,
            r = Math.min(w / iw, h / ih),
            nw = iw * r,
            nh = ih * r,
            cx, cy, cw, ch, ar = 1;
        if (nw < w) ar = w / nw;
        if (Math.abs(ar - 1) < 1e-14 && nh < h) ar = h / nh;
        nw *= ar;
        nh *= ar;
        cw = iw / (nw / w);
        ch = ih / (nh / h);
        cx = (iw - cw) * offsetX;
        cy = (ih - ch) * offsetY;
        if (cx < 0) cx = 0;
        if (cy < 0) cy = 0;
        if (cw > iw) cw = iw;
        if (ch > ih) ch = ih;
        ctx.drawImage(img, cx, cy, cw, ch,  x, y, w, h);
      },
      progress : function(next){
        $(Method.$progressClass).width( ((next.index()) / Method.$stepCount * 100) + '%' );
      }
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-review-write]',
					attrName:'data-module-review-write',
					moduleName:'module_review_write',
					handler:{context:this, method:Method.moduleInit}
				});
			},
			destroy:function(){
				$deferred = null;
				console.log('destroy review-write module');
			},
			setDeferred:function(defer){
				$deferred = defer;
			},
			moduleConnect:function(){
				fileLoad.setToappUploadImage({
					fileName:arguments[0],
					fullUrl:arguments[1],
					result:(arguments[2] === '1') ? true : false
				});
			}
		}
	});
})(Core);
