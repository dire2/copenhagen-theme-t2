(function(Core){
	Core.register('module_mobilegnb', function(sandbox){

		var Method = {
			moduleInit:function(){
				var $this = $(this);
				var args = arguments[0];
				var clickIS = false;
				var isSearch = false;

				var $modile = $('#mobile-menu');
				$modile.find('.mobile-onedepth_list').on('click', '> a', function(e){
					if(!$(this).hasClass('link')){
						e.preventDefault();
						//$(this).siblings().show().stop().animate({'left':0}, 300);
					}
				});

				$modile.find('.location').on('click', function(e){
					e.preventDefault();
					$(this).parent().stop().animate({'left':-270}, 300, function(){
						$(this).css('left', 270).hide();
					});
				});

				$('.gnb-search-btn').click(function(e){
					e.preventDefault();
					$('.header-menu_etc > ul').css('display', 'block');
					$('.search-panel').css('display', 'block');
					$('.etc-search-wrap').addClass('active');
					$("label[for='search-header']").text('검색어를 입력해주세요.');
					$('body').append('<div class="dimed"></div>');
					$('.dimed').click(function(){
						$('.header-menu_etc > ul').css('display', 'none');
						$(this).remove();
					})
				});


				$modile.on('show.uk.offcanvas', function(event, area){
					Core.ga.pv('pageview', '/mobileMenu');
					//android 기본 브라우저에서 scroll down 시 메뉴 노출되지 않은 현상 때문에 clip css 삭제처리
					$('.uk-offcanvas-bar').removeAttr("style");
				});

				$modile.on('hide.uk.offcanvas', function(event, area){
					if(isSearch){
						sandbox.getModule('module_search').searchTrigger();
					}
					isSearch = false;
					$('.dimed').remove();
				});

				$modile.find('.mobile-lnb-search').on('click', function(e){
					e.preventDefault();
					isSearch = true;
					UIkit.offcanvas.hide();
				});

				//모바일에서 프로모션영역 제거
				$(this).find(".mobile-onedepth_list a span").each(function(index){
					if($(this).text() == "FEATURED" || $(this).text() == "BRAND") {
						$(this).closest("li").empty();
					}
				});

				//모바일에서 프로모션영역 제거
				$(this).find(".mobile-twodepth_list a span").each(function(index){
					if($(this).text() == "promoArea") {
						$(this).closest("li").empty();
					}
				});
			}
		}
		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-mobilegnb]',
					attrName:'data-module-mobilegnb',
					moduleName:'module_mobilegnb',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
