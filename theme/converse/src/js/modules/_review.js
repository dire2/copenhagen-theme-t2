(function(Core){
	Core.register('module_review', function(sandbox){
		var $deferred, $this, modal, args, arrQueryString = [], currentProductId, isSignIn;
		var Method = {

			moduleInit:function(){
				args = arguments[0];
				$this = $(this);
				Method.$reviewListModal = UIkit.modal("#modal-reviews-display", {modal: false});
				Method.$triggerClass = '.review-teaser-card-link';
				Method.$modalScroll = Method.$reviewListModal.find('.reviews-summary-scrollable');
				isSignIn = (args.isSignIn === 'true') ? true : false;

				//필터조건 초기화 ( 최신순, 도움순 )
				arrQueryString[2] = sandbox.utils.getQueryParams($('.reviews-summary-options').find('.review-filter').filter('.active').attr('href'), 'array').join('&');

				//modal init
				modal = UIkit.modal('#common-modal', {center:true});
				modal.off('.uk.modal.review').on({
					'hide.uk.modal.review':function(){
						console.log('review modal hide');
						$this.find('.contents').empty();
						if(isSignIn != sandbox.getModule('module_header').getIsSignIn()){
							if(currentProductId) Method.reviewProcessorController(currentProductId);
						}
					}
				});


				$this.find(Method.$triggerClass).click(function(){
					Method.$reviewListModal.show();
					var reviewid = $(this).data('reviewid');
					if (reviewid) {
						reviewid = Method.$reviewListModal.find('[data-reviewid="' + reviewid + '"]');
						setTimeout(function(){
							var scrollable = 'm' == Core.getModule('module_gnb').deviceMedia() ? Method.$modalScroll : Method.$reviewListModal.element,
								scrollTop = reviewid.position().top;
							scrollable.scrollTop(scrollTop);

							reviewid.addClass('set-highlighted');
							setTimeout(function(){
								reviewid.removeClass('set-highlighted');
							}, 639);
						},426);
					}

				});

				// product detail 상품 리뷰 쓰기
				$('.review-write-btn').off('click').on('click',function(e){
					e.preventDefault();

					var target = $(this).attr('href');
					var productId = $(this).attr('data-productid');

					if(!productId){
						UIkit.notify('productID 가 없습니다.', {timeout:3000,pos:'top-center',status:'warning'});
						return;
					}

					Method.reviewTask(target, productId);
				});


				//review feedback
				var feedBack = sandbox.getComponents('component_like', {context:$this}, function(){
					this.addEvent('likeFeedBack', function(data){
						if(data.hasOwnProperty('HELPFUL')){
							$(this).parent().siblings().find('.like-num').text(data.HELPFUL);
						}else if(data.hasOwnProperty('NOTHELPFUL')){
							console.log(data.NOTHELPFUL);
						}
					});
				});


				// 상품 리뷰 수정
				$('.review-modify').off('click').on('click',function(e){
					e.preventDefault();

					var target = $(this).attr('href');
					var url = $(this).attr('data-link');
					var productId = $(this).attr('data-productid');
					var defer = $.Deferred();
					var successMsg = $(this).attr('data-successmsg');

					//review 모달 css 추가
					$(target).addClass('window-modal modal-reviews-submit');

					sandbox.utils.promise({
						url:url,
						type:'GET',
						data:{'redirectUrl':location.pathname}
					}).then(function(data){
						$(target).find('.contents').empty().append(data).css('height','100%');
						sandbox.moduleEventInjection(data, defer);
						return defer.promise();
					}).then(function(data){
						//arrQueryString = [];
						UIkit.notify(successMsg, {timeout:3000,pos:'top-center',status:'success'});
						Method.reviewProcessorController(productId);
						modal.hide();
						location.reload();
					}).fail(function(msg){
						defer = null;
						UIkit.notify(msg, {timeout:3000,pos:'top-center',status:'danger'});
						if(modal.isActive()) modal.hide();
					});
				});



				// 상품 리뷰 삭제
				$('.review-remove').off('click').on('click',function(e){
					e.preventDefault();
					var _self = this;
					var url = $(this).attr('href');
					var productId = $(this).attr('data-productid');
					var reviewId = $(this).attr('data-reviewId');
					var successMsg = $(this).attr('data-successmsg');

					UIkit.modal.confirm("삭제 할까요?", function(){
						sandbox.utils.ajax(url, 'GET', {}, function(data){
							var data = sandbox.rtnJson(data.responseText);
							if(data.result){
								UIkit.notify(successMsg, {timeout:3000,pos:'top-center',status:'success'});
								Method.reviewProcessorController(productId);
                				location.reload();
							}else{
								UIkit.notify(data.errorMessage, {timeout:3000,pos:'top-center',status:'danger'});
							}
						});
					});
				});


				//review filter
				$('.review-filter').off('click').on('click',function(e){
					e.preventDefault();
					var _this = $(this);
					var query = sandbox.utils.getQueryParams($(this).attr('href'), 'array').join('&');
					var productId = $(this).attr('data-productid');

					sandbox.utils.ajax(args.api, 'GET', 'productId='+ productId + '&' + query + '&mode=template&resultVar=reviewSummaryDto', function(data){
						var domObject = $(data.responseText).find('.reviews-summary-list');
						_this.addClass('active').siblings().removeClass('active');
						$(data.responseText).find('.reviews-summary-list');
						Method.$reviewListModal.find('.reviews-summary-list').empty().append(domObject[0].outerHTML);
						sandbox.moduleEventInjection(domObject[0].outerHTML);
					})
				});

				$('.review-filter-delete').off('click').on('click',function(e){
					e.preventDefault();

					var query = sandbox.utils.getQueryParams($(this).attr('href'), 'array').join('&');
					var productId = $(this).attr('data-productid');

					arrQueryString = [];
					arrQueryString[2] = query;
					Method.reviewProcessorController(productId);
				});



				/* browse history back */
				if(Core.Utils.mobileChk) {
					$(window).on('popstate', function(e) {
						var data = e.originalEvent.state;
						if(modal && modal.active){
							modal.hide();
						}
					});
				}
			},
			reviewTask:function(target, productId){
				var defer = $.Deferred();
				currentProductId = productId;

				sandbox.getModule('module_header').setLogin(function(data){
					//console.log('review : ', data);
					var isSignIn = data.isSignIn;
					sandbox.utils.promise({
						url:sandbox.utils.contextPath + '/review/reviewWriteCheck',
						type:'GET',
						data:{'productId':productId}
					}).then(function(data){
						//data.expect 기대평
						//data.review 구매평
						if(data.expect || data.review){
							/* review history */
							if(Core.Utils.mobileChk) history.pushState({page:'review'}, "review", location.href);

							isSignIn = isSignIn;
							modal.show();
							return sandbox.utils.promise({
								url:sandbox.utils.contextPath + '/review/write',
								type:'GET',
								data:{'productId':productId, 'redirectUrl':location.pathname, 'isPurchased':data.review}
							})
						}else{
							defer.reject('리뷰를 작성할 수 없습니다.');
						}
					}).then(function(data){
						$(target).removeClass('modal-account-auth');
						$(target).addClass('window-modal modal-reviews-submit');
						$(target).find('.contents').empty().append(data);
						sandbox.moduleEventInjection(data, defer);
						return defer.promise();
					}).then(function(data){
						Method.reviewProcessorController(productId);
						modal.hide();
						location.reload();
					}).fail(function(msg){
						defer = null;
						UIkit.notify(msg, {timeout:3000,pos:'top-center',status:'danger'});
						if(modal.isActive()) modal.hide();
					});
				});
			},
			reviewProcessorController:function(productId){
				var arrData = [];
				var obj = {
					/*'mode':'template',
					'templatePath':'/modules/productListReview',
					'resultVar':'review',*/
					'productId':productId
				}

				for(var key in obj){
					arrData.push(key+'='+obj[key]);
				}

				///processor/execute/review      /review/list, /account/reviewlist
				//console.log(arrQueryString.join('&'));

				//템플릿 캐시로 인해 추가된 로딩바 상태
				sandbox.setLoadingBarState(true);
				sandbox.utils.ajax(args.api, 'GET', arrData.join('&') + '&' + arrQueryString.join('&') + '&mode=template&resultVar=reviewSummaryDto', function(data){
					// var articleGet = $(data.responseText).find('.reviews-summary-list');
					// console.log(articleGet);
					//
					// var listWrap = $(args.target).find('.reviews-summary-list');
					// listWrap.empty().append(articleGet);


					// var cnt = $(data.responseText).find(".star-review-num").attr("data-totalratingcount");
					// if(typeof(cnt) == "undefined"){
					// 	cnt=0;
					// }
					// if(Core.Utils.mobileChk) {
					// 	$("div[data-module-review]").empty().append(data.responseText);
					// 	$(".detail-review").find(".detail-title").text('상품평 ('+cnt+')');
					// 	$(".detail-review").find(".detail-title").append("<i class='icon-arrow_bottom'></i>");
					// }else{
					 	$(args.target).empty().append(data.responseText);
					//
					// 	//상품평개수 변경
          //   $(".review-cnt").text(cnt);
          //   $(".n-review-sort").find(".review-cnt").text("총 "+cnt+" 건 상품평");
          //   $(".total-cnt").text("총 "+cnt+"건 상품평");
					// 	//상세 밑에 리뷰추가
					// 	$(".detail-review").empty().append(data.responseText);
					// }
					// setTimeout( function(){
					// 	$(".review-write-btn").attr("data-productid",productId);
					// }, 500);
					sandbox.moduleEventInjection(data.responseText);
				}, false, false);
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-review]',
					attrName:'data-module-review',
					moduleName:'module_review',
					handler:{context:this, method:Method.moduleInit}
				});
			},
			destroy:function(){
				$deferred = null;
				$this = null;
				args = null;
				modal = null;

				console.log('destroy reveiw module');
			},
			setDeferred:function(defer){
				$deferred = defer;
			},
			history:function(){

			}
		}
	});
})(Core);
