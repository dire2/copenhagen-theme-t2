(function(Core){
    Core.register('module_exchanged_payment', function(sandbox){
		var Method = {
		    $that:null,
			moduleInit:function(){
				var args = Array.prototype.slice.call(arguments).pop();
				$.extend(Method, args);

				var $this = $(this);
				Method.$that = $this;
				$this.siblings('#popup-exchange-step02').find('button.exchange-payment-item').on("click", Method.checkout );
        //Method.$activeItem = $this.find(".payment-method-item.active");
				/* PAYMENT 다각화 대비
				Method.$submitBtn = $this.find('[data-checkout-btn]');
				Method.$submitBtn.on("click", Method.checkout );
				$this.find('[data-payment-method]').find('.payment-method-item-title').on('click', Method.changePaymentMethod);
				$this.find('[data-cod-btn]').on("click", Method.codCheckout );

				$(document).on('click','.payment-method-item-title', Method.changePaymentMethod_1);  //payment 종류 선택.
				*/

			},

	checkout:function(e){
        e.preventDefault();
          $(this).attr("enabled");

          var _self = $(this);
          var payItemList = $(this).closest("#popup-exchange").closest(".order-item-wrap");
          var orderItemList = $(this).closest("#popup-exchange-step02").find('[data-exchange-complete-list]');
          var orderNumber = $(this).closest("form").find('[name="orderReturnNumber"]').val();
          var $activeItem = Method.$that.find(".payment-method-item.active");
          var orderId = $(this).closest("#popup-exchange").find('[name="orderId"]').val();
          var returnOrderId = $(this).closest("#popup-exchange-step02").find('[name="returnOrderId"]').val();
          var exchageAmount = $(this).closest("#popup-exchange-step02").find('[data-exchange-amount]').text().replace(/[^0-9]/g,'');

          var $form = $(this).closest("#popup-exchange-step02").find('form');
          var param = $form.serialize() + '&exchageAmount=' + exchageAmount;

          if( $activeItem.data("type") == 'CREDIT_CARD' ){
            param += '&pay_method=card';
          } else {
            param += '&pay_method='+$activeItem.data("method");
          }

          sandbox.validation.validate( $form );
          if( sandbox.validation.isValid($form)){
            sandbox.utils.promise({
              url:sandbox.utils.contextPath + '/account/orders/exchangeable/requestPay',
              type:'POST',
              data:param
            }).then(function(data){
              sandbox.setLoadingBarState(true);
              if(data.isError == false){
                sandbox.setLoadingBarState(false);
                var paymentType = ($activeItem.length > 0) ? $activeItem.data("type") : null;
                endPoint.call("orderSubmit", { 'paymentType' : paymentType });

                if(data.payStatus == true){
                  //iamport 모듈
                  if( $activeItem.data('provider') == 'IAMPORT' ){
                    Method.checkoutIamport( $activeItem, data ,returnOrderId, payItemList, orderItemList);
                  }
                } else {
                  UIkit.modal.alert("교환 신청이 완료 되었습니다.").on('hide.uk.modal', function(){
                    location.href = sandbox.utils.contextPath + "/account/orders/returned";
                  });
                }
              }else{
                sandbox.setLoadingBarState(false);
                UIkit.modal.alert( data._global ).on('hide.uk.modal', function() {
                  location.reload();
                });

                // if(data._global == '선택 한 상품의 재고가 없습니다'){
                //   UIkit.modal.confirm(data._global + '<br/>해당상품의 수량변경 또는 삭제하여야 주문이 가능합니다.<br/>장바구니로 이동하시겠습니까?', function(){
                //     location.href = sandbox.utils.contextPath + '/account/orders/returned';
                //   });
                // }else{
                //   //Method.updateSubmitBtn( true );
                //   UIkit.modal.alert(data._global);
                // }
              }
            }).fail(function(msg){
                sandbox.setLoadingBarState(false);
                UIkit.notify(msg, {timeout:3000,pos:'top-center',status:'danger'});
                //Method.updateSubmitBtn( true );
            });
          }else{
            UIkit.modal.alert("교환 사유를 입력하여 주시기 바랍니다.");
          }

      },
      checkoutIamport:function( $activeItem, data ,returnOrderId, payItemList, orderItemList){
        // 결제 전일때

        var IMP = window.IMP;

        var paymentMethod = $activeItem.data("method"); // 결제 수단
        var mRedirectUrl = $activeItem.data("m-redirect-url"); // 모바일 url
        var noticeUrl = $activeItem.data("notice-url"); // 노티피케이션 url
        var version = $activeItem.data("version") || 'old'; // 에스크로 분기 처리를 위한 값  new or old
        var escrow = $activeItem.data("escrow"); // 에스크로 사용 여부

        var useReplaceReturnUrl = false;
        var cUrl = Core.Utils.url.getCurrentUrl();

        // 접근한 URL이 mshop 일 때
        if( cUrl.indexOf( 'www.' ) > -1 ){
          useReplaceReturnUrl = true;
        }else{
          // 접근한 URL이 mshop 이 아닌데 deviceOs 가 ios 일때
          if( String(Core.Utils.url.getQueryStringParams(cUrl).deviceOs ).toLowerCase() == 'ios' ){
            useReplaceReturnUrl = true;
          }
        }

        if( useReplaceReturnUrl ){
          if( mRedirectUrl != null ){
            mRedirectUrl = mRedirectUrl.replace('m.', 'www.');
          }
        }

        var appScheme = $activeItem.data("app-scheme"); // 모바일 앱 스키마 정보
        var identityCode = $activeItem.data("identity-code"); // iamport key
        var pg = $activeItem.data("pg");

        if( paymentMethod == '' || identityCode == '' || pg == ''){
          UIkit.modal.alert('결제 수단 정보로 인한 문제가 발생하였습니다.<br/>고객센터('+_GLOBAL.SITE.TEL+ ')로 문의 주시면 신속히 처리 도와드리겠습니다.');
          return;
        }

        IMP.init(identityCode);

        var $orderList = orderItemList.find("ul").find("li");
        var name = '(교환비) ' + $orderList.eq(0).find('.tit').text();
        if( $orderList.length > 1 ){
          name += ' 외 ' + ($orderList.length-1);
        }
        var buyerName = data.return_address.fullName;
        var param = {
          //pay_method : _GLOBAL.PAYMENT_TYPE_BY_IAMPORT[ paymentMethod ], // 추후 provider 에 따라 변수변경 *서버에서 내려오고 있음
          pg : pg,
          pay_method : paymentMethod, // 추후 provider 에 따라 변수변경
          merchant_uid : returnOrderId + '_' + new Date().getTime(),
          name: name,
          amount:data.total_amount.amount,
          buyer_email:data.emailAddress,
          buyer_name:buyerName,
          buyer_tel:data.return_address.phonePrimary.phoneNumber,
          buyer_addr:data.return_address.addressLine1 +' '+ data.return_address.addressLine2,
          buyer_postcode:data.return_address.postalCode,
          m_redirect_url:mRedirectUrl,
          app_scheme:appScheme,
          notice_url:noticeUrl,
          bypass:{acceptmethod:"SKIN(#111)"}
        };

        var depositPeriod = $activeItem.find('[name="depositPeriod"]').val() || 2;
        if( paymentMethod == 'vbank' ) {
          param.vbank_due = moment().add(depositPeriod, 'day').format('YYYYMMDD2359');
          param.custom_data = $activeItem.find('form').serialize().replace(/=/gi, ':').replace(/&/gi, '|');
        }
        if ($activeItem.data('type') == 'NAVER_PAY'){
					param.naverPopupMode = true;
					param.naverProducts = [];
					param.naverProducts.push({
            categoryType: 'ETC',
            categoryId: 'ETC',
            uid: 1,
            name: name,
            count: 1,
          })
				}
        if( escrow == true ){
          param.escrow = true;
        }
        console.log(param);
        IMP.request_pay(param, function(rsp) {
          //결제 완료시
          if ( rsp.success ) {
            var msg = '결제가 완료되었습니다.<br>';
            msg += '고유ID : ' + rsp.imp_uid + '<br>';
            msg += '상점 거래ID : ' + rsp.merchant_uid + '<br>';
            msg += '결제 금액 : ' + rsp.paid_amount + '<br>';
            msg += 'custom_data : ' + rsp.custom_data + '<br>';

            if ( rsp.pay_method === 'card' ) {
              msg += '카드 승인번호 : ' + rsp.apply_num + '<br>';
            } else if ( rsp.pay_method === 'vbank' ) {
              msg += '가상계좌 번호 : ' + rsp.vbank_num + '<br>';
              msg += '가상계좌 은행 : ' + rsp.vbank_name + '<br>';
              msg += '가상계좌 예금주 : ' + rsp.vbank_holder + '<br>';
              msg += '가상계좌 입금기한 : ' + rsp.vbank_date + '<br>';
            }
            //alert( msg );
            sandbox.setLoadingBarState(true);
            _.delay(function(){
              UIkit.modal.alert("배송비 결제가 완료되었습니다.");
              location.href = sandbox.utils.contextPath + '/checkout/iamport-checkout/hosted/return?imp_uid=' + rsp.imp_uid + '&pay_method=' + rsp.pay_method + '&pg_provider=' + rsp.pg_provider + '&custom_data=' + rsp.custom_data;
            }, 3000);
          }else{
            sandbox.setLoadingBarState(false);
            if( rsp.error_msg == '사용자가 결제를 취소하셨습니다'){
              endPoint.call('orderCancel');
            }
            UIkit.modal.alert( rsp.error_msg ).on('hide.uk.modal', function() {
              sandbox.setLoadingBarState(true);
              var cartId = Method.$that.find("input[name='cartId']").val();
              location.href = sandbox.utils.contextPath + '/account/orders/returnable';
            });
          }
        });
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-exchanged-payment]',
					attrName:'data-module-exchanged-payment',
					moduleName:'module_exchanged_payment',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
