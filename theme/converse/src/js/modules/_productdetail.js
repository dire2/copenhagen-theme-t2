(function(Core){
	Core.register('module_product_detail', function(sandbox){
		var $this;
		var Method = {
			$pdpdetails : $('.pdp-details-section'),
			$reviewTeaser : $('.review-teaser-slider'),
			$header : $('.header-general'),
			$reviewWrap: $('[data-module-review]'),
			$slickReview : null,
			$deviceMedia : null,
        moduleInit : function(){
            $this = $(this);
            
            //PDP 옵션 상세 더보기 Btn
            $('.product-description-lead').on('click',function(){
                $('html,body').animate({ 'scrollTop' : Method.$pdpdetails.offset().top - Method.$header.height() },426);
                return false;
            });

            //PDP 옵션 리뷰 Btn
            $('.reviews-stars-link').on('click',function(){
                $('html,body').animate({ 'scrollTop' : Method.$reviewWrap.offset().top - Method.$header.height() },426);
                return false;
            });

            // PDP 상세 더보기 버튼
            Method.$pdpdetails.on('click','.pdp-details-toggle',function(e){
                e.preventDefault();
                var scrollPosition;
                Method.$pdpdetails.add(this).toggleClass('active');

                if($(this).hasClass('topBtn')){
                    $(this).addClass('uk-hidden');
                    $('.bottomBtn').removeClass('uk-hidden');
                }else{
                    $(this).addClass('uk-hidden');
                    $('.topBtn').removeClass('uk-hidden');
                    $('html,body').animate({ 'scrollTop' : Method.$pdpdetails.offset().top - Method.$header.height() },426);
                    
                }
            });

            Method.sizeGuideAction();
            Method.tagsAccordion();
            if (!Method.$reviewTeaser.length) return false;
            Method.reviewTeaser();
            
        },
        reviewTeaser : function(){
            Method.$slickReview = Method.$reviewTeaser.slick({
            infinite: false,
            autoplay : false,
            dots:false,
            slidesToShow: 2,
            slidesToScroll: 2,
            centerPadding: 0,
            prevArrow : '<button type="button" class="icon-rotate-h slick-prev slick-arrow" title="Go to Previous"><svg class="icon"><use xlink:href="#icon-arrow"></use></svg></button>',
            nextArrow : '<button type="button" class="slick-next slick-arrow" title="Go to Next"><svg class="icon"><use xlink:href="#icon-arrow"></use></svg></button>',
            appendArrows : Method.$reviewTeaser.closest('.pdp-details-section').find('.slick-arrows'),

            });
        },
        tagsAccordion : function(){
            var tags = $('.product-tags'),
                tagsHead = tags.find('.product-tags-head'),
                tagsContent = tags.find('.product-tags-contents');

            tagsHead.on('click',function(){
                if (!tagsContent.is(':animated')) {
                    tags.toggleClass('active');
                    tagsContent.slideToggle();
                }
                return false;
            });
        },
        sizeGuideAction : function(){
            var chartClass = '.size-guide-chart',
            hoverText = 'hover',
            siblinghoverText = 'sibling-hover';

            $(document).on('click','[data-size-chart-toggle]',function(){
                var anchor = $(this),
                    target = anchor.data('sizeChartToggle'),
                    parent = anchor.parent(),
                    wrap = anchor.closest('.size-guide-content');
                    
                parent.siblings().children('a').removeClass('active');
                anchor.addClass('active');
                wrap.find('[data-size-chart-table="'+ target +'"]').removeClass('hidden').siblings().addClass('hidden');
                return false;
            });
            $(document).on('mouseenter', chartClass + ' td',function(){
                var td = $(this),
                    col = td.index(),
                    table = td.closest(chartClass);

                td.siblings('.exception').length && (col -= 1);
                table.find('td').removeClass(hoverText).removeClass(siblinghoverText);
                table.find('th').removeClass(siblinghoverText);
                table.find('thead').find('th').not('.exception').eq(col).addClass(siblinghoverText);
                td.addClass(hoverText);
                td.prevAll().addClass(siblinghoverText);

                td.parent().prevAll().each(function(eq, tr){
                    $(tr).children().not('.exception').eq(col).addClass(siblinghoverText);
                });
            });
            $(document).on('mouseleave',chartClass,function(){
                var table = $(this);
                table.find('td').removeClass(hoverText).removeClass(siblinghoverText);
                table.find('th').removeClass(siblinghoverText);
            });
        }
    }
		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-product-detail]',
					attrName:'data-module-product-detail',
					moduleName:'module_product_detail',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
