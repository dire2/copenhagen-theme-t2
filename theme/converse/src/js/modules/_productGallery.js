(function(Core){
    'use strict';

	Core.register('module_product_gallery', function(sandbox){

		var Method = {
                $reszieEvent : 'orientationchange' in window ? 'orientationchange.pdpgallery' : 'resize.pdpgallery',
                $gallery : $('.pdp-gallery-carousel'),
                $galleryControls : $('.pdp-media-gallery-controls'),
                $primary : $('.pdp-primary-image'),
                $slickGallery : null,
                $videoGallery : null,
                $deviceMedia : null,
                $isOpen : false,
                moduleInit : function(){
                    if (!Method.$gallery.length) return false;

                    Method.$videoGallery = Method.$gallery.find('video');
                    var carouselIndex = 0;
                    Method.$primary.each(function(eq, el){
                        el = $(el);
                        el.data('carouselIndex', carouselIndex);
                        carouselIndex++;
                    });
                    Method.$primary.on('click','img',function(){
                        Method.zoomGallery($(this).data('carouselIndex'));
                        return false;
                    });
                    Method.$gallery.find('.item-container:eq(0)').on('click','img',function(){
                        Method.zoomGallery(0);
                        return false;
                    });

                    Method.resizeAction();
                },
                resizeAction : function(){
                    $(window).on(Method.$reszieEvent,function(){
                        if (Method.$deviceMedia != Core.getModule('module_gnb').deviceMedia()){
                            Method.$deviceMedia = Core.getModule('module_gnb').deviceMedia();
                            Method.productGallery();
                        }
                    }).trigger(Method.$reszieEvent);
                },
                productGallery : function(){
                    if ('m' == Method.$deviceMedia){
                        Method.$gallery.on('init beforeChange',function(e,o){
                            Method.$videoGallery.length && Method.$videoGallery.get(0).pause();
                        }).on('afterChange',function(e,o){
                            var video = o.$slides.filter('.slick-active').find('video');
                            video.length && video.get(0).play();
                        });
                        Method.$slickGallery = Method.$gallery.slick({
                            infinite: false,
                            autoplay : false,
                            dots:true,
                            prevArrow : '<button type="button" class="icon-rotate-h slick-prev slick-arrow" title="Go to Previous"><svg class="icon"><use xlink:href="#icon-arrow"></use></svg></button>',
                            nextArrow : '<button type="button" class="slick-next slick-arrow" title="Go to Next"><svg class="icon"><use xlink:href="#icon-arrow"></use></svg></button>',
                            appendDots : Method.$galleryControls.find('.carousel-count'),
                            appendArrows : Method.$galleryControls,
                            customPaging : function(slick, index){
                                return (index+1)    + '/' + slick.slideCount;
                            }
                        });
                    }
                    if ('m' != Method.$deviceMedia){
                        Method.$slickGallery && Method.$slickGallery.slick('unslick');
                        Method.$videoGallery.length && Method.$videoGallery.get(0).play();
                    }
                },
                zoomGallery : function(index){
                    $('body').css('overflow','hidden');
                    var itemList = [];
                    Method.$primary.find('img').each(function(){
                        var img = $(this),
                            item = {};
                        var w = $(window).width(), h = $(window).height(), m = w>h?w:h, r1 = w/h, r2 = h/w, r = r1 < r2 ? r1 : r2;
                        var iw = img.get(0).width,
                            ih = img.get(0).height,
                            s = m/iw > m/ih ? m/iw : m/ih;
                        item.src = img.attr('src').split('?')[0];
                        item.w = iw * s;
                        item.h = ih * s;
                        itemList.push(item);
                    });

                    var pswpElement = document.querySelectorAll('.pswp')[0];
                    var pswp = new PhotoSwipe( pswpElement, Method.photoSwipeUI, itemList, {
                        index : index,
                        barsSize : {top:0,bottom:0},
                        history: false,
                        loop: false,
                        focus: false,
                        closeOnScroll : false,
                        showAnimationDuration: 0,
                        hideAnimationDuration: 0,
                        pinchToClose : false,
                        closeOnVerticalDrag : false,
                        scaleMode : 'orig',
                        getDoubleTapZoom: function(isMouseClick, item) {
                            if(isMouseClick) {
                                    return 2;
                            } else {
                                    return item.initialZoomLevel < 1 ? 2 : 1.33;
                            }
                        },
                        getThumbBoundsFn: function(index) {
                           var w = $(window).width(), h = $(window).height();
                           return {x: w/2, y:h/2, w:w};
                        },                        
                        maxSpreadZoom: 2
                    });
                    pswp.init();
                    pswp.listen('close', function() {
                        $('body').css('overflow','visible');
                        Method.$isOpen = false;
                        return false;
                    });
                    pswp.listen('afterChange', function() {
                        if ('m' == Method.$deviceMedia){
                            var index = pswp.getCurrentIndex();
                            if (Method.$videoGallery.length){
                                var videoIndex = Method.$videoGallery.closest('.item-container').index();
                                if (index >= videoIndex) {
                                    index += 1;
                                }
                            }
                            Method.$slickGallery.slick('slickGoTo',index);
                        }
                    });
                    Method.$isOpen = true;
                },
                photoSwipeUI: function photoSwipeUI(pswp, framework) {
                    var ui = this;
                    var _overlayUIUpdated = false,
                        _controlsVisible = true,
                        _fullscrenAPI,
                        _controls,
                        _captionContainer,
                        _fakeCaptionContainer,
                        _indexIndicator,
                        _shareButton,
                        _shareModal,
                        _shareModalHidden = true,
                        _initalCloseOnScrollValue,
                        _isIdle,
                        _listen,

                        _loadingIndicator,
                        _loadingIndicatorHidden,
                        _loadingIndicatorTimeout,

                        _galleryHasOneSlide,

                        _options,
                        _defaultUIOptions = {
                            barsSize: {top:0, bottom:0},
                            timeToIdle: 4000,
                            timeToIdleOutside: 1000,
                            loadingIndicatorDelay: 1000, // 2s
                            closeEl:true,
                            captionEl: true,
                            fullscreenEl: true,
                            zoomEl: true,
                            shareEl: true,
                            counterEl: true,
                            arrowEl: true,
                            preloaderEl: true,

                            tapToClose: false,
                            tapToToggleControls: true,

                            clickToCloseNonZoomable: true,
                            indexIndicatorSep: ' / ',
                            fitControlsWidth: 1200

                        },
                        _blockControlsTap,
                        _blockControlsTapTimeout;



                    var _onControlsTap = function(e) {
                        if(_blockControlsTap) {
                            return true;
                        }


                        e = e || window.event;

                        var target = e.target || e.srcElement,
                            uiElement,
                            clickedClass = target.getAttribute('class') || '',
                            found;

                        for(var i = 0; i < _uiElements.length; i++) {
                            uiElement = _uiElements[i];
                            if(uiElement.onTap && clickedClass.indexOf('pswp__' + uiElement.name ) > -1 ) {
                                uiElement.onTap();
                                found = true;

                            }
                        }

                        if(found) {
                            if(e.stopPropagation) {
                                e.stopPropagation();
                            }
                            _blockControlsTap = true;

                            // Some versions of Android don't prevent ghost click event
                            // when preventDefault() was called on touchstart and/or touchend.
                            //
                            // This happens on v4.3, 4.2, 4.1,
                            // older versions strangely work correctly,
                            // but just in case we add delay on all of them)
                            var tapDelay = framework.features.isOldAndroid ? 600 : 30;
                            _blockControlsTapTimeout = setTimeout(function() {
                                _blockControlsTap = false;
                            }, tapDelay);
                        }

                    },
                    _fitControlsInViewport = function() {
                        return !pswp.likelyTouchDevice || _options.mouseUsed || screen.width > _options.fitControlsWidth;
                    },
                    _togglePswpClass = function(el, cName, add) {
                        framework[ (add ? 'add' : 'remove') + 'Class' ](el, 'pswp__' + cName);
                    },
                    // add class when there is just one item in the gallery
                    // (by default it hides left/right arrows and 1ofX counter)
                    _countNumItems = function() {
                        var hasOneSlide = (_options.getNumItemsFn() === 1);

                        if(hasOneSlide !== _galleryHasOneSlide) {
                            _togglePswpClass(_controls, 'ui--one-slide', hasOneSlide);
                            _galleryHasOneSlide = hasOneSlide;
                        }
                    },
                    _toggleShareModalClass = function() {
                        _togglePswpClass(_shareModal, 'share-modal--hidden', _shareModalHidden);
                    },
                    _toggleShareModal = function() {

                        _shareModalHidden = !_shareModalHidden;


                        if(!_shareModalHidden) {
                            _toggleShareModalClass();
                            setTimeout(function() {
                                if(!_shareModalHidden) {
                                    framework.addClass(_shareModal, 'pswp__share-modal--fade-in');
                                }
                            }, 30);
                        } else {
                            framework.removeClass(_shareModal, 'pswp__share-modal--fade-in');
                            setTimeout(function() {
                                if(_shareModalHidden) {
                                    _toggleShareModalClass();
                                }
                            }, 300);
                        }
                        return false;
                    },
                    _hasCloseClass = function(target) {
                        /*
                        for(var    i = 0; i < _options.closeElClasses.length; i++) {
                            if( framework.hasClass(target, 'pswp__' + _options.closeElClasses[i]) ) {
                                return true;
                            }
                        }
                        */
                    },
                    _idleInterval,
                    _idleTimer,
                    _idleIncrement = 0,
                    _setupFullscreenAPI = function() {
                        if(_options.fullscreenEl && !framework.features.isOldAndroid) {
                            if(!_fullscrenAPI) {
                                _fullscrenAPI = ui.getFullscreenAPI();
                            }
                            if(_fullscrenAPI) {
                                framework.bind(document, _fullscrenAPI.eventK, ui.updateFullscreen);
                                ui.updateFullscreen();
                                framework.addClass(pswp.template, 'pswp--supports-fs');
                            } else {
                                framework.removeClass(pswp.template, 'pswp--supports-fs');
                            }
                        }
                    },
                    _setupLoadingIndicator = function() {
                        // Setup loading indicator
                        if(_options.preloaderEl) {

                            _toggleLoadingIndicator(true);

                            _listen('beforeChange', function() {

                                clearTimeout(_loadingIndicatorTimeout);

                                // display loading indicator with delay
                                _loadingIndicatorTimeout = setTimeout(function() {

                                    if(pswp.currItem && pswp.currItem.loading) {

                                        if( !pswp.allowProgressiveImg() || (pswp.currItem.img && !pswp.currItem.img.naturalWidth)    ) {
                                            // show preloader if progressive loading is not enabled,
                                            // or image width is not defined yet (because of slow connection)
                                            _toggleLoadingIndicator(false);
                                            // items-controller.js function allowProgressiveImg
                                        }

                                    } else {
                                        _toggleLoadingIndicator(true); // hide preloader
                                    }

                                }, _options.loadingIndicatorDelay);

                            });
                            _listen('imageLoadComplete', function(index, item) {
                                if(pswp.currItem === item) {
                                    _toggleLoadingIndicator(true);
                                }
                            });

                        }
                    },
                    _toggleLoadingIndicator = function(hide) {
                        if( _loadingIndicatorHidden !== hide ) {
                            _togglePswpClass(_loadingIndicator, 'preloader--active', !hide);
                            _loadingIndicatorHidden = hide;
                        }
                    },
                    _applyNavBarGaps = function(item) {
                        var gap = item.vGap;

                        if( _fitControlsInViewport() ) {

                            var bars = _options.barsSize;
                            if(_options.captionEl && bars.bottom === 'auto') {
                                if(!_fakeCaptionContainer) {
                                    _fakeCaptionContainer = framework.createEl('pswp__caption pswp__caption--fake');
                                    _fakeCaptionContainer.appendChild( framework.createEl('pswp__caption__center') );
                                    _controls.insertBefore(_fakeCaptionContainer, _captionContainer);
                                    framework.addClass(_controls, 'pswp__ui--fit');
                                }
                                if( _options.addCaptionHTMLFn(item, _fakeCaptionContainer, true) ) {

                                    var captionSize = _fakeCaptionContainer.clientHeight;
                                    gap.bottom = parseInt(captionSize,10) || 44;
                                } else {
                                    gap.bottom = bars.top; // if no caption, set size of bottom gap to size of top
                                }
                            } else {
                                gap.bottom = bars.bottom === 'auto' ? 0 : bars.bottom;
                            }

                            // height of top bar is static, no need to calculate it
                            gap.top = bars.top;
                        } else {
                            gap.top = gap.bottom = 0;
                        }
                    };

                    var _uiElements = [
                        {
                            name: 'button--zoom',
                            option: 'zoomEl',
                            onTap: pswp.toggleDesktopZoom
                        },
                        {
                            name: 'counter',
                            option: 'counterEl',
                            onInit: function(el) {
                                _indexIndicator = el;
                            }
                        },
                        {
                            name: 'button--close',
                            option: 'closeEl',
                            onTap: function() {
                                setTimeout(function(){
                                    pswp.close();
                                });
                            }
                        },
                        {
                            name: 'button--arrow--left',
                            option: 'arrowEl',
                            onTap: pswp.prev
                        },
                        {
                            name: 'button--arrow--right',
                            option: 'arrowEl',
                            onTap: pswp.next
                        },
                        {
                            name: 'preloader',
                            option: 'preloaderEl',
                            onInit: function(el) {
                                _loadingIndicator = el;
                            }
                        }
                    ];
                    var _setupUIElements = function() {
                        var item,
                            classAttr,
                            uiElement;
                        var loopThroughChildElements = function(sChildren) {
                            if(!sChildren) {
                                return;
                            }
                            var l = sChildren.length;
                            for(var i = 0; i < l; i++) {
                                item = sChildren[i];
                                classAttr = item.className;
                                for(var a = 0; a < _uiElements.length; a++) {
                                    uiElement = _uiElements[a];
                                    if(classAttr.indexOf('pswp__' + uiElement.name) > -1    ) {
                                        if( _options[uiElement.option] ) {
                                            framework.removeClass(item, 'pswp__element--disabled');
                                            if(uiElement.onInit) {
                                                uiElement.onInit(item);
                                            }
                                        } else {
                                            framework.addClass(item, 'pswp__element--disabled');
                                        }
                                    }
                                }
                            }
                        };
                        loopThroughChildElements(_controls.children);

                        var topBar =    framework.getChildByClass(_controls, 'pswp__top-bar');
                        if(topBar) {
                            loopThroughChildElements( topBar.children );
                        }
                    };

                    ui.init = function() {

                        // extend options
                        framework.extend(pswp.options, _defaultUIOptions, true);

                        // create local link for fast access
                        _options = pswp.options;

                        // find pswp__ui element
                        _controls = framework.getChildByClass(pswp.scrollWrap, 'pswp__ui');

                        // create local link
                        _listen = pswp.listen;
                        // update controls when slides change
                        _listen('beforeChange', function(){
                            ui.coverSize();
                            ui.update();
                        });

                        // toggle zoom on double-tap
                        _listen('doubleTap', function(point) {
                            var initialZoomLevel = pswp.currItem.initialZoomLevel;
                            if(pswp.getZoomLevel() !== initialZoomLevel) {
                                pswp.zoomTo(initialZoomLevel, point, 333);
                            } else {
                                pswp.zoomTo(_options.getDoubleTapZoom(false, pswp.currItem), point, 333);
                            }
                        });

                        // Allow text selection in caption
                        _listen('preventDragEvent', function(e, isDown, preventObj) {
                            var t = e.target || e.srcElement;
                            ui.update();
                            if(
                                t &&
                                t.getAttribute('class') && e.type.indexOf('mouse') > -1 &&
                                ( t.getAttribute('class').indexOf('__caption') > 0 || (/(SMALL|STRONG|EM)/i).test(t.tagName) )
                            ) {
                                preventObj.prevent = false;
                            }
                        });
                        _listen('resize', function() {
                            if (Method.$isOpen) pswp.close();
                        });
                        // bind events for UI
                        _listen('bindEvents', function() {
                            framework.bind(_controls, 'pswpTap click', _onControlsTap);
                            framework.bind(pswp.scrollWrap, 'pswpTap', ui.onGlobalTap);
                        });

                        // unbind events for UI
                        _listen('unbindEvents', function() {
                            if(!_shareModalHidden) {
                                _toggleShareModal();
                            }

                            if(_idleInterval) {
                                clearInterval(_idleInterval);
                            }
                            framework.unbind(_controls, 'pswpTap click', _onControlsTap);
                            framework.unbind(pswp.scrollWrap, 'pswpTap', ui.onGlobalTap);

                            if(_fullscrenAPI) {
                                framework.unbind(document, _fullscrenAPI.eventK, ui.updateFullscreen);
                                if(_fullscrenAPI.isFullscreen()) {
                                    _options.hideAnimationDuration = 0;
                                    _fullscrenAPI.exit();
                                }
                                _fullscrenAPI = null;
                            }
                        });

                        // clean up things when gallery is destroyed
                        _listen('destroy', function() {
                            if(_options.captionEl) {
                                if(_fakeCaptionContainer) {
                                    _controls.removeChild(_fakeCaptionContainer);
                                }
                            }

                            if(_shareModal) {
                                _shareModal.children[0].onclick = null;
                            }
                            framework.removeClass(_controls, 'pswp__ui--over-close');
                        });

                        _listen('parseVerticalMargin', _applyNavBarGaps);

                        _setupUIElements();

                        if(_options.shareEl && _shareButton && _shareModal) {
                            _shareModalHidden = true;
                        }

                        _countNumItems();

                        _setupFullscreenAPI();

                        _setupLoadingIndicator();

                        ui.coverSize();
                    };
                    ui.coverSize = function(force){
                        var w = $(window).width(), h = $(window).height(), m = w>h?w:h, r1 = w/h, r2 = h/w, r = r1 > r2 ? r1 : r2;
                        if (pswp.currItem){
                            pswp.viewportSize = {
                                x : m,
                                y : m
                            }
                            force && pswp.zoomTo(pswp.currItem.initialZoomLevel,w/2,h/2);
                        }
                    }
                    ui.update = function() {
                        // Don't update UI if it's hidden
                        ui.coverSize();
                        if(_controlsVisible && pswp.currItem) {

                            ui.updateIndexIndicator();
                            _overlayUIUpdated = true;

                        } else {
                            _overlayUIUpdated = false;
                        }

                        if(!_shareModalHidden) {
                            _toggleShareModal();
                        }

                        if(!pswp.options.loop) {
                            var index = pswp.getCurrentIndex();
                            var topBar =    framework.getChildByClass(_controls, 'pswp__top-bar');
                            var _arrowLeft = framework.getChildByClass(topBar, 'pswp__button--arrow--left');
                            var _arrowright = framework.getChildByClass(topBar, 'pswp__button--arrow--right');
                            if (!index){
                                framework.addClass(_arrowLeft,'set-invisible');
                                framework.removeClass(_arrowright,'set-invisible');
                            } else if (index == pswp.items.length - 1 ) {
                                framework.removeClass(_arrowLeft,'set-invisible');
                                framework.addClass(_arrowright,'set-invisible');
                            } else {
                                framework.removeClass(_arrowLeft,'set-invisible');
                                framework.removeClass(_arrowright,'set-invisible');
                            }
                        }
                        _countNumItems();
                    };

                    ui.updateFullscreen = function(e) {

                        if(e) {
                            // some browsers change window scroll position during the fullscreen
                            // so PhotoSwipe updates it just in case
                            setTimeout(function() {
                                pswp.setScrollOffset( 0, framework.getScrollY() );
                            }, 50);
                        }

                        // toogle pswp--fs class on root element
                        framework[ (_fullscrenAPI.isFullscreen() ? 'add' : 'remove') + 'Class' ](pswp.template, 'pswp--fs');
                    };

                    ui.updateIndexIndicator = function() {
                        if(_options.counterEl) {
                            _indexIndicator.innerHTML = (pswp.getCurrentIndex()+1) +
                                                        _options.indexIndicatorSep +
                                                        _options.getNumItemsFn();
                        }
                    };

                    ui.onGlobalTap = function(e) {
                        e = e || window.event;
                        var target = e.target || e.srcElement;
                        if(_blockControlsTap) {
                            return;
                        }
                        if(e.detail && e.detail.pointerType === 'mouse') {
                            // close gallery if clicked outside of the image
                            if(_hasCloseClass(target)) {
                                pswp.close();
                                return;
                            }
                            if(framework.hasClass(target, 'pswp__img')) {
                                if(pswp.getZoomLevel() === 1 && pswp.getZoomLevel() <= pswp.currItem.fitRatio) {
                                    if(_options.clickToCloseNonZoomable) {
                                        pswp.close();
                                    }
                                } else {
                                    pswp.toggleDesktopZoom(e.detail.releasePoint);
                                }
                            }
                        } else {
                            // tap anywhere (except buttons) to toggle visibility of controls
                            if(_options.tapToToggleControls) {
                                if(!_controlsVisible) {
                                    ui.showControls();
                                }
                            }
                            // tap to close gallery
                            if(_options.tapToClose && (framework.hasClass(target, 'pswp__img') || _hasCloseClass(target)) ) {
                                pswp.close();
                                return;
                            }

                        }
                    };
                    ui.showControls = function() {
                        _controlsVisible = true;
                        if(!_overlayUIUpdated) {
                            ui.update();
                        }
                        framework.removeClass(_controls,'pswp__ui--hidden');
                    };
                    ui.supportsFullscreen = function() {
                        var d = document;
                        return !!(d.exitFullscreen || d.mozCancelFullScreen || d.webkitExitFullscreen || d.msExitFullscreen);
                    };
                    ui.getFullscreenAPI = function() {
                        var dE = document.documentElement,
                            api,
                            tF = 'fullscreenchange';

                        if (dE.requestFullscreen) {
                            api = {
                                enterK: 'requestFullscreen',
                                exitK: 'exitFullscreen',
                                elementK: 'fullscreenElement',
                                eventK: tF
                            };
                        } else if(dE.mozRequestFullScreen ) {
                            api = {
                                enterK: 'mozRequestFullScreen',
                                exitK: 'mozCancelFullScreen',
                                elementK: 'mozFullScreenElement',
                                eventK: 'moz' + tF
                            };
                        } else if(dE.webkitRequestFullscreen) {
                            api = {
                                enterK: 'webkitRequestFullscreen',
                                exitK: 'webkitExitFullscreen',
                                elementK: 'webkitFullscreenElement',
                                eventK: 'webkit' + tF
                            };
                        } else if(dE.msRequestFullscreen) {
                            api = {
                                enterK: 'msRequestFullscreen',
                                exitK: 'msExitFullscreen',
                                elementK: 'msFullscreenElement',
                                eventK: 'MSFullscreenChange'
                            };
                        }

                        if(api) {
                            api.enter = function() {
                                // disable close-on-scroll in fullscreen
                                _initalCloseOnScrollValue = _options.closeOnScroll;
                                _options.closeOnScroll = false;

                                if(this.enterK === 'webkitRequestFullscreen') {
                                    pswp.template[this.enterK]( Element.ALLOW_KEYBOARD_INPUT );
                                } else {
                                    return pswp.template[this.enterK]();
                                }
                            };
                            api.exit = function() {
                                _options.closeOnScroll = _initalCloseOnScrollValue;

                                return document[this.exitK]();

                            };
                            api.isFullscreen = function() { return document[this.elementK]; };
                        }
                        return api;
                    };
                }
        }
		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-product-gallery]',
					attrName:'data-module-product-gallery',
					moduleName:'module_product_gallery',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
