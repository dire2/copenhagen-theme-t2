(function(Core){ /* BOPIS & LOPIS PICKUP LOCATION LAYER */
    Core.register('module_pickup_location', function(sandbox){
    var $this,
        hasLocalNo,
        storeMap,
        storeInfo,

        pickupLocation = {
            currentLocation: null,
            storeList: [],
            localSelect: [],
            locationCodeWrap: false,
            storeAll: false,
            sortLt: false,
            sortNa: false,
            sortQt: false,
        },
        pickupshippingInfo = { isshippingInfo: false, storeInfo: null, itemRequest: null},

        pickupQuantity={
            isTaskQuantity:false,
            productPrice:0,
            newValue:0,
            min:0,
            max:0,
            size:0,
            quantity:{
                maxQuantity:1,
                msg:'개 까지만 구매가능 합니다.',
                quantityStateMsg:'상품의 수량이 없습니다.'
            }
        },

        totalPickupLocation = {},
        location_inventory = {},

        args={},

        $btn,
        serviceMenData={},
        reservationData={},
        arrInventoryList,
        itemRequest,
        confirmData,
        selectedProduct,
        hasLocalNo,
        needMakeMap,
        areaMap = new Map();


    // 내 주변순 위도 : 경도
    function calculateDistance (lat1, lon1, lat2, lon2) {

        var theta = lon1 - lon2;
        var dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));

        dist = Math.acos(dist);
        dist = rad2deg(dist);

        dist = dist * 60 * 1.1515;
        dist = dist * 1.609344; // 킬로미터 단위적용

        function deg2rad (deg) {
            return (deg * Math.PI / 180.0);
        }

        function rad2deg (rad) {
            return (rad * 180 / Math.PI);
        }

        return dist;
    }

    //사이즈 선택
    var sizeOptionToggle = function (size, skuID) {
        $this.find('.size-select-txt').text(size);
        $this.find('#reservation-size-title-area').removeClass('uk-active');
        $this.find('.uk-accordion-content').removeClass('uk-active');
        $this.find('.accordion-wrapper').animate({'height':0}, 300, "linear", function () {});

        $this.find('input[name="itemAttributes[size]"]').val(size);

        pickupQuantity.size = parseInt(size);
        vueContainer.pickupLocation(size, skuID);
    }

    // vue 컴포넌트 초기화
    var vueContainer = new Vue({
      el:'#idLocationSearch',
      data: {
        'location': pickupLocation,
        'shippinginfo': pickupshippingInfo,
        'quantity': pickupQuantity,
        'quantityNo': 0,

        'skuIdNe': '',
        'sizeIdNe': '',
        'localSelect': [],
        'stateList': [],
        'storeType': [],
        'sortCheck': 'allcheck',

        'dataList': [],
        'inventory': [],

        'currentSort': 'name',
        'currentSortDir': 'asc',

        'tag': false,
        'flag': false,
      },
      created: function () {
          var vm = this;

          // 오늘 날짜 location 대입
          var today = new Date(),
              yyyy = today.getFullYear(),
              mm = today.getMonth() + 1,
              dd = today.getDate();

          if (dd < 10) dd = '0' + dd
          if (mm < 10) mm = '0' + mm
          today = String(yyyy) + String(mm) + String(dd);


          sandbox.utils.promise({
            url: sandbox.utils.contextPath + '/cmsstatic/fulfillment-location-json/file/fulfill-location.json',//location-'+ today +'.json',
            type: 'GET'
          }).then(function (data) {
      			var $defer = $.Deferred();
      			if(data !== ''){
      				vm.dataList = JSON.parse(data);
      				$defer.resolve(data);
      			}else{
      				$defer.reject('location info is empty');
      			}
      			return $defer.promise();
          })
          .then(function(data){

            vm.dataList.filter(function (row, index) {

              vm.localSelect[index] = {
                active: true,
                'view': 'hide',
                'id': row.id,
                'name': row.name,
                'state': row.state,
                'latitude': row.latitude,
                'longitude': row.longitude,
                'externalId': row.externalId
               };

              // 사이즈 선택 시 해당 지역만 필터에 노출
              vm.localSelect.filter(function (row) {
                  var _id = row.id;
                  var _externalId = row.externalId;
                  vm.inventory.some(function (size) {
                    var _size = size.fulfillmentLocationId;
                    if (_id == _size && _externalId !== undefined) row.view = 'show';
                  });
                  if (row.view !== 'show') row.state = '';
              });
            });
            //중복처리
            vm.localSelect = _.unionBy(vm.localSelect, vm.localSelect, 'state');
      		})
          .fail(function (msg) {
            UIkit.notify(msg, {timeout:3000,pos:'top-center',status:'danger'});
          });
        },

        computed: {
          localSelect: function () {
            return this.localSelect;
          },
          storeList: function (s) {
            var vm = this;
            var flag = this.flag;

            vm.stateList = vm.storeType.concat(vm.localSelect);

            return this.dataList.filter(function (row, index) {

              var _id = row.id;
              var _state = row.state;
              var _externalId = row.externalId;

              if( _externalId !== undefined){

                row.locationTarget = calculateDistance(serviceMenData.latitude, serviceMenData.longitude, row.latitude, row.longitude);

                row.isAbleCod = (args.ableCod === 'true') ? true : false;
                row.isAblePickup = (args.ablePickup === 'true') ? true : false;

                return vm.inventory.some(function (size) {

                  var _size = size.fulfillmentLocationId;

                  if (_id == _size) {
                    row.quantityNo = size.quantityAvailable;
                  }

                  return vm.stateList.some(function (tag) {
                    if (!flag) {
                      return _id == _size && tag.active === true;
                    } else {
                      return _id == _size && tag.state == _state && tag.active === true;
                    }
                  });
                });
              }

            }).sort(function (a, b) {

              // 정렬기준 변경 시 스크롤 top
              $(".reservation-wrap .store-list, .Store_sortArea").scrollTop(0);

              if (vm.currentSortDir === 'desc') {
                if (a[vm.currentSort] < b[vm.currentSort]) return 1;
                if (a[vm.currentSort] > b[vm.currentSort]) return -1;
              } else {
                if (a[vm.currentSort] < b[vm.currentSort]) return -1;
                if (a[vm.currentSort] > b[vm.currentSort]) return 1;
              }
              return 0;
            });
          },

          storeType: function () {
            return this.storeType
          },

          localSelect: function () {
            return this.localSelect
          }
        },

      methods:{
        closeModal: function(){
          $this.find('.dim').removeClass('active');
          $('#store_info_Modal').hide();
        },
        pickupLocation: function (size, skuId) {
          var vm = this;

          this.skuIdNe = skuId;
          this.sizeIdNe = size;

          sandbox.utils.promise({
            url: sandbox.utils.contextPath + '/processor/execute/pickable_inventory',
            type: 'GET',
            data: {
              'skuId': skuId,
              'json': 'true',
              'fType': 'PHYSICAL_PICKUP',
              'jsonCache': 'false'
            }
          }).then(function (data) {
            vm.inventory = data;

            vm.dataList.filter(function (row, index) {
              vm.localSelect[index] = {
                active: true,
                'view': 'hide',
                'id': row.id,
                'name': row.name,
                'state': row.state,
                'latitude': row.latitude,
                'longitude': row.longitude,
                'externalId': row.externalId
               };
            });

            // 사이즈 선택 시 해당 지역만 필터에 노출
            vm.localSelect.filter(function (row) {
                var _id = row.id;
                var _externalId = row.externalId;
                vm.inventory.some(function (size) {
                    var _size = size.fulfillmentLocationId;
                    if (_id == _size && _externalId !== undefined) row.view = 'show';
                });
                if (row.view !== 'show') row.state = '';

            });

            vm.localSelect = _.unionBy(vm.localSelect, vm.localSelect, 'state');

            // 사이즈 변경 시 초기화
            vm.localSelect.filter(function (row) {
                row.active = true;
            });
            pickupLocation.storeAll = true;


            // 사이즈 변경 시 스크롤 top
            $(".reservation-wrap .store-list, .Store_sortArea").scrollTop(0);

          }).fail(function (msg) {

          });
        },

        sort: function (s) {
          switch (s) {
            case 'locationTarget': this.currentSortDir = 'asc'; break;
            case 'name': this.currentSortDir = 'asc'; break;
            case 'quantityNo': this.currentSortDir = 'desc'; break;
          }

          this.currentSort = s;
        },

        toggle: function (bools) {
          this.flag = JSON.parse(bools);
          var vm = this;

          if (this.flag) { // 지역선택

            pickupLocation.storeAll = false;

          } else { // 전체

            if(pickupLocation.storeAll) {
              vm.localSelect.filter(function (row) {
                  row.active = false;
              });
              pickupLocation.storeAll = false;

            } else {
              vm.localSelect.filter(function (row) {
                  row.active = true;
              });
              pickupLocation.storeAll = true;
            }

          }
        },

        pickupOrderQuantity: function (storeId, pickupType, maxQuantity) { // 수량 선택
          var $form = $this.find('form');
          var itemRequest = BLC.serializeObject($form);

            var _pickupType = (pickupType === 'lopis') ? true : false;

            itemRequest.isJustReservation = _pickupType
            itemRequest.titleName = (pickupType === 'lopis') ? '예약' : '픽업';

            $this.find('#isJustReservation').val(_pickupType);
            $this.find('#fulfillmentLocationId').val(storeId);

          itemRequest.fulfillmentLocationId = storeId;
          pickupshippingInfo.itemRequest = itemRequest;

          for (var i = 0; i < totalPickupLocation.length; i++) {
            if (totalPickupLocation[i].id == storeId) {
              pickupshippingInfo.storeInfo = totalPickupLocation[i];
            }
          }

          pickupQuantity.isTaskQuantity = true;
          pickupQuantity.quantity.maxQuantity = pickupQuantity;
          $this.find('.dim').addClass('active');
        },

        orderConfirmSubmit: function() {
          Method.executeOrderCountFinish();
        },

        orderCancel: function (status) {
          $this.find('.dim').removeClass('active');

          var _productPriceDefault = $this.find('#productPriceDefault').val();

          // 상품가격 초기화
          $this.find('#productPrice').val(_productPriceDefault);
          $this.find('#retailPrice').val(_productPriceDefault);

          switch (status) {
            case 'shippinginfo' :
              pickupshippingInfo.isShippingInfo = false;
              break;
            case 'quantity' :
              pickupQuantity.isTaskQuantity = false;
              break;
          }
        }
      }
    });

    // template
    Vue.component('pickup-location', {
      props: ['currentLocation', 'locationCodeWrap', 'storeAll', 'storeList', 'dp', 'st', 'ls', 'sort', 'toggle', 'sortLt', 'sortNa', 'sortQt'],
      template:'<div>\
          <div class="Store_sortArea">\
      			<dl>\
      				<dt>정렬방식</dt>\
  						<dd class="btn-location-self mobile-only" v-bind:class="{active:sortLt}">\
                <span class="input-radio">\
                  <input type="radio" id="MyLocation_Sort" name="Store_SortName" />\
                  <label for="MyLocation_Sort" v-on:click="findLocation">\
                    <i class="brz-icon-radio"></i>\
                    내 주변 순\
                  </label>\
                </span>\
              </dd>\
  						<dd class="store-name" v-bind:class="{active:sortNa}">\
                <span class="input-radio">\
                  <input type="radio" id="Store_Sort" name="Store_SortName" checked />\
                  <label for="Store_Sort" v-on:click="sort(\'name\')">\
                    <i class="brz-icon-radio"></i>매장명 순\
                  </label>\
                </span>\
              </dd>\
  						<dd class="prd-cnt" v-bind:class="{active:sortQt}" >\
                <span class="input-radio">\
                  <input type="radio" id="quantity_Sort" name="Store_SortName" />\
                  <label for="quantity_Sort" v-on:click="sort(\'quantityNo\')">\
                    <i class="brz-icon-radio"></i>\
                    재고 순\
                  </label>\
                </span>\
              </dd>\
            </dl>\
            <dl class="code-wrap_radio">\
              <dt>지역선택</dt>\
							<dd>\
                <span class="input-checkbox">\
                <input type="checkbox" id="storeAll" name="storelocalAll" v-model="storeAll" />\
                <label for="storeAll" id="storeAll" v-on:click="toggle(\'false\')"><i class="brz-icon-checkbox"></i>전체</label>\
                </span>\
							</dd>\
							<dd v-for="(tag, index) in ls" v-if="tag.view === \'show\'">\
                <span class="input-checkbox">\
                <input type="checkbox" name="storelocal" v-model="tag.active" v-bind:id="\'storelocal\' + index" />\
                <label v-bind:for="\'storelocal\' + index" v-on:click="toggle(\'true\')"><i class="brz-icon-checkbox"></i>{{ tag.state }}</label>\
                </span>\
							</dd>\
      			</dl>\
      		</div>\
          <div class="dim"></div>\
          <div class="store-list">\
            <!--/* 상품예약 서비스 상점 리스트 */-->\
            <div class="shipping-body">\
              <template v-if="dp.length">\
                <div v-for="store in dp" class="shipping-list" v-bind:data-locationid="store.id">\
                  <div class="column column-addr" v-on:click="shippinginfo(store.id)" style="cursor:pointer;">\
                    <h2 class="tit">{{store.name}}</h2>\
                    <dl class="address-wrap">\
                      <dd class="addr">({{store.zip}}) {{store.address1}} {{store.address2}}</dd>\
                      <dd class="addr">{{store.phone}}</dd>\
                    </dl>\
                  </div>\
                  <div class="column column-reserve">\
                    <span class="quantity" v-if="store.quantityNo <= 10">{{store.quantityNo}}</span>\
                    <span class="quantity" v-if="store.quantityNo > 10">10+</span>\
                    <button v-on:click="runOrderTask" v-bind:data-locationid="store.id" v-bind:data-maxquantity="store.quantityNo" data-pickup-type="bopis" class="pickup-apply btn-link mini">픽업 신청</button>\
                  </div>\
                </div>\
              </template>\
              <template v-else>\
                <div class="less-items uk-text-center">\
                  <i class="icon-search color-less x2large"></i><br />\
                  해당 지역의 매장정보가 없습니다.\
                </div>\
              </template>\
            </div>\
          </div>\
        </div>',
      methods:{
        currentLocation:function(e){
          e.preventDefault();
          /* 위치기반 서비스 개발 */
          this.$emit('currentLocation');
        },
        runOrderTask:function(e){

            e.preventDefault();

            var storeId = e.target.getAttribute('data-locationid');
            var pickupType = e.target.getAttribute('data-pickup-type');
            var maxQuantity = e.target.getAttribute('data-maxquantity');

            pickupQuantity.min = 1;
            pickupQuantity.newValue = 1;
            pickupQuantity.max = maxQuantity;

            for (var i = 0; i < this.dp.length; i++) {
                if (this.dp[i].id == storeId) {
                    pickupshippingInfo.storeInfo = this.dp[i];
                }
            }

            pickupQuantity.productPrice = sandbox.rtnPrice($this.find('#productPriceDefault').val());

            this.$emit('ordertask', storeId, pickupType, maxQuantity);
        },

        locationCodeClose: function (e) {
          e.preventDefault();
          pickupLocation.locationCodeWrap = false;
        },

        locationSelect: function (e) {
          e.preventDefault();
          pickupLocation.locationCodeWrap = true;
        },
        findLocation:function(e){
          // 사용자 위치 정보 조회 (재고 보유 매장의 위치 정보를 얻어와 내주변 정렬하기 위한 기능)
          Core.Loading.show();

          var vm = this;
          var positionOpt = {
              enableHighAccuracy : true,
              maximumAge : 5000,
          };

          if (!navigator.geolocation) {

              UIkit.notify("내 위치 정보를 찾을 수 없습니다.", {timeout:3000, pos:'top-center',status:'warning'});
              //showPositionError({"error":"ERROR - Geolocation is not supported by this agent"});
              Core.Loading.hide();
          }

          function success(position){
            if(serviceMenData['latitude'] != position.coords.latitude || serviceMenData['longitude'] != position.coords.longitude) {
                serviceMenData['latitude']  = position.coords.latitude;  // API RESERVE
                serviceMenData['longitude'] = position.coords.longitude; // API RESERVE
            }

            pickupLocation.currentLocation = serviceMenData;
            // if(isOnLoad===false) {
            //     loadStoreIfRadioChange();
            // }
            vm.sort('locationTarget');
            Core.Loading.hide();
          }

          function error(){
            Core.Loading.hide();
            //에러 시 radio의 checked 를 재고명 radio로 변경
            vm.sort('name');
            $("#MyLocation_Sort").attr('checked',false)
            $("#Store_Sort").attr('checked',true)

            alert('위치정보를 이용할 수 없습니다.')
            //UIkit.notify("내 위치 정보를 찾을 수 없습니다..", {timeout:3000, pos:'top-center',status:'warning'});
          }

          navigator.geolocation.getCurrentPosition(success, error);
        },
        shippinginfo:function(storeId){
          var $form = $this.find('form');
          var itemRequest = BLC.serializeObject($form);

          itemRequest.fulfillmentLocationId = storeId;
          pickupshippingInfo.itemRequest = itemRequest;

          for (var i = 0; i < this.dp.length; i++) {
              if (this.dp[i].id == storeId) {
                  pickupshippingInfo.storeInfo = this.dp[i];
              }
          }

          var storeLat = pickupshippingInfo.storeInfo.latitude;
          var storeLong = pickupshippingInfo.storeInfo.longitude;

          var Setpositon = new naver.maps.LatLng (storeLat, storeLong)

          //맵크기 조정
          var $thisWidth = $this.width();
          storeMap.setSize(new naver.maps.Size($thisWidth,200));
          $(window).resize(function(){ //with 반응형
            var $thisWidth = $this.width();
            storeMap.setSize(new naver.maps.Size($thisWidth,200));
          });



          //위도, 경도 값으로 지도 마커 찍어내기
          var marker = new naver.maps.Marker({
            map:storeMap,
            position:Setpositon,
            title:pickupshippingInfo.storeInfo.longitude.name,
            icon:"https://image.converse.co.kr/assets/images/marker-converse.png",
            zoom: 10
          });

          //팝업닫으면 찍었던 마크 삭제
          $('#store_info_Modal .uk-close').click(function(){
            marker.setMap(null);
          })

          //맵 해당 매장 포지션 Center
          storeMap.setCenter(Setpositon);
          storeMap.setZoom(10);

          $this.find('.dim').addClass('active');
          $("#store_info_Modal").show();
          $("#store_info_Modal .address-wrap").scrollTop(0);
          pickupshippingInfo.isShippingInfo = true;
        }
      }
    });

    Vue.component('pickup-shippinginfo', {
      props:['isshippingInfo', 'storeInfo', 'itemRequest'],
      template:'<div class="contents store" v-bind:class="{active:isshippingInfo}" v-if="storeInfo != null">\
            <p>{{storeInfo.name}}</p>\
            <dl class="address-wrap">\
              <dt class="addr-type">주소</dt>\
              <dd class="addr">({{storeInfo.zip}}) {{storeInfo.address1}} {{storeInfo.address2}}</dd>\
              <dt class="addr-type">전화번호</dt>\
              <dd class="addr">{{storeInfo.phone}}</dd>\
              <dt class="addr-type">영업시간</dt>\
              <dd class="addr">{{ storeInfo.additionalAttributes["caTimeTxt"] == null || storeInfo.additionalAttributes["caTimeTxt"] == " " ? "매장으로 연락 주시기 바랍니다."  : storeInfo.additionalAttributes["caTimeTxt"] }}</dd>\
              <dt class="addr-type">휴무일</dt>\
              <dd class="addr">{{ storeInfo.additionalAttributes["holidayTxt"] == null || storeInfo.additionalAttributes["holidayTxt"] == " " ? "별도 휴무일이 없습니다." : storeInfo.additionalAttributes["holidayTxt"] }}</dd>\
            </dl>\
          </div>',
      methods: {
        cancel: function () {
          this.$emit('cancel', 'shippinginfo');
        }
      }
    });

    Vue.component('pickup-quantity', {
      props:['isTaskQuantity', 'productPrice', 'quantity', 'cf', 'newValue', 'max', 'min', 'itemRequest'],
      template:'<article id="order-count-select" class="order-count" v-bind:class="{active:isTaskQuantity}" v-bind:data-component-quantity="quantity">\
          <a class="uk-close order-count-select-close" v-on:click="cancel"></a>\
          <h2 class="title">수량 선택</h2>\
          <div class="body">\
            <div class="count">\
              <div class="count-box">\
                <input type="number" v-model="newValue" class="label" readonly />\
                <button type="button" id="count-plus" v-on:click="plusBtn" class="plus"><i class="icon-plus"></i><span>1개씩 추가</span></button>\
                <button type="button" id="count-minus" v-on:click="minusBtn" class="minus"><i class="icon-minus"></i><span>1개씩 삭제</span></button>\
              </div>\
              <div class="price-box">{{productPrice}}</div>\
            </div>\
            <p class="msg" v-if="newValue == max">{{max + quantity.msg}}</p>\
            <!--/* <button type="button" class="qty-selected btn-link width-max large" v-on:click="selected">선택완료</button>*/-->\
            <a href="javascript:;" class="reservation-apply btn-link width-max large" data-click-area="inventory" v-on:click="submit">픽업하기</a>\
          </div>\
        </article>',
      methods: {
        plusMinusFun: function (operator, num) {

          var productPrice = $this.find('#productPrice').val();
          var productPriceDefault = $this.find('#productPriceDefault').val();

          if (operator == 'plus') {
            var count = parseInt(productPrice) + parseInt(productPriceDefault);
          } else {
            var count = parseInt(productPrice) - parseInt(productPriceDefault);
          }

          $this.find('#productPrice').val(count);
          $this.find('#retailPrice').val(count);
          $this.find('#quantity').val(num);

          pickupshippingInfo.itemRequest.quantity = num;
          pickupshippingInfo.itemRequest.retailprice = sandbox.rtnPrice(String($this.find('#productPrice').val()));

          this.productPrice = sandbox.rtnPrice(String($this.find('#productPrice').val()));
        },
        plusBtn: function () {
          if(this.max == undefined || (this.newValue < this.max)) {
            this.newValue = this.newValue + 1;
            this.plusMinusFun('plus', this.newValue);
          }
        },
        minusBtn: function () {
          if((this.newValue) > this.min) {
            this.newValue = this.newValue - 1;
            this.productPrice = this.productPrice * 2;
            this.plusMinusFun('minus', this.newValue);
          }
        },
        selected: function (e) {
            e.preventDefault();
            pickupshippingInfo.itemRequest.size = pickupQuantity.size;
            this.$emit('selected', 1);
            this.$emit('submit');
        },
        submit:function(e){
            e.preventDefault();

            this.$emit('selected', 1);
            this.$emit('submit');
        },
        cancel: function () {
          //pickupQuantity.isTaskQuantity = false;
          this.$emit('cancel', 'quantity');
        }
      },
      created: function() {

      }
    });

    var Method = {
        updateAreaAgencyCnt:function(hasLocalNo, areaMap){
            try {
                var countVal,
                    areaId;

                if(hasLocalNo==false) {//지역미선택
                    $this.find('[data-area-info]').each(function(){
                        areaId = $(this).attr('value');
                        countVal = areaMap[areaId];
                        if(typeof(countVal)==="undefined") {
                            countVal = "0";
                          }
                        $this.find('#area-branch-cnt-' + areaId).text(countVal);
                    });
                } else { //지역선택시 해당 지역만 초기화
                    var areaList = [];
                    for(var areaId in areaMap){
                        $this.find('#area-branch-cnt-' + areaId).text(areaMap[areaId]);
                    }
                }
            } catch (e) {
                //alert("지역 초기화중 에러 : "+ e);
            }
        },

        executeOrderCountFinish:function(){
            Core.getModule('module_header').setLogin(function(){
                var $form = $this.find('form');

                // form value
                var itemRequest = BLC.serializeObject($form);

                sandbox.setLoadingBarState(true);

                BLC.ajax({
                    url:$form.attr("action"),
                    type:"POST",
                    dataType:"json",
                    data: itemRequest
                },function (data) {
                    if(data.error){
                        sandbox.setLoadingBarState(false);
                        UIkit.modal.alert(data.error);
                        UIkit.modal('#common-modal').hide();
                    }else{
                        Core.Loading.show();
                        endPoint.call( 'buyNow', itemRequest);
                        _.delay(function(){
                            window.location.assign(sandbox.utils.contextPath +'/checkout' );
                        }, 500);
                    }
                }).fail(function(msg){
                    if(commonModal.active) commonModal.hide();
                    Core.Loading.hide();
                    if(msg !== '' && msg !== undefined){
                        UIkit.notify(msg, {timeout:3000,pos:'top-center',status:'warning'});
                    }
                });
            });
        },

        moduleInit:function () {
            $this = $(this);
            args = arguments[0];
            var skuData;

            if(!window.naver){
              $.getScript("https://openapi.map.naver.com/openapi/v3/maps.js?ncpClientId=" + args.mapkey, function(){
                //지도생성
                storeMap = new naver.maps.Map('map_area', {
                    center: new naver.maps.LatLng(37.3595704, 127.105399),
                    zoom: 10
                });
              });
            };

            var currentDate = new Date();
            var reservationModal = UIkit.modal('#reservation-modal', {center:true});
            var disabledDays = [];
            var AllskuData =  sandbox.getComponents('component_product_option', {context:$(document)}, function(i){
                skuData = this.getDefaultSkuData();
            })
            var radioComponent = sandbox.getComponents('component_radio', {context:$this}, function(i) {
                var _self = this;
                var INDEX = i;

                this.addEvent('change', function(size){
                    var skuID = 0;
                    var _this = this;
                    skuData.some(function (size) {
                        for(var a = 0; a < size.selectedOptions.length; a++){
                          if(size.selectedOptions[a] == $(_this).data('id')){
                            skuID = size.skuId;
                            return skuID;
                          }
                        }
                    });
                    sizeOptionToggle(size, skuID);
                });

                // PDP 사이즈값 받아오기
                var pdp_option_size = $('.option-wrap .hidden-option[name="itemAttributes[size]"]').val();
                if (pdp_option_size) {
                    this.trigger(pdp_option_size, pdp_option_size);
                }
                // PDP COLOR 값 받아오기
                var pdp_option_color = $('.option-wrap .hidden-option[name="itemAttributes[Color]"]').val();
                if (pdp_option_color) {
                  $('.reservation_product input[name="itemAttributes[Color]"]').val(pdp_option_color);
                }else{
                  pdp_option_color = $('.option-wrap [data-attribute-name=Color] input').attr('data-value');
                  $('.reservation_product input[name="itemAttributes[Color]"]').val(pdp_option_color);
                }
            });
        }
    }

    return {
        init:function(){
            sandbox.uiInit({
                selector:'[data-module-pickup-location]',
                attrName:'data-module-pickup-location',
                moduleName:'module_pickup_location',
                handler:{context:this, method:Method.moduleInit}
            });
        }
    }
});

})(Core);
