(function(Core){
	'use strict';

	Core.register('module_map', function(sandbox){
		var storeList = null,
			$this,
			$infoViewContainer = null,
			map = null,
			markers = [],
			infoWindows = [],
			filterQuery = [],
			currentLocation = '',
			currentStoreIndex = 0,
			primaryStore,
			isAjax = false,
			reqPage = 1;
		/*
			store-info-view 에 추가되는 dom object

			<h2 class="tit">명동</h2>
			<span class="address">서울 영등포구 여의도동</span>
			<span class="phonenum">070-0000-0000</span>
			<dl class="info">
				<dt class="key">운영시간</dt>
				<dd class="value">평일 10:00, 주말 11:00</dd>
				<dt class="key">매장정보</dt>
				<dd class="value">네이버 지도 API v3는 JavaScript 형태로 제공되는 NAVER 지도 플랫폼으로써</dd>
			</dl>
			<button class="close-btn"><i class="icon-delete_thin"></i></button>
		*/

		/**************************

			매장검색
			매장이름, 지역검색 : _find=지하철&_search=name&_condition=like
			필터 : _find={{매장타입}}&_search=storeType&_condition=or||and

			admin에서 재고위치/대리점의 추가속성에 storeType, icon은 각각, 검색필터와 마커의 아이콘의 클래스를 입력하는 속성이다.

		***************************/

		var makeMarkerIcon = function(storeData, storeVal){

			var icon = {
				size:new naver.maps.Size(32, 57),
				anchor:new naver.maps.Point(12, 37),
				origin:new naver.maps.Point(0, 0)
			}

			if(storeData.additionalAttributes && storeData.additionalAttributes.icon && storeData.additionalAttributes.icon !== ''){
				icon.content = '<i class="icon_map_marker '+storeData.additionalAttributes.icon+'"></i>';
			}else{
				//icon.url = '/assets/images/marker-'+storeVal+'.png';
				icon.url = '/assets/images/marker-'+storeVal+'.png';
			}
			return icon;
		}

		var submitSearchMap = function($form){
			var searchQuery = $form.serializeArray(),
				searchQueryString = '',
				filterParams = location.pathname + '?';
			filterQuery = [];
			searchQuery.forEach(function(row){
				row.value = encodeURIComponent(row.value);
				if (row.name == '_find' || row.name == '_search' || row.name == '_condition' || row.name == 'pageSize') {
					searchQueryString += (row.name + '=' + row.value + '&');
				} else {
					row.value != 'all' && filterQuery.push(row.value);
				}
			});
			if(searchQueryString !== ''){
				filterParams += searchQueryString;
			}
			if (filterQuery.length) {
				filterParams += ('_find=' + filterQuery.join(',') + '&_search=storeType&_condition=like');
			}
			location.href = filterParams.replace(/[?|&]$/, '');
		}

		var Method = {
			moduleInit:function(){
				var args = arguments[0];
				$this = $(this);
				$infoViewContainer = $(this).find(args['data-module-map'].infoview);
				storeList = args['data-store-list'];
				var storeType = $this.attr('data-store-type');

				//map 초기화
				var firstLatitude = (storeList[0]) ? storeList[0].latitude:37.3595953;
				var firstLongitude = (storeList[0]) ? storeList[0].longitude:127.1053971;
				var currentParams = sandbox.utils.getQueryParams(location.href);
				var arrCurrentFilters = [];
				if(currentParams.hasOwnProperty('_find') && currentParams['_find'] instanceof Array){
					arrCurrentFilters = currentParams['_find'][1].split(',');
				}else{
					currentParams = null;
					arrCurrentFilters = null;
				}

				map = new naver.maps.Map(args['data-module-map'].target, {
					center:new naver.maps.LatLng(firstLatitude, firstLongitude),
					zoom:9
				});

				var searchField = sandbox.getComponents('component_searchfield', {context:$this}, function(){
					/*this.addEvent('beforeSubmit', function(){
						var val = arguments[0];
						$('input[name=_find]').val(val);
					});*/
					this.addEvent('searchEmpty', function($form){
						submitSearchMap($form);
					});

					this.addEvent('searchKeyword', function($form){
						submitSearchMap($form);
					});
				});

				var searchCheckBox = sandbox.getComponents('component_checkbox', {context:$this}, function(i, dom){
					if (this.getThis().prop('checked')){
						if(this.getThis()[0].value !== 'all' || this.getThis()[0].value !== '') filterQuery.push(this.getThis()[0].value);
					}
					this.addEvent('change', function(val){
						var index = filterQuery.indexOf(this.value);
						if(index === -1){
							if(this.value !== 'all') filterQuery.push(this.value);
						}else{
							filterQuery.splice(index, 1);
						}
					});

					if(arrCurrentFilters !== null && arrCurrentFilters.indexOf(encodeURIComponent(this.getThis().val())) > -1){
						this.getThis().prop('checked', true);
						this.getThis().trigger('change');
					}
				});

				var locationSelectbox = sandbox.getComponents('component_select', {context:$this}, function(i, dom){
					this.addEvent('change', function(val){
						currentLocation = val;
					});
				});

				$this.on('click', '[data-store-id]', function(){
					Method.mapEvent($(this).parent().index());
					return false;
				});
				$this.on('click', '.close-btn', function(){
					Method.mapEvent(currentStoreIndex);
					return false;
				});
				$(".search-result").on('scroll',function(){
				    var curHeight = (this.scrollTop+this.clientHeight);
				    var totHeight = this.scrollHeight;
				    if(totHeight - curHeight >= 0 && totHeight - curHeight < 100 && reqPage) {
				    	Method.addMap();
				    	isAjax = true;
				    }
				});
				$(window).on('scroll',function(e){
					var _this = $(this),
						result = $(".search-result");
					if (result.css('overflow') == 'visible') {
						var screenBotton = _this.scrollTop() + _this.height(),
							resultBotton = result.offset().top + result.height();
						if (resultBotton <= screenBotton && reqPage) {
							Method.addMap();
							isAjax = true;
						}
					}
				});
				Method.updateCheckAll();

				// 매장찾기 전체 체크시
				$(this).find('input.check-all-store[type="checkbox"]').on('change', Method.changeAllCheck);

				// 아이템 체크박스 선택시
				$(this).find('input.check-item-store[type="checkbox"]').on("change", Method.changeItemCheck);

				Method.mapInit(storeType);
			},
			// 매장찾기 전체 체크 처리
			changeAllCheck:function(e){
				e.preventDefault();
				var isCheck = $(this).prop('checked');
				$('input.check-item-store[type="checkbox"]').each( function(){
					if(isCheck == true && !$(this).prop('checked')){
						$(this).prop('checked', isCheck ).trigger('change');
					}
					if(isCheck == false && $(this).prop('checked')){
						$(this).prop('checked', isCheck ).trigger('change');
					}
				});
			},
			// 아이템 체크박스 선택시
			changeItemCheck:function(e){
				var isCheck = $(this).prop('checked');
				if( isCheck ){
					$(this).parent().addClass('checked');
				}else{
					$(this).parent().removeClass('checked');
				}
				setTimeout( function(){
					$this.find('.btn_search').trigger('click'); 
				}, 500);
				Method.updateCheckAll();
			},
			// 아이템 체크박스 변경시 전체 선택 체크박스 상태처리
			updateCheckAll:function(){
				if($('input.check-item-store[type="checkbox"]').length == $('input.check-item-store[type="checkbox"]:checked').length ){
					$('input.check-all-store[type="checkbox"]').prop( 'checked', true);
				}else{
					$('input.check-all-store[type="checkbox"]').prop( 'checked', false);
				}
			},
			getStoreList:function(id){
				for(var key in storeList){
					if(storeList[key].id == id){
						return {'info':storeList[key], 'index':key}
					}
				}
			},
			showInfoDetail:function(data){
				var template = Handlebars.compile($('#map-store-info').html())(data);
				$infoViewContainer.empty().append(template);

				$this.find('.search-result-wrap').addClass('set-active');
			},
			mapInit:function(storeType){
				//store 위도, 경도 값으로 지도 마커 찍어내기
				for (var i=0; i<storeList.length; i++) {
					var curStoreType = storeList[i].additionalAttributes.brandName;
					var position = new naver.maps.LatLng(storeList[i].latitude, storeList[i].longitude);
					var marker = new naver.maps.Marker({
						map:map,
						position:position,
						title:storeList[i].name,
						icon:makeMarkerIcon(storeList[i]),
						zIndex:100
					});

					var infoWindow = new naver.maps.InfoWindow({
						content: '<div style="width:150px;text-align:center;padding:10px;">'+ storeList[i].name +'</div>'
					});

					markers.push(marker);
					infoWindows.push(infoWindow);
				}

				for (var i=0, ii=markers.length; i<ii; i++) {
					naver.maps.Event.addListener(markers[i], 'click', getClickHandler(i));
				}

				function getClickHandler(seq){
					return function(){
						Method.mapEvent(seq);
					}
				}
			},
			mapEvent:function(seq){
				var marker = markers[seq], infoWindow = infoWindows[seq];
				if (infoWindow.getMap()) {
					infoWindow.close();
					$this.find('.search-result-wrap').removeClass('set-active');
				} else {
					infoWindow.open(map, marker);

					var objStoreInfo = Method.getStoreList($(this).attr('data-store-id'));
					Method.showInfoDetail(storeList[seq]);
					currentStoreIndex = seq;

					//선택한 store 좌표로 이동
					map.setCenter(new naver.maps.LatLng(storeList[seq].latitude, storeList[seq].longitude));
					map.setZoom(10);
				}
			},
			addMap:function() {
				if(isAjax == false && reqPage) {
					console.log(reqPage);
					reqPage = ++reqPage;

					var queryParams = sandbox.utils.getQueryParams(location.href, 'array');
					var url = (queryParams.length > 0) ? '/store?' + queryParams.join('&') : '/store';
					Core.Utils.ajax(url, 'GET',{'page':reqPage}, function(data) {
						var mapData = sandbox.rtnJson($(data.responseText).find('[data-module-map]').attr('data-store-list')) || [];
						if( data ){
							setTimeout(function(e){
								isAjax = false;
							}, 100);

							if($(data.responseText).find(".search-list").length === 0){
								reqPage = undefined;
							} else {
								mapData.forEach(function(a, b, c){
									storeList.push(a);
								});

								$(data.responseText).find(".search-list").each(function(){
									$(".search-result").append($(this));
								});

								Method.reInitMap();
							}
						}else{
							UIkit.notify(data.errorMsg, {timeout:3000,pos:'top-center',status:'danger'});
						}
					},true);
				}
			},
			reInitMap:function(){
				markers = [];
				infoWindows = [];

				Method.mapInit();
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-map]',
					attrName:['data-module-map', 'data-store-list'],
					moduleName:'module_map',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
