(function(Core){
	Core.register('module_category', function(sandbox){
		var $that;
		var arrViewLineClass=['uk-width-medium-1-3', 'uk-width-large-1-2', 'uk-width-large-1-3', 'uk-width-large-1-4', 'uk-width-large-1-5'];
		var Method = {
			$reszieEvent : 'orientationchange' in window ? 'orientationchange.flyout' : 'resize.flyout',
      $productTileClass : '.product-tile',
      $productWishClass : '.product-wish',
      $productImgClass : '.product-img',
      $productUrlClass : '.product-url',
      $wishActionClass : '.wish-action',
      $wishActive : 'set-active',
      $wishChange : 'set-changed',
      $wishSuccess : 'set-wish-success',
      $colorClass : '.swatch-color',
      $colorMore : 'data-swatch-count',
      $colorSeleted : 'selected',
      $lazyLoadInstance : null,
			moduleInit:function(){
        //Method.addEventWishAction();
        Method.imgLazyLoad();
        Method.addEventColorAction();
        Method.addEventMoreColor();

				$this = $(this);
				var $filterWrap = $this.find('.filter-wrapper');
				//uk-width-medium-1-3 uk-width-large-1-3
				//view Length 2:maxLen
				// $this.find('.select-view > button').click(function(e){
				// 	e.preventDefault();
				//
				// 	if(!$(this).hasClass('active')){
				// 		$(this).addClass('active').siblings().removeClass('active');
				// 		$this.find('[data-component-categoryitem]').parent()
				// 		.removeClass(arrViewLineClass.join(' '))
				// 		.addClass('uk-width-large-1-'+$(this).attr('data-value'));
				// 		$this.find('[data-component-categoryitem1]').parent()
				// 		.removeClass(arrViewLineClass.join(' '))
				// 		.addClass('uk-width-large-1-'+$(this).attr('data-value'));
				// 		$this.find('[data-component-categoryitem2]').parent()
				// 		.removeClass(arrViewLineClass.join(' '))
				// 		.addClass('uk-width-large-1-'+$(this).attr('data-value'));
				//
				// 		//category lineSize
				// 		sandbox.getModule('module_pagination').setLineSize($(this).attr('data-value'));
				// 	}
				// });
				//
				// $this.find('.filter-btn').click(function(e){
				// 	e.preventDefault();
				//
				// 	if($filterWrap.hasClass('pc-only')){
				// 		$filterWrap.removeClass('pc-only');
				// 	}else{
				// 		$filterWrap.addClass('pc-only');
				// 	}
				//
				// });
			},
      moreColorCount : function(){
        $('.swatch-list-color').each(function(){
          var el = $(this),
          morePrint = el.closest(Method.$productTileClass).find('[' + Method.$colorMore + ']'),
          boxSize = el.width(),
          hiddenColorSize = 0;
          el.find(Method.$colorClass).removeClass('hidden').each(function(){
            var color = $(this);
            if (boxSize < color.position().left + color.width()){
              color.addClass('hidden');
              hiddenColorSize++;
            } else {
              color.removeClass('hidden');
            }
          });
          if (hiddenColorSize){
            morePrint.removeClass('hidden').attr(Method.$colorMore, hiddenColorSize);
          } else {
            morePrint.addClass('hidden');
          }
        });
      },
      changeImage : function(tile, data){
        tile.find(Method.$productImgClass).each(function(eq){
          $(this).find('img').attr('src',data[eq]);
        });
      },
      addEventMoreColor : function(){
        $(window).on(Method.$reszieEvent + Method.$colorClass, function(){
          Method.moreColorCount();
        }).trigger(Method.$reszieEvent + Method.$colorClass);
      },
      removeEventMoreColor : function(){
        $(window).off(Method.$reszieEvent + Method.$colorClass);
      },
      addEventColorAction : function(){
        $(document).on('click' + Method.$colorClass, Method.$colorClass,function(){
          var anchor = $(this),
              data = anchor.data(),
              productTile = anchor.closest(Method.$productTileClass);

          if (productTile.length) {
            anchor.addClass(Method.$colorSeleted);
            anchor.siblings('.' + Method.$colorSeleted).removeClass(Method.$colorSeleted);
            productTile.find(Method.$productWishClass).data('productId',data.productId);
            productTile.find(Method.$productUrlClass).attr('href',anchor.attr('href'));
            Method.changeImage(productTile, data.productImage);
            return false;
          }
        }).on('mouseenter' + Method.$colorClass, Method.$colorClass,function(){
          var anchor = $(this),
              data = anchor.data(),
              productTile = anchor.closest(Method.$productTileClass);

          productTile.length && Method.changeImage(productTile, data.productImage);
        }).on('mouseleave' + Method.$colorClass, Method.$colorClass,function(){
          var anchor = $(this),
              productTile = anchor.closest(Method.$productTileClass),
              activeAnchor = productTile.find(Method.$colorClass + '.' + Method.$colorSeleted),
              activeData = activeAnchor.data();

          productTile.length && Method.changeImage(productTile, activeData.productImage);
        });
      },
      removeEventColorAction : function(){
        $(document).off('click' + Method.$colorClass, Method.$colorClass)
        .off('mouseenter' + Method.$colorClass, Method.$colorClass)
        .off('mouseleave' + Method.$colorClass, Method.$colorClass);
      },
      // addEventWishAction : function(){
      //   $(document).on('click' + Method.$wishActionClass, Method.$wishActionClass,function(){
      //     var el = $(this),
      //       action = el.data('wish-type'),
      //       product = el.closest(Method.$productTileClass),
      //       parent = product.find(Method.$productWishClass),
      //       tileMain = product.find('.product-tile-main');
      //     if (parent.hasClass(Method.$wishChange)) return false;
      //     parent.addClass(Method.$wishChange);
      //     el.removeClass(Method.$wishActive).siblings().addClass(Method.$wishActive);
      //     action == 'add' && tileMain.addClass(Method.$wishSuccess);
      //
      //     setTimeout(function(){
      //       parent.removeClass(Method.$wishChange);
      //       action == 'add' && (tileMain.removeClass(Method.$wishSuccess), window.headModule.addWish());
      //     },426);
      //     return false;
      //   });
      // },
      removeEventWishAction : function(){
        $(document).off('click' + Method.$wishActionClass, Method.$wishActionClass);
      },
      imgLazyLoad : function(){
        if (!LazyLoad) {
          return false;
        }
        Method.imgLazyInit();
        Method.lazyLoadInstance = new LazyLoad({
          elements_selector: '.lazy',
          class_loading: 'lazy-loading',
          callback_error:function(img){
            img = $(img);
            img.attr('src', img.data('error'));
          }
        });
        $(window).on(Method.$reszieEvent + '.lazy', function(){
            setTimeout(function(){
              Method.imgLazyInit();
              Method.lazyLoadInstance.update();
            });
        });
      },
      imgLazyInit : function(){
        var lazyImg = $('.lazy').not('.set-lazy');
        lazyImg.each(function(a,b){
          var img = $(this),
            src = img.attr('src');
            img.data('error',src).addClass('set-lazy');
        });
      }
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-category]',
					attrName:'data-module-category',
					moduleName:'module_category',
					handler:{context:this, method:Method.moduleInit}
				});
			},
			destory : function(){
        Method.removeEventWishAction();
        Method.removeEventColorAction();
        Method.removeEventMoreColor();
      }
		}
	});
})(Core);
