(function(Core){
	Core.register('module_promotion', function(sandbox){
		var endPoint;
		var Method = {
			$that:null,
			$form:null,
			$errorMessage:null,

			moduleInit:function(){
				/*
					@replaceTarget : 결과값이 들어갈 dom
					@errorMessageTarget : error message 가 들어갈 dom
				*/
				$.extend(Method, arguments[0]);

				var $this = $(this);
				Method.$that = $this;
				Method.$form = $this.find("form.promo-form");
				Method.$errorMessage = $this.find(Method.errorMessageTarget);
				endPoint = Core.getComponents('component_endpoint');

				if( Method.$form.length < 1 ){
					return;
				}

				$(this).find("button[type='submit']").on("click", function(e){
					e.preventDefault();
					Method.submitCode();
				});

				$(this).find(".promo-list .btn-delete").on("click", function(e){
					e.preventDefault();
					Method.removeCode( $(this).attr("href") );
				});

			},
			removeCode:function(url){
				sandbox.setLoadingBarState(true);
				BLC.ajax({
					url: url,
					type: "GET"
				}, function(data) {
					if (data.error && data.error == "illegalCartOperation") {
						UIkit.modal.alert(data.exception);
						sandbox.setLoadingBarState(false);
					} else {
						window.location.reload();
					}
				});
			},
			submitCode:function(){
				var $form = Method.$form;
				if($form.find('#promoCode').val() !== ''){
					sandbox.setLoadingBarState(true);
					BLC.ajax({url: $form.attr('action'),
							type: "POST",
							data: $form.serialize()
						}, function(data, extraData) {

							var endPointData = $.extend(extraData, {
								promoCode : sandbox.utils.url.getQueryStringParams( $form.serialize() ).promoCode
							});

							if (data.error && data.error == 'illegalCartOperation') {
								sandbox.setLoadingBarState(false);
								UIkit.modal.alert(data.exception);
								endPointData.exception = 'illegalCartOperation';

							} else {
								if(!extraData.promoAdded) {
									sandbox.setLoadingBarState(false);
									Method.$errorMessage.html(extraData.exception);
									Method.$errorMessage.addClass("active").removeClass('hidden');
								} else {
									if( _.isElement( Method.replaceTarget) ){
										sandbox.setLoadingBarState(false);
										$(Method.replaceTarget).html( data );
									}else{
										window.location.reload();
									}
								}
							}

							endPoint.call('applyPromoCode', endPointData);
						}
					);
					return false;
				};

			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-promotion]',
					attrName:'data-module-promotion',
					moduleName:'module_promotion',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
