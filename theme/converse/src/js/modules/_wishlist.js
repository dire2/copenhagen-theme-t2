(function(Core){
	'use strict';
	Core.register('module_wishlist', function(sandbox){

		var $this, modal;
		var Method = {
			moduleInit:function(){
				var miniCartModule = sandbox.getModule('module_minicart');
				$this = $(this);
				modal = UIkit.modal('#quickview-modal');
				$this.find('.wish-delete_btn').on('click', function(){
					var $that = $(this);
					UIkit.modal.confirm('위시리스트에서 삭제 하시겠습니까?', function(){
						setTimeout(function(){
							location.href = $that.attr('href');
						});
					});
					return false;
				});

				$this.find('.addtocart').each(function(i){
					var target = $(this).attr('data-target');
					var url = $(this).attr('data-href');

					$(this).click(function(e){
						e.preventDefault();

						Core.Utils.ajax(url, 'GET', {quickview:true}, function(data){
							var domObject = $(data.responseText).find('#quickview-wrap');

							if (domObject.length == 0) {
								UIkit.modal.alert('판매 중지된 상품입니다.');
							} else {
								//$(target).addClass('window-modal modal-quickview');
								$(target).find('.contents').empty().append(domObject[0].outerHTML);
								Core.moduleEventInjection(domObject[0].outerHTML);
								modal.show();
							}
						});

						/*sandbox.utils.ajax(url, 'POST', data, function(data){
							var jsonData = sandbox.rtnJson(data.responseText);
							if(jsonData.hasOwnProperty('error')){
								UIkit.notify(jsonData.error, {timeout:3000,pos:'top-center',status:'warning'});
							}else{
								//UIkit.notify('쇼핑백에 상품이 담겼습니다.', {timeout:3000,pos:'top-center',status:'success'});
								miniCartModule.update();
							}
						});*/
					});
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-wishlist]',
					attrName:'data-module-wishlist',
					moduleName:'module_wishlist',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
