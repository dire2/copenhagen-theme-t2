(function(Core){
	'use strict';

	Core.register('module_sc_slider', function(sandbox){
		var $this, args, endPoint;
		var Method = {
			moduleInit:function(){
				$this = $(this);
				args = arguments[0];
				endPoint = Core.getComponents('component_endpoint');

        var $slider = $this.find('.slick-slider');

        var defaultOption = {
          dots: false,
          infinite: false,
          speed: 300,
          arrows: false,
          slidesToShow: 8,
          swipeToSlide:true,
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 6,
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 4,
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 2,
                variableWidth: true
              }
            }
          ]
				}

        $slider.slick(defaultOption);

			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-sc-slider]',
					attrName:'data-module-sc-slider',
					moduleName:'module_sc_slider',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
