(function(Core){
	'use strict';

	Core.register('module_exchangeable', function(sandbox){
		var Method = {
			$that:null,
			$allCheck:null, // 팝업 전체 선택 체크박스
			$itemList:null, // 선택 해서 popup에 노출되는 아이템 리스트
			$popModal:null,
			$returnItemList:null,
			$popSubmitBtn:null,
			$beforeAddress:null,
			$newAddress:null,
			isAblePartialVoid:null,
			ex_deliverySearch:null,
			ex_deliverySearchAnother:null,
			isAddress:false,
			isNewAddress:false,
			isDoubleClickFlag:true,
			isCheckeds:false,
			$errorMessage:'<p class="error-message essential-select"><span class="parsley-required">필수 입력 항목입니다.</span></p>',

			moduleInit:function(){
				var args = Array.prototype.slice.call(arguments).pop();
				$.extend(Method, args);

				var $this = $(this);
				Method.$that = $this;
				Method.$popModal = UIkit.modal("#popup-exchange");
				Method.$ex_paymentPopModal = UIkit.modal("#popup-exchange-step02",{bgclose:false});
				Method.$popCuntBtn = Method.$popModal.find('[data-cunt-submit]');
				Method.$popSubmitBtn = Method.$popModal.find('[data-exchange-submit]');
				Method.$popCompleteBtn = Method.$ex_paymentPopModal.find('[exchange-complete-btn]');
				Method.$popAddressModal01 = UIkit.modal("#popup-customer-address01", {modal: false});
				Method.$popAddressModal02 = UIkit.modal("#popup-customer-address02", {modal: false});
				Method.$beforeAddress01 = Method.$popModal.dialog.find('[data-before-exchange-address01]');
				Method.$beforeAddress02 = Method.$popModal.dialog.find('[data-before-exchange-address02]');
				Method.$newAddress01 = Method.$popModal.dialog.find('[data-new-exchange-address01]');
				Method.$newAddress02 = Method.$popModal.dialog.find('[data-new-exchange-address02]');
				Method.$refundAccountInfo = Method.$popModal.find('[data-refund-account-info]');
				Method.$popAddressInputModal = UIkit.modal("#ex_addressSelectPopup",{modal: false, center: true});
				Method.$popNewAddressInputModal = UIkit.modal("#ex_newAddressSelectPopup",{modal: false, center: true});



				// 주문별 전체 교환
				$this.find('[data-exchange-order-btn]').on('click', function(e){
					e.preventDefault();
					Method.openExchangPopup($(this).data("orderid"));
				});

				// 교환 사유 변경시
				Method.$ex_paymentPopModal.find('[data-exchange-reason-type]').on("change", Method.ex_updateStatus);

				//교환신청 주소 tab 기능 - 삭제
				//Method.$ex_paymentPopModal.find('[data-order-tab]').on("mousedown", Method.updateOrderTab);

				//수거지 정보동일 버튼 해제 - 새로입력
				Method.$newAddress01.find('input').on('mousedown',function(){
					$("#add-duplication").removeAttr('checked').parents('.input-checkbox').removeClass('checked');
				});
				Method.$newAddress02.find('input').on('mousedown',function(){
					$("#add-duplication").removeAttr('checked').parents('.input-checkbox').removeClass('checked');
				});

				// 신청완료 전 팝업 종료시 새로고침
				Method.$ex_paymentPopModal.find('.uk-modal-close').click(function(){
					window.location.reload();
				});

				Method.$ex_paymentPopModal.find('.payment-method-item').on('click',function(){
					var paymentType = $(this).data('type');
					$('.payment-method-item').removeClass('active');
					$('.payment-method-item[data-type="'+paymentType+'"]').addClass('active');
					return false;
				});


				Method.ex_deliverySearch = sandbox.getComponents('component_searchfield', {context:Method.$popModal, selector:'.search-field.exchange', resultTemplate:"#address-find-list"}, function(){
					// 수거지주소 입력 처리
					var $zipCodeInput = Method.$popModal.find('[name="addressPostalCode"]');
					var $cityInput = Method.$popModal.find('[name="addressCity"]');

					// 검색된 내용 선택시 zipcode 처리
					this.addEvent('resultSelect', function(data){
						var zipcode = $(data).data('zip-code5');
						var city = $(data).data('city');
						var doro = $(data).data('doro');

						var $input = this.getInputComponent().setValue( city + ' ' + doro );

						$zipCodeInput.val( zipcode );
						$cityInput.val( city.split(' ')[0] );
					});
				});



				Method.ex_deliverySearchAnother = sandbox.getComponents('component_searchfield', {context:Method.$popModal, selector:'.search-field.exchangeNew', resultTemplate:"#address-find-list"}, function(){
					// 받는 주소 입력 처리
					var $zipCodeInput = Method.$popModal.find('[name="newAddressPostalCode"]');
					var $cityInput = Method.$popModal.find('[name="newAddressCity"]');

					// 검색된 내용 선택시 zipcode 처리
					this.addEvent('resultSelect', function(data){
						var zipcode = $(data).data('zip-code5');
						var city = $(data).data('city');
						var doro = $(data).data('doro');

						var $input = this.getInputComponent().setValue( city + ' ' + doro );

						$zipCodeInput.val( zipcode );
						$cityInput.val( city.split(' ')[0] );
					});
				});




				// 수거지 선택 버튼
				Method.$popModal.find('[data-customer-address-btn01]').on('click', function(e){
					e.preventDefault();
					Method.$popAddressModal01.show();
				});

				// 받는주소 선택 버튼
				Method.$popModal.find('[data-customer-address-btn02]').on('click', function(e){
					e.preventDefault();
					Method.$popAddressModal02.show();
				});



				// 수거지 주소와 동일
				$("#add-duplication").change(function(){
					var $target01 = Method.$popModal.dialog.find('[data-before-exchange-address01]');
					var $target02 = Method.$popModal.dialog.find('[data-before-exchange-address02]');
					var $newtarget01 = Method.$popModal.dialog.find('[data-new-exchange-address01]');

	        		if($(this).is(":checked")){
						if ($target01.find('input').is(':disabled')){
							var $fullName = $newtarget01.find('input[name="addressFirstName"]').val();
							var $phone = $newtarget01.find('input[name="addressPhone"]').val();
							var $address1 = $newtarget01.find('input[name="addressLine1"]').val();
							var $address2 = $newtarget01.find('input[name="addressLine2"]').val();
							var $postalCode = $newtarget01.find('input[name="addressPostalCode"]').val();
							var $addressCity = $newtarget01.find('input[name="addressCity"]').val();
						} else{
							var $fullName = $target01.find('input[name="addressFirstName"]').val();
							var $phone = $target01.find('input[name="addressPhone"]').val();
							var $address1 = $target01.find('input[name="addressLine1"]').val();
							var $address2 = $target01.find('input[name="addressLine2"]').val();
							var $postalCode = $target01.find('input[name="addressPostalCode"]').val();
							var $addressCity = $target01.find('input[name="addressCity"]').val();
						}
						$target02.find('[data-user-name]').html($fullName);
						$target02.find('[data-phone]').html($phone);
						$target02.find('[data-postalCode]').html($postalCode);
						$target02.find('[data-address1]').html($address1);
						$target02.find('[data-address2]').html($address2);
						$target02.find('input[name="newAddressFirstName"]').val($fullName);
						$target02.find('input[name="newAddressPhone"]').val($phone);
						$target02.find('input[name="newAddressLine1"]').val($address1);
						$target02.find('input[name="newAddressLine2"]').val($address2);
						$target02.find('input[name="newAddressPostalCode"]').val($postalCode);
						$target02.find('input[name="newAddressCity"]').val($addressCity);
	        		}
		    	});



				// 수거메모 선택시
				Method.$popModal.find('[data-personal-message-select]').on('change', Method.updatePersonalMessage );
				Method.$popModal.find('[data-newPersonal-message-select]').on('change', Method.updateNewPersonalMessage );

				Method.$popSubmitBtn.on('click', Method.exchangeOrderSubmit );
			},

			// updateOrderTab:function(e){
			// 	e.preventDefault();
			// 	var $icon = $(this).find('[class^="icon-toggle-"]');
			// 	var $view = $(this).closest('.order-tab').find('.view');
			// 	var $preview = $(this).find('.preview');
			//
			// 	if( $view.length > 0 ){
			// 		$preview.toggleClass('uk-hidden');
			// 		$icon.toggleClass('uk-hidden');
			// 		$view.toggleClass('uk-hidden');
			// 	}
			// },

			updatePersonalMessage:function(e){
				e.preventDefault();
				var $msgContainer = Method.$popModal.dialog.find('[data-personal-message]');
				var $personalMsg = $msgContainer.find('[name="personalMessageText"]');

				var value = $(this).val();
				if(value == ''){
					$personalMsg.val('');
					$msgContainer.addClass('uk-hidden');
				}else if(value == 'dt_1'){
					// 직접입력일 경우
					$personalMsg.val('');
					$msgContainer.removeClass('uk-hidden');
				}else{
					//$personalMsg.val( $(this).find("option:selected").val() + "||" + $(this).find("option:selected").text() );
					$personalMsg.val( $(this).find("option:selected").text());
					$msgContainer.addClass('uk-hidden');
				}
			},

			updateNewPersonalMessage:function(e){
				e.preventDefault();
				var $msgContainer = Method.$popModal.dialog.find('[data-newpersonal-message]');
				var $personalMsg = $msgContainer.find('[name="newPersonalMessageText"]');

				var value = $(this).val();
				if(value == ''){
					$personalMsg.val('(교환) ');
					$msgContainer.addClass('uk-hidden');
				}else if(value == 'dt_1'){
					// 직접입력일 경우
					$personalMsg.val('(교환) ');
					$msgContainer.removeClass('uk-hidden');
				}else{
					//$personalMsg.val( $(this).find("option:selected").val() + "||" + $(this).find("option:selected").text() );
					$personalMsg.val('(교환) ' + $(this).find("option:selected").text());
					$msgContainer.addClass('uk-hidden');
				}
			},

			// 교환 신청 팝업
			openExchangPopup:function( orderId ){

				var $modal = Method.$popModal;
				var $modalForm = $modal.dialog.find('form');
				Method.$itemList = $modal.dialog.find('[data-exchange-reason-list] #exChangeItem');

				var $reasonItem = $modal.dialog.find('[data-exchange-reason-item]');
				var $returnAddress = $modal.dialog.find('input[name="return-address"]');

				// 전체 선택 체크박스 처리
				Method.$allCheck = Method.$popModal.find('input[name="ex_check-all"]');
				Method.$allCheck.on("change", Method.ex_changeAllCheck );


				// 배송지 입력 타입 버튼 선택시
				$modal.find('[data-return-address-type]').on('show.uk.switcher', function(){
					Method.updateAddressInput();
				});

				// 배송지 선택 모듈 select 이벤트 호출( 배송지 선택했을때 호출됨 )
				Core.getComponents('component_customer_address', {context: $('[id*="popup-customer-address"]')}, function(){
					this.addEvent('select', function(data){
						if( Method.$popAddressModal01.isActive()){
							Method.updateCustomerAddress01(data);
							Method.$popAddressModal01.hide();
							$("#add-duplication").removeAttr('checked').parents('.input-checkbox').removeClass('checked');
						};
						if( Method.$popAddressModal02.isActive()){
							Method.updateCustomerAddress02(data);
							Method.$popAddressModal02.hide();
							$("#add-duplication").removeAttr('checked').parents('.input-checkbox').removeClass('checked');
						};
					});
				});

				Method.$itemList.empty();

				//sandbox.getModule('module_header').reDirect().setLogin(function(){
					sandbox.utils.promise({
						url:sandbox.utils.contextPath + '/account/orders/exchangeable/request/' + orderId,
						method:'GET'
					}).then(function(data){
						var defer = $.Deferred();
						$modal.find('[data-exchange-reason-list]').find('#exChangeItem').remove();
						$modal.find('[data-exchange-reason-list]').append(data);

						/*
						isAble 				: 주문 취소, 교환 가능 여부
						isAblePartial   	: 부분 취소, 교환 가능 여부
						*/
						//Method.isAblePartialVoid = $modal.find('#exChangeItem').find('.exchange-item').data('isablepartial');

						var $newItem = $reasonItem.removeClass("uk-hidden");
						var $info = $newItem.find('[exchange-reason-info]');
						var $thumb = $newItem.find('[exchange-reason-thumb]');
						var $item = $modal.find('[data-exchange-reason-list] [data-exchange-reason-item]');

						//팝업내 select 리셋
						//$modal.find('.select-body li:first-child span').trigger('click').parents('.select-box').removeClass('checked');

						//부분교환 확인
						var exchangeableQuantitySum = [];
						$item.each(function(){
							var $this_exchangeableQuantity = $(this).find('input[name="quantity"]').val();
							exchangeableQuantitySum.push($this_exchangeableQuantity);
						});
						var sumTotal = exchangeableQuantitySum.reduceRight(function(a,b){return a+b;});
						if(sumTotal == $item.length){
							Method.isAblePartialVoid = false;
						} else {
							Method.isAblePartialVoid = true;
						}

						$item.each(function () {
							// 교환 가능 수량
							var quantity = $(this).find('input[name="quantity"]').val();

							//사이즈 change 처리
							var skuId = $(this).find('input[name="skuId"]');
							$(this).find('select[name="skuSizeSel"]').change(function () {
						     var valueSelected  = $(this).find("option:selected").val();
								 skuId.val(valueSelected);
								 $(this).parents('[data-exchange-reason-item]').find('.complet-size span').text($(this).find("option:selected").text());

						     if(valueSelected == "" && $(this).find('.error-message').length == 0){
										$(this).parent('.select-box').after(Method.$errorMessage);
						     } else{
										$(this).parent('.select-box').next('.error-message').remove();
						     }
							 });

							 if( Method.isAblePartialVoid ){
  								var $quantity = $(this).find('[exchange-partials-quantity]');
 									$quantity.find("select").removeAttr('disabled');


								// 부분교환시 아이템 가능 수량이 1개 이상 일때
 								if(quantity > 1){
 									for( var i=1; i<=quantity; i++){
 										$quantity.find("select").append( '<option value="' + i + '">' + i +'</option>');
 	 								}
 									$(this).find('input[name="quantity"]').removeAttr('disabled').val(1);
									if($(this).closest('#exChangeItem').find('[data-exchange-reason-item]').length == 1){
										$quantity.show();
									}
 								}else{
									// 부분교환시 아이템 가능 수량이 1개 일때
 									var $quantity = $(this).find('input[name="quantity"]');
 	 								$quantity.removeAttr('disabled').val(quantity);
 								}

  							}else{
  								var $inputQuantity = $(this).find('input[name="quantity"]');
  								$inputQuantity.removeAttr('disabled').val(quantity);
  							}

 							$(this).find('.exchangeQuantity select').change(function () {
 								var valueSelected  = $(this).find("option:selected").val();
 								$(this).parents('[data-exchange-reason-item]').find('.complet-quantity span').text(valueSelected);
 								$(this).parents('[data-exchange-reason-item]').find('input[name="quantity"]').removeAttr('disabled').val(valueSelected);
 							});

 						});

						//상품이 1개 이면 체크박스 X , 이상이면 O
						var itemPartial = $item.length > 1; //1개이면 false, 이상이면 true

						if(!itemPartial){
							var itemError = $item.find('#skuSizeSel').val() == '';
							if(itemError){
								$item.find('#skuSizeSel').parent('.select-box').removeClass('hidden');
								$item.find('#skuSizeSel').parent('.select-box').after(Method.$errorMessage);
							}

							Method.updateSubmitBtn( true );
						}else{
							Method.updateSubmitBtn( !itemPartial );
							$modal.find('[data-exchange-reason-list] [data-exchange-reason-item]').find('input[type="checkbox"]').on("change", Method.changeItemCheck );
						}

						Method.updateInfoByIsPartial( itemPartial ); //단일상품 or 복수상품에 대한 UI처리
						Method.defaultAddressInput(); //주문주소지로 기본설정


						//$modal.find('[data-exchange-reason-list]>ul>li').find('select').on("change", Method.updateStatus );

						// 전체 취소로 초기화
						Method.$allCheck.prop('checked', false ).trigger('change');
						$modal.show();
						sandbox.validation.reset( $modal.dialog.find('form'));

						sandbox.getComponents('component_select', {context:$modal.dialog}, function(i){
							this.addEvent('change', function(val, $selected, index){
								if($(this).attr('name') == 'accountCode'){
									$(this).siblings().filter('input[name=accountName]').val($selected.text());
								}
							});
						});

						sandbox.moduleEventInjection(data, defer);

						Method.$itemList = $modal.dialog.find('[data-exchange-reason-list] #exChangeItem');

						return defer.promise();
					}).then(function(data){
						// UIkit.modal.alert("취소 되었습니다.").on('hide.uk.modal', function() {
						// 	window.location.reload();
						// });
					}).fail(function(error){
						// if(error){
						// 	UIkit.modal.alert(error).on('hide.uk.modal', function() {
						// 		window.location.reload();
						// 	});
						// }else{
						// 	window.location.reload();
						// }
					});
				//});
			},

			// 교환 사유에 따른 배송비 계산
			ex_updateStatus:function(e){
				e.preventDefault();

				var $item = Method.$popModal.find('[data-exchange-reason-list] [data-exchange-reason-item]');
				//상품이 1개이면 전체교환 not 부분교환
				var itemPartial = $item.length > 1; //1개이면 false, 이상이면 true
				if(itemPartial){
					var $items = Method.$itemList.find('[data-exchange-reason-item]').find('input[type="checkbox"]:checked').closest('[data-exchange-reason-item]');
				}else{
					var $items = Method.$itemList.find('[data-exchange-reason-item]');
				}

				var $modal = Method.$ex_paymentPopModal;
				var $form = $modal.dialog.find('form');
				var $itemForm = Method.getFormByPartialItem( $items );
				var action = $form.attr('action') + '/calculatorFee';
				var param = $itemForm.serialize() + '&' + $form.serialize();

				Core.Utils.ajax( action, 'POST', param, function(data){

					var data = sandbox.rtnJson(data.responseText, true);
					var $paymentAmount = $modal.find('[data-exchange-amount]');
					//var $paymentItem = $modal.find('.uk-hidden[data-payment-item]');
					//$paymentList.empty();

					if( !data ){
						// var $newItem = Method.getReplacePaymentItem( $paymentItem, '서버 통신 오류' );
						// $newItem.appendTo( $paymentList );
						console.log('데이터오류?'+data);
					}
					var result = data.result;
					if( result == true ){
						// 배송비
						var addFulfillmentChargeFee = data.addFulfillmentChargeFee.amount;
						$paymentAmount.text(sandbox.utils.price(addFulfillmentChargeFee));
						// if(addFulfillmentChargeFee != 0){
						$modal.find('[exchange-payment-item]').removeAttr('disabled').removeClass('disabled');
						// 	$modal.find('[exchange-payment-item]').removeClass('uk-hidden');
						// }else{
						// 	$modal.find('[exchange-complete-btn]').removeAttr('disabled').removeClass('disabled').show();
						// 	$modal.find('[exchange-payment-item]').addClass('uk-hidden');
						// }
						var paymentGroup = $modal.find('[data-exchange-payment-group]');
						if(addFulfillmentChargeFee == 0){
							paymentGroup.addClass('uk-hidden');
						} else {
							paymentGroup.removeClass('uk-hidden');
						}
					}else{
						// var $newItem = Method.getReplacePaymentItem( $paymentItem, data.errorMsg );
						// $newItem.appendTo( $paymentList );
						//Method.updatePaymentInfo( false );
						//Method.isCheckeds = false;
						UIkit.modal.alert(data.errorMsg);
					}
				}, true);

			},

			defaultAddressInput:function(){
				// 주문시 주소로 기본입력처리
				var $orderAddress = Method.$popModal.dialog.find('[order-address]');
				var $addressArea = Method.$popModal.dialog.find('.return-address');

				//실제 주문시 입력한 주소정보
				var $order_firstName = $orderAddress.find('input[name="order_addressFirstName"]').val();
				var $order_phone = $orderAddress.find('input[name="order_addressPhone"]').val();
				var $order_addressLine1 = $orderAddress.find('input[name="order_addressLine1"]').val();
				var $order_addressLine2 = $orderAddress.find('input[name="order_addressLine2"]').val();
				var $order_postalCode = $orderAddress.find('input[name="order_addressPostalCode"]').val();
				var $order_city = $orderAddress.find('input[name="order_addressCity"]').val();

				//수거지 및 배송지 공통 주소 정보 입력
				$addressArea.find(".addressFirstName").val($order_firstName);
				$addressArea.find(".addressPhone").val($order_phone);
				$addressArea.find(".addressLine1").val($order_addressLine1);
				$addressArea.find(".addressLine2").val($order_addressLine2);
				$addressArea.find(".addressPostalCode").val($order_postalCode);
				$addressArea.find(".addressCity").val($order_city);

				$addressArea.find('[data-user-name]').html($order_firstName);
				$addressArea.find('[data-phone]').html($order_phone);
				$addressArea.find('[data-postalCode]').html($order_postalCode);
				$addressArea.find('[data-address1]').html($order_addressLine1);
				$addressArea.find('[data-address2]').html($order_addressLine2);
			},

			// 아이템 단위로 사이즈 선택할 수 있는 select 노출 처리
			showHideAvailabeSize:function( $checkbox ){
				var $exchangeSize = $checkbox.parents('.order-product-item').find('[exchange-reason-partials-size]');
				var itemPartial = Method.$popModal.dialog.find('[data-exchange-reason-item]');
				$exchangeSize.next('.error-message').remove();

				if( $checkbox.prop('checked')){
					$exchangeSize.removeClass('hidden');
					$exchangeSize.after(Method.$errorMessage);
					// if(itemPartial.find('.error-message').length == 0 && $exchangeSize.find('#skuSizeSel').val() == ''){
					// 	$exchangeSize.after(Method.$errorMessage);
					// }
				}else{
					if(itemPartial.length == 1){ //상품이 1개일 때
						itemPartial.find('[exchange-reason-partials-size]').removeClass('hidden');
						if(itemPartial.find('.error-message').length == 0 && $exchangeSize.find('#skuSizeSel').val() == ''){
							itemPartial.find('[exchange-reason-partials-size]').after(Method.$errorMessage);
						}
					}else{
						$exchangeSize.addClass('hidden');
						$exchangeSize.find('#skuSizeSel').val('');
						$exchangeSize.next('.error-message').remove();
					}
				}
			},

			// 아이템 단위로 수량 선택할 수 있는 select 노출 처리
			showHideAvailabeQuantity:function( $checkbox ){
				var $selectQuantity = $checkbox.closest('[data-exchange-reason-item]').find('[exchange-partials-quantity]');
				var availableQuantity = $checkbox.closest('[data-exchange-reason-item]').find('input[name=quantity]').val();
				var $inputQuantity = $checkbox.closest('[data-exchange-reason-item]').find('[exchange-quantity]');
				if( $checkbox.prop('checked')){
					if(availableQuantity >= 1){
						$selectQuantity.show();
						$inputQuantity.show();
					}
				}else{
					if($checkbox.closest('#exChangeItem').find('[data-exchange-reason-item]').length == 1 && availableQuantity > 1){
						$selectQuantity.show();
					}else if($checkbox.closest('#exChangeItem').find('[data-exchange-reason-item]').length == 1 && availableQuantity == 1){
						$inputQuantity.show();
					}else{
						$selectQuantity.hide();
						$inputQuantity.hide();
					}
				}
			},

			// 부분 교환 가능 여부에 따른 정보 노출 처리
			updateInfoByIsPartial:function( $bool ){
				var $checkAllContainer = Method.$popModal.dialog.find('.ex_check-all');
				var $checkboxs = Method.$popModal.dialog.find('[data-exchange-reason-list] #exChangeItem').find('.checkbox');
				var $info = Method.$popModal.dialog.find('[data-info-text]');

				if( $bool ){
					$checkAllContainer.show();
					$checkboxs.show();
					$checkboxs.siblings('svg').addClass('icon').show();
					//$info.show();
				}else{
					$checkAllContainer.hide();
					$checkboxs.hide();
					$checkboxs.siblings('svg').removeClass('icon').hide();
					//$info.hide();
				}
			},

			// modal에서 전체 선텍
			ex_changeAllCheck:function(e){
				var isCheck = $(this).prop('checked');
				Method.$itemList.find('[data-exchange-reason-item]').each( function(){
					$(this).find('input[type="checkbox"]').prop( 'checked', isCheck );
					Method.showHideAvailabeSize( $(this).find('input[type="checkbox"]') );
					Method.showHideAvailabeQuantity( $(this).find('input[type="checkbox"]') );
				});
				Method.updateStatusChecked();

				if (Method.isCheckeds) {
					Method.updatePaymentInfo( false );
					Method.isCheckeds = false;
				}
			},

			// modal에서 체크박스 선택시
			changeItemCheck:function(e){
				var isCheck = $(this).prop('checked');
				Method.showHideAvailabeSize( $(this ));
				Method.showHideAvailabeQuantity( $(this ));
				//$(this).parents('[data-exchange-reason-item]').find('select[name="skuSizeSel"]').val( $(this).parents('[data-exchange-reason-item]').find('select[name="skuSizeSel"]') ).trigger('update');

				Method.updateStatusChecked();
				Method.updateCheckAll(isCheck);

				if (Method.isCheckeds) {
					Method.updatePaymentInfo( false );
					Method.isCheckeds = false;
				}
			},

			// 아이템 체크박스 변경시 전체 선택 체크박스 상태처리
			updateCheckAll:function(isCheck){
				var checkboxList = Method.$itemList.find('[data-exchange-reason-item] .i-checkbox');
		    if(isCheck){
	        if(checkboxList.length == checkboxList.find('input[type="checkbox"]:checked').length ){
				    Method.$allCheck.prop( 'checked', true );
  				}else{
  					Method.$allCheck.prop( 'checked', false );
  				}
		    }else{
	        Method.$allCheck.prop( 'checked', false );
		    }
			},

			// 리턴 상황에 따른 가격정보와
			updatePaymentInfo:function( $bool ){
				var $paymentContainer = Method.$popModal.find('[data-payment-conatiner]');

				if( $bool ){
					$paymentContainer.show();
					Method.$popCuntBtn.hide();
				}else{
					Method.$popCuntBtn.show();
				}
			},

			updateStatusChecked:function(){
				var $modal = Method.$popModal;
				var $items = Method.$itemList.find('[data-exchange-reason-item]').find('input[type="checkbox"]:checked').parents('[data-exchange-reason-item]');

				if( $items.length > 0 ){
					if (Method.isDoubleClickFlag) {
						Method.updateSubmitBtn( true ); // 체크박스 선택 가능
						Method.isDoubleClickFlag = false;
					}
				} else {
					// 아이템 없을때
					// 부분 취소, 교환 가능 여부
					var isablepartial = true;

					if (!isablepartial) {
						Method.updateSubmitBtn( true );
					} else {
						if (!Method.isDoubleClickFlag) {
							Method.updateSubmitBtn( false ); // 체크박스 선택 불가능
							Method.isDoubleClickFlag = true;
						}
					}
				}
			},

			// 버튼 활성화/비활성화 처리
			updateSubmitBtn:function( $bool ){
				var $paymentContainer = Method.$popModal.find('[data-payment-conatiner]');
				var _disabledCheck;
				// Method.$popModal.find('[data-exchange-reason-list] [data-exchange-reason-item]').each( function(){
				// 	var _disabled = $(this).find('input[type="checkbox"]').attr('disabled');
				// 	var _bool = _disabled == 'disabled';
				// 	_disabledCheck = _.some([_bool], Boolean); // true
				// });
				// if (_disabledCheck) {
				// 	Method.$allCheck.attr('disabled', true).closest('.input-checkbox').addClass('disabled');
				// } else {
				// 	Method.$allCheck.attr('disabled', false).closest('.input-checkbox').removeClass('disabled');
				// }
				if ($bool) {
					Method.$popSubmitBtn.removeAttr('disabled').removeClass('disabled');
				} else {
					Method.$popSubmitBtn.attr('disabled','true').addClass('disabled');
				}
			},

			// 선택된 아이템의 order dom
			getOrderElementByChecked:function($checkbox){
				var $order = $checkbox.closest('[data-exchange-order]');
				return {
					order : $order,
					allCheckbox : $order.find('input[name="ex_check-all"]'),
					itemList : $order.find('[data-exchange-order-list]')
				}
			},

			// 배송지 선택으로 주소 입력시
			updateCustomerAddress01:function( data ){
				// 수거지 주소
				var $target01 = Method.$popModal.dialog.find('[data-before-exchange-address01]');
				if( $target01.find('[data-user-name]').length > 0 ){
					$target01.find('[data-user-name]').html($.trim(data.fullName));
				}

				if( $target01.find('[data-phone]').length > 0 ){
					$target01.find('[data-phone]').html($.trim(data.phoneNumber));
				}

				if( $target01.find('[data-postalCode]').length > 0 ){
					$target01.find('[data-postalCode]').html($.trim(data.postalCode));
				}

				if( $target01.find('[data-address1]').length > 0 ){
					$target01.find('[data-address1]').html($.trim(data.addressLine1));
				}

				if( $target01.find('[data-address2]').length > 0 ){
					$target01.find('[data-address2]').html($.trim(data.addressLine2));
				}

				// 변경된 값 input 에 적용
				$target01.find('input[name="addressFirstName"]').val($.trim(data.fullName));
				$target01.find('input[name="addressPhone"]').val($.trim(data.phoneNumber));
				$target01.find('input[name="addressLine1"]').val($.trim(data.addressLine1));
				$target01.find('input[name="addressLine2"]').val($.trim(data.addressLine2));
				$target01.find('input[name="addressPostalCode"]').val($.trim(data.postalCode));
				$target01.find('input[name="addressCity"]').val($.trim(data.city));
			},
			updateCustomerAddress02:function( data ){
				// 받으실 주소
				var $target02 = Method.$popModal.dialog.find('[data-before-exchange-address02]');
				if( $target02.find('[data-user-name]').length > 0 ){
					$target02.find('[data-user-name]').html($.trim(data.fullName));
				}

				if( $target02.find('[data-phone]').length > 0 ){
					$target02.find('[data-phone]').html($.trim(data.phoneNumber));
				}

				if( $target02.find('[data-postalCode]').length > 0 ){
					$target02.find('[data-postalCode]').html($.trim(data.postalCode));
				}

				if( $target02.find('[data-address1]').length > 0 ){
					$target02.find('[data-address1]').html($.trim(data.addressLine1));
				}

				if( $target02.find('[data-address2]').length > 0 ){
					$target02.find('[data-address2]').html($.trim(data.addressLine2));
				}

				// 변경된 값 input 에 적용
				$target02.find('input[name="newAddressFirstName"]').val($.trim(data.fullName));
				$target02.find('input[name="newAddressPhone"]').val($.trim(data.phoneNumber));
				$target02.find('input[name="newAddressLine1"]').val($.trim(data.addressLine1));
				$target02.find('input[name="newAddressLine2"]').val($.trim(data.addressLine2));
				$target02.find('input[name="newAddressPostalCode"]').val($.trim(data.postalCode));
				$target02.find('input[name="newAddressCity"]').val($.trim(data.city));
			},

			// 실제 전송될 주소 정보를 설정하기 위해 불필요 정보 disabled
			updateAddressInput:function(){
				// 수거지 주소
				// if( Method.$beforeAddress01.hasClass('uk-active')){
				// 	Method.isAddress = false;
				// 	Method.$beforeAddress01.find('input').attr('disabled', false );
				// 	Method.$newAddress01.find('input').attr('disabled', true );
				// } else{
				// 	Method.isAddress = true;
				// 	Method.$beforeAddress01.find('input').attr('disabled', true );
				// 	Method.$newAddress01.find('input').attr('disabled', false );

				// 	//Method.$newAddress01.find('input#addressLine1').attr('disabled', true);
				// 	//Method.$popModal.find("#add-duplication").removeAttr('checked').parents('.input-checkbox').removeClass('checked');
				// }

				// 받으실 주소
				if( Method.$beforeAddress02.hasClass('uk-active')){
					Method.isNewAddress = false;
					Method.$beforeAddress02.find('input').attr('disabled', false );
					Method.$newAddress02.find('input').attr('disabled', true );
				} else{
					Method.isNewAddress = true;
					Method.$beforeAddress02.find('input').attr('disabled', true );
					Method.$newAddress02.find('input').attr('disabled', false );

					//Method.$newAddress02.find('input#addressLine1').attr('disabled', true);
					//Method.$popModal.find("#add-duplication").removeAttr('checked').parents('.input-checkbox').removeClass('checked');
				}


			},

			// 선택된 아이템을 하나의 form으로 만들어 리턴
			getFormByPartialItem:function( $items ){
				var $itemForm = $('<form></form>');

				//체크되어있는 아이템 가져와 form에 append
				$items.each( function(){
					var quantity = $(this).find('[name="quantity"]:enabled').val();
					var $newItem = $(this).clone();

					$newItem.find('[name="quantity"]').val(quantity);
					$newItem.appendTo( $itemForm );
				});
				return $itemForm;
			},

			exchangeOrderSubmit:function(e){
				e.preventDefault();

				// $itemList에 에러메시지가 없으면 실행
				if(Method.$itemList.find('.error-message.essential-select').length == 0){
					var $modalForm = Method.$popModal.dialog.find('form');
					sandbox.validation.validate( $modalForm );
					if( sandbox.validation.isValid( $modalForm )){

						// if( Method.isAddress ){
						// 	if( !Method.ex_deliverySearch.getValidateChk()){
						// 		UIkit.modal.alert("검색을 통하여 배송지를 입력해주세요.");
						// 		return false;
						// 	}
						// }

						if( Method.isNewAddress ) {
							if( !Method.ex_deliverySearchAnother.getValidateChk() ){
								UIkit.modal.alert("검색을 통하여 배송지를 입력해주세요.");
								return false;
							}
						}

						//console.log( $modalForm.attr('action'))
						UIkit.modal.confirm('교환 하시겠습니까?', function(){
							// 주소에 노출된 우편번호 제거
							if( Method.isAddress ){
								var $addressLine1 = $modalForm.find('[name="addressLine1"]:hidden');
								if( $addressLine1 ){
									if( $addressLine1.val().indexOf(')') >= 1 ){
										var address1 = $addressLine1.val().split(')');
										if( address1.length > 1 ){
										$addressLine1.val( $.trim( address1[1]) );
										}
									}
								}
							}

							if( Method.isNewAddress ){
								var $newAddressLine1 = $modalForm.find('[name="newAddressLine1"]:hidden');
								if( $newAddressLine1 ){
									if( $newAddressLine1.val().indexOf(')') >= 1 ){
										var newAddress1 = $newAddressLine1.val().split(')');
										if( newAddress1.length > 1 ){
											$newAddressLine1.val( $.trim( newAddress1[1]) );
										}
									}
								}
							}


							var isPartial = Method.$itemList.find('[data-exchange-reason-item]').length != 1; //신청 상품개수가 단일인지 판단
							var param = '';

							//신청 다음단계에서 보여줄 상품리스트
							var exchangeCompleteList = Method.$ex_paymentPopModal.find('[data-exchange-complete-list]');
							exchangeCompleteList.html(' ');

							// 부분 교환일때는 상품별
							if( isPartial ){
								var $itemForm = Method.getFormByPartialItem( Method.$itemList.find('[data-exchange-reason-item]').find('input[type="checkbox"]:checked').parents('[data-exchange-reason-item]') );
								param = $itemForm.serialize() +'&'+ $modalForm.serialize();

								//신청 다음단계에서 보여줄 상품리스트 clone & append
								var exchangeComplete = Method.$itemList.find('[data-exchange-reason-item]').find('input[type="checkbox"]:checked').parents('[data-exchange-reason-item]').clone();
								exchangeCompleteList.append(exchangeComplete);

							}else{
								var $itemForm = Method.getFormByPartialItem( Method.$itemList.find('[data-exchange-reason-item]') );
								param = $itemForm.serialize() +'&'+ $modalForm.serialize();
								param += '&entireReturn=true';

								//신청 다음단계에서 보여줄 상품리스트 clone & append
								var exchangeComplete = Method.$itemList.find('[data-exchange-reason-item]').clone();
								exchangeCompleteList.append(exchangeComplete);
							}

							//다음단계에서 주소 가져오기
							Method.updateSubmitBtn( false );
							Core.Utils.ajax( $modalForm.attr('action'), 'POST', param, function(data){
								var data = sandbox.rtnJson(data.responseText, true);
								var result = data['result'];
								if( result == true ){

									//교환 ga 미사용(미개발) 주석처리
									// if( _GLOBAL.MARKETING_DATA().useGa == true ){
									// 	var marketingOption = {
									// 		orderType : 'RETURN',
									// 		orderId : data.ro.returnOrderId
									// 	};
									// 	Core.ga.processor( marketingOption );
									// }

									//step 01 아이템 정보에서 step 02 노출
									Method.$ex_paymentPopModal.find(".exchange_02").removeClass('hidden');
									Method.$ex_paymentPopModal.find("[exchange-reason-partials-size]").addClass('hidden');
									Method.$ex_paymentPopModal.find("[exchange-partials-quantity]").addClass('hidden');

									//var addFulfillmentchargeFee = data['addFulfillmentChargeFee']; //배송비
									var exchangeReturnOrderId = data['exchangeReturnOrderId'];
									var ex_ReturnOrderIdInput = Method.$ex_paymentPopModal.find('[name="returnOrderId"]');
									ex_ReturnOrderIdInput.val(exchangeReturnOrderId);

									//param으로 전송되는 주소값을 가지고 최종 팝업에 주소입력
									var paramChk = sandbox.utils.getQueryParams(param);
									var nextBeforeAddressWrap = Method.$ex_paymentPopModal.find('.address1 .address-info');
									var nextAfterAddressWrap = Method.$ex_paymentPopModal.find('.address2 .address-info');

									//수거지 주소
									nextBeforeAddressWrap.find('[data-user-name]').text(decodeURIComponent(paramChk.addressFirstName).replace(/[+]/gi, " "));
									nextBeforeAddressWrap.find('[data-postalcode]').text(decodeURIComponent(paramChk.addressPostalCode).replace(/[+]/gi, " "));
									nextBeforeAddressWrap.find('[data-address1]').text(decodeURIComponent(paramChk.addressLine1).replace(/[+]/gi, " "));
									nextBeforeAddressWrap.find('[data-address2]').text(decodeURIComponent(paramChk.addressLine2).replace(/[+]/gi, " "));
									nextBeforeAddressWrap.find('[data-phone]').text(decodeURIComponent(paramChk.addressPhone));
									//Method.$ex_paymentPopModal.find('.address1 .preview').text(nextBeforeAddressWrap.find('li:nth-child(2)').text());
									if(decodeURIComponent(paramChk.personalMessageText) == ''){
										nextBeforeAddressWrap.find('[data-personalMessageText]').hide();
									}else{
										nextBeforeAddressWrap.find('[data-personalMessageText]').text(decodeURIComponent(paramChk.personalMessageText).replace(/[+]/gi, " "));
									}

									//받는주소
									nextAfterAddressWrap.find('[data-user-name]').text(decodeURIComponent(paramChk.newAddressFirstName).replace(/[+]/gi, " "));
									nextAfterAddressWrap.find('[data-postalcode]').text(decodeURIComponent(paramChk.newAddressPostalCode).replace(/[+]/gi, " "));
									nextAfterAddressWrap.find('[data-address1]').text(decodeURIComponent(paramChk.newAddressLine1).replace(/[+]/gi, " "));
									nextAfterAddressWrap.find('[data-address2]').text(decodeURIComponent(paramChk.newAddressLine2).replace(/[+]/gi, " "));
									nextAfterAddressWrap.find('[data-phone]').text(decodeURIComponent(paramChk.newAddressPhone));
									//Method.$ex_paymentPopModal.find('.address2 .preview').text(nextAfterAddressWrap.find('li:nth-child(2)').text());
									if(decodeURIComponent(paramChk.newPersonalMessageText) == ''){
										nextAfterAddressWrap.find('[data-personalMessageText]').hide();
									}else{
										nextAfterAddressWrap.find('[data-personalMessageText]').text(decodeURIComponent(paramChk.newPersonalMessageText).replace(/[+]/gi, " "));
									}

									//UIkit.modal.alert(".").on('hide.uk.modal', function() {
										//window.location.href = sandbox.utils.contextPath + "/account/orders/returned";
										Method.$popModal.hide();
										Method.$ex_paymentPopModal.show();
									//});

								}else{
									UIkit.modal.alert(data['errorMsg']).on('hide.uk.modal', function() {
										window.location.reload();
									});
								}
								//$('.customer-contents').replaceWith($(data.responseText).find('.customer-contents'));

							},true)

						}, function(){},
						{
							labels: {'Ok': '확인', 'Cancel': '취소'}
						});
					}
				} else{
					Method.$itemList.find('select').focus();
				}
			},

			// 부분교환 가능 여부 판단
			getIsAvailablePartialReturn:function(){

				var itemList = {};
				var $checkedList = Method.$itemList.find('[data-exchange-reason-item]').find('input[type="checkbox"]:checked').parents('[data-exchange-reason-item]');

				$checkedList.each( function(){
					isAvailable = false;
					var quantity = $(this).find('input[name="quantity"]').val();
					var skuId = $(this).find('input[name="skuId"]').val();
					var orderId = $(this).find('input[name="orderId"]').val();
					var orderItemId = $(this).find('input[name="orderItemId"]').val();
					var fulfillmentGroupId = $(this).find('input[name="fulfillmentGroupId"]').val();

					if( itemList[orderId] == undefined ){
						itemList[orderId] = {};
						itemList[orderId].items = [];
					}

					var itemObj = {
						skuId : Number(skuId),
						orderItemId : Number(orderItemId),
						quantity : Number(quantity),
						fulfillmentGroupId : Number(fulfillmentGroupId)
					}

					itemList[orderId].items.push( itemObj );
				})

				var isAvailable = false;
				$.each( itemList, function(){
					// 전체 교환이 불가능하거나 현재 주문의 전체 아이템 사이즈와 선택된 아이템 사이즈가 같지 않다면
					// console.log( this.orderItemSize + ' : ' + this.items.length );
					/*
					if( this.ableEntireReturn == "false" || this.orderItemSize != this.items.length ){
						//console.log( '전체 교환이 불가능하거나 현재 주문의 전체 아이템 사이즈와 선택된 아이템 사이즈가 같지 않다면')
						isAvailable = true;
					}else{
						$.each( this.items, function(){
							//console.log( this.quantity + ' : ' + this.quantity);
							if( this.skuId != 0 || this.quantity != this.quantity ){
								//console.log('교환된 수량이 있거나 전체 수량이 아닌것 있음');
								isAvailable = true;
								return;
							}
						})
					}
					*/

					if( isAvailable == false ){
						//console.log('부분교환 불가능한 order가 있음');
						return false;
					}
				})
				return isAvailable;
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-exchangeable]',
					attrName:'data-module-exchangeable',
					moduleName:'module_exchangeable',
					handler:{
						context:this,
						method:Method.moduleInit
					}
				});
			}
		}
	});
})(Core);
