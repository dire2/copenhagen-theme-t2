(function(Core){
	Core.register('module_launchproduct', function(sandbox){
		var currentFirstOptValue = '';
		var currentQuantity = 1;
		var itemAttributes = '';
		var miniOptionIS = false;
		var objProductOption = {};
		var minOffsetTop = 30;
		var maxOffsetTop = 0;
		var args = null;
		var $this;
		var imgCurrentIndex;
		var categoryId = '';
		var productId = '';
		var productDrawId = '';
		var skuId = '';
		var isQuickView = false;
		var isQuantity = true;
		var productOption;
		var quantity;
		var endPoint;
		var privateId;
		var currentSkuData;
		var pickupModal;
		var itemRequest;

		var quantityCheck = function(inventoryType, maxQty){
			var obj = {isQuantity:false, maxQty:0}
			if(inventoryType !== 'UNAVAILABLE'){
				if(inventoryType === 'CHECK_QUANTITY'){
					obj.isQuantity = (maxQty > 0) ? true : false;
					obj.maxQty = maxQty;
				}else if(inventoryType === 'ALWAYS_AVAILABLE'){
					obj.isQuantity = true;
					obj.maxQty = null;
				}
			}else{
				obj.isQuantity = false;
				obj.maxQty = 0;
			}

			return obj;
		}

		var defaultSkuSetup = function(productOptComponents){
			var skuData, quantityState;
			if(!productOptComponents) return;
			if(quantity){
				if(Array.isArray(productOptComponents)){
					$.each(productOptComponents, function(i){
						skuData = this.getDefaultSkuData()[0];
						quantityState = quantityCheck(skuData.inventoryType, skuData.quantity);
						quantity[i].setMaxQuantity(quantityState.maxQty);
						isQuantity = quantityState.isQuantity;
					});
				}else{
					skuData = productOptComponents.getDefaultSkuData()[0];
					quantityState = quantityCheck(skuData.inventoryType, skuData.quantity);
					quantity.setMaxQuantity(quantityState.maxQty);
					isQuantity = quantityState.isQuantity;
				}
			}
		}

		var Method = {
			moduleInit:function(){
				$this = $(this);
				args = arguments[0];
				categoryId = sandbox.utils.getQueryParams(location.href).categoryid;
				productId = args.productId;
				productDrawId = args.productDrawId;
				privateId = args.privateId;
				referCategoryId = args.categoryId;
				endPoint = Core.getComponents('component_endpoint');

				var $dim = $('[data-miniOptionDim]');
				var guideModal = UIkit.modal('#guide', {modal:false});
				var commonModal = UIkit.modal('#common-modal', {modal:false});
				var pickUpModal = UIkit.modal('#pickup-modal', {modal:false});
				var miniCartModule = sandbox.getModule('module_minicart');
				//var pickUpModal = UIkit.modal('#pickup-popup', {modal:false});
				var gallery = sandbox.getComponents('component_gallery', {context:$this});
				var $infoHeightWrap = $('[data-info-height]');
				var snsInputCheck = $(".product-draw-inputs").length > 0;


				var addonProductGroup = {};
				var addonComponents = sandbox.getComponents('component_addon_product_option', {context:$(document)}, function(i){
					var INDEX = i;
					var key = this.getAddonId();

					if(!addonProductGroup.hasOwnProperty(key)){
						addonProductGroup[key] = [];
						INDEX = i;
					}else{
						INDEX++;
					}
					addonProductGroup[key].push(this);

					this.addEvent('addToAddOnItem', function(privateId, $selected){
						for(var i=0; i<addonProductGroup[key].length; i++){
							if(i != INDEX){
								addonProductGroup[key][i].setTrigger($selected.val(), privateId);
							}
						}
					});

					this.addEvent('itemDelete', function(privateId){
						for(var i=0; i<addonProductGroup[key].length; i++){
							if(i != INDEX){
								addonProductGroup[key][i].removeItems();
							}
						}
					});
				});

				quantity = sandbox.getComponents('component_quantity', {context:$(document)}, function(i){
					var INDEX = i;
					this.addEvent('change', function(qty){
						for(var i=0;i<quantity.length;i++){
							if(i !== INDEX){
								quantity[i].setQuantity(qty);
							}
						}
					});
				});


				var currentOptValueId = '';
				productOption = sandbox.getComponents('component_product_option', {context:$(document)}, function(i){ //product Option select components
					var CURRENT_INDEX = i;
					var INDEX = 0;
					var _self = this;
					var key = this.getProductId();


					if(!objProductOption.hasOwnProperty(key)){
						objProductOption[key] = [];
						INDEX = 0;
					}else{
						INDEX++;
					}

					objProductOption[key].push(this);

					this.addEvent('changeFirstOpt', function(firstOptName, optionName, productId, value, valueId, id){
						if(currentOptValueId != valueId){
							currentOptValueId = valueId;

							for(var i=0; i<objProductOption[productId].length; i++){
								if(i != INDEX){
									skuId = '';
									objProductOption[productId][i].setTrigger(optionName, value, valueId);
								}

								if(optionName !== 'COLOR'){
									objProductOption[productId][i].getValidateChk();
								}
							}

							// if(optionName === 'COLOR'){
							// 	gallery.setThumb(value);
							// }
						}

						//console.log( "changeFirstOpt");
						if( _.isFunction(marketingUpdateProductInfo)){
							marketingUpdateProductInfo();
						}
					});

					this.addEvent('skuComplete', function(skuOpt){
						currentSkuData = skuOpt
						if(quantity){
							var quantityState = quantityCheck(skuOpt.inventoryType, skuOpt.maxQty);
							isQuantity = quantityState.isQuantity;
							skuId = skuOpt.id;

							if(args.isDefaultSku !== 'true'){
								if(Array.isArray(quantity)){
									quantity[CURRENT_INDEX].setQuantity(1);
									quantity[CURRENT_INDEX].setMaxQuantity(quantityState.maxQty);
								}else{
									quantity.setQuantity(1);
									quantity.setMaxQuantity(quantityState.maxQty);
								}
							}
						}

						//console.log( "skuComplete");
						if( _.isFunction(marketingUpdateProductInfo)){
							marketingUpdateProductInfo();
						}
					});
				});


				/* isDefaultSku - true  ( option이 없는 경우 )  */
				if(args.isDefaultSku === 'true') defaultSkuSetup(productOption);



				//DRAW 진행사항을 체크하여 상태에 맞는 스크립트 호출
				//응모예정, 응모진행중, 응모종료, 당첨확인, 행사종료
				var drawProgress = $this.find('[data-draw-endprogress]').data('draw-endprogress');
				var redirectUrl = $(location).attr('href');
				var drawurl = sandbox.utils.contextPath + '/draw/entry/isWin';

				// if(drawProgress == '응모진행중'){
				// 	// PDP진입시 참여여부 체크
				// 	BLC.ajax({
				// 		type : "POST",
				// 		dataType : "json",
				// 		url : drawurl,
				// 		data : {
				// 			prodId : productId,
				// 			drawId : productDrawId,
				// 			redirectUrl : redirectUrl
				// 		}
				// 	},function(data){
				// 		if(data.result) {
				// 			if(data.winFlag == "win" || data.winFlag == "lose") {
				// 				$('.product-draw-inputs').remove();
				// 				$('.selector-size').remove();
				// 				$('[data-add-item]').empty().append('<a class="button button-primary flex flex-justify-center flex-align-center"><span class="btn-buy">응모완료</span></a>');
				// 			}
				// 		}
				// 	});
				// }

				$("#drawiswinBtn").click(function(e){
					e.preventDefault();

					if(!Core.getModule('module_header').getIsSignIn()){
						e.preventDefault();
						Core.getModule('module_header').setLogin(function(){
							Core.Loading.show();
							_.delay(function(){
								window.location.reload();
							}, 500);
						});
					}else{
						var drawWinPopUp = UIkit.modal('#draw-win-modal', {modal:false});
						var drawLosePopUp = UIkit.modal('#draw-lose-modal', {modal:false});

						BLC.ajax({
							type : "POST",
							dataType : "json",
							url : drawurl,
							data : {
								prodId : productId,
								drawId : productDrawId,
								redirectUrl : redirectUrl
							}
						},function(data){
							if(data.result) {
								if(data.winFlag == "win") {
									//console.log(data.result);
								    $('#drawBuyForm').attr('productId', data.productId);
								    $('#drawBuyForm').attr('fType', data.fType);
								    $('#drawBuyForm').attr('quantity', data.quantity);
								    $('#drawBuyForm').attr('SIZE', data.SIZE);
									$('#drawBuyForm').attr('COLOR', data.COLOR);
								    $('#drawBuyForm').attr('itemAttributesSize', data.itemAttributesSize);
									$('#drawBuyForm').attr('itemAttributesColor', data.itemAttributesColor);
								    $('#drawBuyForm').attr('attributenameSize', data.attributeNameSize);
									$('#drawBuyForm').attr('attributenameColor', data.attributeNameColor);

									//드로우 용 attributename 추가
									$('#drawBuyForm').attr('draw_itemAttributes', data.drawEntryId);

									//드로우 win modal - 옵션 추가
									$(".drawProductOption").text(data.displayItemAttributesColor + ' / ' + data.displayItemAttributesSize);

									UIkit.modal('#draw-win-modal', {modal:false}).show();
									//$('#draw-win-modal').find('#directOrder').attr('href',data.drawProductUrl);
								}
								else if(data.winFlag == "lose") {
									UIkit.modal('#draw-lose-modal', {modal:false}).show();
								}
								else if(data.winFlag == "notEntry") {
									alert('응모하신 내역이 없습니다.');
									//UIkit.modal('#draw-notentry-modal', {modal:false}).show();
								}
							}
						});
					}


				});



				/* cart Update */
				$('[data-add-item]').each(function(i){
					var INDEX = i;

					//매장픽업 제외 - 장바구니, 바로구매 등
					$(this).find('.btn-link').click(function(e){
						e.preventDefault();
						var _self = $(this);

						// THE DRAW Start
						var actionType = _self.attr('action-type');
						if(actionType === 'drawlogin') {
							Core.getModule('module_header').setLogin(function(){
								Core.Loading.show();
								_.delay(function(){
									window.location.reload();
								}, 500);
							});
						}else if(actionType === 'certified'){
							var certificationYnModal = UIkit.modal('#certification-yn-modal', {center:true});
							var redirectUrl = $(location).attr('href');
							$.cookie("drawCertified", 'draw', {expires: 1, path : '/'});
							$.cookie("drawRedirectUrl", redirectUrl, {expires: 1, path : '/'});
							certificationYnModal.show();
							return;
						}else if(actionType === 'drawentry'){
							// option valid
							var validateChk;
							if(args.isDefaultSku === 'false'){
								validateChk = sandbox.utils.getValidateChk(productOption, '사이즈를 선택해 주세요.');
							}

							// form valid
							var $form = $(this).parents('form');
							sandbox.validation.validate($form);

							if(validateChk && sandbox.validation.isValid($form)){
								// 개인정보 동의 체크
								var agreePolicyVal = $form.find('input[name=privacy_AgreePolicy]:checked').val();
								var agreeUseVal = $form.find('input[name=privacy_AgreeUse]:checked').val();
								var agreeValid = agreePolicyVal == 'agree' && agreeUseVal == 'agree';

								if(snsInputCheck && !agreeValid){
									var errorMsg = (agreePolicyVal !== 'agree' ? '개인정보 수집 및 이용에 동의해주세요. ' : '개인정보 취급 위탁에 동의해주세요.');
									UIkit.modal.alert(errorMsg);
								}else{
									var option =  $this.find('.hidden-option').val();
									if(option != 'undefined' && option != '' ){
										// var drawId = $("[data-drawid]").data("drawid");
										var drawurl = sandbox.utils.contextPath + '/draw/entry';
										var skuId = $this.find(".input-radio.checked").find('input').data("skuid");
										var redirectUrl = $(location).attr('href');
										var drawproductxref = $this.find(".input-radio.checked").find('input').data("drawproductxref");
										var drawskuxref = $this.find(".input-radio.checked").find('input').data("drawskuxref");
										var externalId = $("input[name=drawSnsId]").val();

										UIkit.modal.confirm('드로우에 응모하시겠습니까?', function(){
											BLC.ajax({
												type : "POST",
												dataType : "json",
												url : drawurl,
												data : {
													prodId : productId,
													drawId : productDrawId,
													skuId : skuId,
													externalId : externalId,
													redirectUrl : redirectUrl,
													drawproductxref : drawproductxref,
													drawskuxref : drawskuxref
												}
											},function(data){
												if(data.result == true){
													UIkit.modal.alert("드로우에 응모되었습니다.<br />마이페이지에서 응모 정보를 꼭 확인해주시기 바랍니다.").on('hide.uk.modal', function() {
														window.location.reload();
													});
												}else{
													UIkit.modal.alert(data.errorMessage);
												}
											});
										}, function(){},
										{
											labels: {'Ok': '확인', 'Cancel': '취소'}
										});
									}
								}
							}
						}
						// THE DRAW End

						if(productDrawId == 'null' || productDrawId == '') {
							var addToCartPromise = Method.moduleValidate(INDEX);
							addToCartPromise.then(function(qty){
								var $form = _self.closest('form');
								itemRequest = BLC.serializeObject($form);
								itemRequest['productId'] = productId;
								if(referCategoryId!=null){
									itemRequest['categoryId'] = referCategoryId;
								}
								itemRequest['quantity'] = qty;

								/* 애드온 상품 추가 */
								var $deferred = $.Deferred();
								var addonProductIndex = 0;
								if(addonComponents){
									for(var key in addonProductGroup){
										if(addonProductGroup[key][0].getValidateChk()){
											var childItems = addonProductGroup[key][0].getChildAddToCartItems();
												for(var i=0; i<childItems.length; i++){
														for(var key in childItems[i]){
													itemRequest['childAddToCartItems['+addonProductIndex+'].'+key] = childItems[i][key];
														}
												}
											addonProductIndex++;
										}else{
											$deferred.reject();
										}
									}
									$deferred.resolve(itemRequest);
								}else{
									$deferred.resolve(itemRequest);
								}

								return $deferred.promise();
							}).then(function(itemRequest){
								var $form = _self.closest('form');
								var actionType = _self.attr('action-type');
								var url = _self.attr('href');

								/*****************************************************************
									유입 channel sessionStorage
									- channel : 유입된 매체 식별 이름
									- pid : 상품 식별 code ( productId, style Code, UPC.... )

									사이트 진입시 URL에 channel, pid 가 있을때 매출을 체크 한다.
									channel 만 있을경우에는 모든 상품을 channel 매출로 인지하고
									channel과 pid 둘다 있을경우 해당 상품만 channel 매출로 인지한다.
								*****************************************************************/

								if(sandbox.sessionHistory.getHistory('channel')){
									if(sandbox.sessionHistory.getHistory('pid')){
										if(sandbox.sessionHistory.getHistory('pid') === privateId){
											itemRequest['itemAttributes[channel]'] = sandbox.sessionHistory.getHistory('channel');
										}
									}else{
										itemRequest['itemAttributes[channel]'] = sandbox.sessionHistory.getHistory('channel');
									}
								}

								switch(actionType){
									case 'externalLink' :
										//외부링크
										window.location.href = url;
										break;
									default :
										BLC.ajax({
											url:url,
											type:"POST",
											dataType:"json",
											data:itemRequest
										}, function(data, extraData){
											if(commonModal.active) commonModal.hide();
											if(data.error){
												UIkit.modal.alert(data.error);
											}else{
												var cartData = $.extend( data, {productId : productId, quantity : itemRequest.quantity, skuId : skuId });
												if(actionType === 'add'){
													sandbox.getModule('module_minicart').update( function( callbackData ){
														if( callbackData != null ){
															cartData.cartId = callbackData.cartId
														}
														endPoint.call('addToCart', cartData );
													});
												}else if(actionType === 'modify'){
													var url = Core.Utils.url.removeParamFromURL( Core.Utils.url.getCurrentUrl(), $(this).attr('name') );
													Core.Loading.show();
													endPoint.call( 'cartAddQuantity', cartData );
													_.delay(function(){
														window.location.assign( url );
													}, 500);
												}else if(actionType === 'redirect'){
													Core.Loading.show();
													endPoint.call( 'buyNow', cartData );
													_.delay(function(){
														window.location.assign( sandbox.utils.contextPath + '/checkout' );
													}, 500);
												}
											}
										});
										break;
								}
							}).fail(function(msg){
								if(!$("#common-modal").hasClass('quickview')){
									if(commonModal.active) commonModal.hide();
								}
								if(msg !== '' && msg !== undefined){
									UIkit.notify(msg, {timeout:3000,pos:'top-center',status:'warning'});
								}
							});
						}
					});
				});

			},
			moduleValidate:function(index){
				var INDEX = index;
				var deferred = $.Deferred();
				var validateChk = (args.isDefaultSku === 'true') ? true : false;
				var qty = 0;

				if(args.isDefaultSku === 'false'){
					validateChk = sandbox.utils.getValidateChk(productOption, '사이즈를 선택해 주세요.');
				}

				if(productDrawId == 'null' || productDrawId == '') {
					if(Array.isArray(quantity)){
						qty = quantity[INDEX].getQuantity();
					}else{
						qty = quantity.getQuantity();
					}

					if(validateChk && isQuantity && qty != 0){
						deferred.resolve(qty);
						// $('.mini-option-wrap').find('.info-wrap_product').removeClass('active');
						// $('[data-miniOptionDim]').removeClass('active');
					}else if(!isQuantity || qty == 0){
						deferred.reject(args.errMsg);
					}else{
						deferred.reject();
					}
				}



				return deferred.promise();
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-launchproduct]',
					attrName:'data-module-launchproduct',
					moduleName:'module_launchproduct',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
