(function(Core){
	Core.register('module_guest_order_search', function(sandbox){
		var Method = {
			$that:null,
			$form:null,
			$stepContainer:null,
			$errorAlert:null,
			moduleInit:function(){

				// listSize = 검색 결과 한번에 보여질 리스트 수
				var args = Array.prototype.slice.call(arguments).pop();
				$.extend(Method, args);

				var $this = $(this);
				Method.$that = $this;
				Method.$form = $this.find("form");

				Method.$stepContainer = $this.parents(".account-container");
				Method.$errorAlert = Method.$that.find('[data-error-alert]');
				Method.$search = $this.find('.search-container');

				Core.getComponents('component_textfield', {context:$this}, function(){
					this.addEvent('enter', function(){
						Method.searchSubmit();
					});
				});

				$this.find('button[type="submit"]').on('click', function(e){
					e.preventDefault();
					Method.searchSubmit();
				} );


				// 로그인 버튼
				$this.on('click', '[data-login-btn]',  Method.customerLogin );

				// 인증하기 버튼
				$('.order-list-container.step-2').on('click', '[data-certify-btn]', Method.guestCertify );

				// $('.account-auth-content.step-2').on('click', '[data-back-btn]', function(){
				// 	Method.viewStep(1);
				// });

				sandbox.validation.init( Method.$form );
			},

			updateSelectOrder:function(e){
				e.preventDefault();
				// 자신 버튼 숨기기
				$(this).parent().hide();
				// 자신 컨텐츠 켜기
				$(this).parents('.order-info-item').find('.account-auth-order').addClass('active');
				// 다른 버튼 보이기
				$(this).parents('.order-info-item').siblings('.order-info-item').find('.order-search-confirm').show();
				// 다른 컨텐츠 숨기기
				$(this).parents('.order-info-item').siblings('.order-info-item').find('.account-auth-order').removeClass('active');
			},
			searchSubmit:function(){
				sandbox.validation.validate( Method.$form );
				if( sandbox.validation.isValid( Method.$form )){
					sandbox.utils.ajax(Method.$form.attr("action"), 'POST', Method.$form.serialize(), function(data){
						Method.createGuestOrderList(JSON.parse( data.responseText ));
					});
				}
			},
			viewStep:function(num){
				if(num !== 2){
					Method.$stepContainer.find('.account-auth-content').addClass('uk-hidden');
				}

				Method.$stepContainer.find('.step-' + num).find('input[name="identifier"]').val('');
				Method.$stepContainer.find('.step-' + num).removeClass('uk-hidden');

			},
			showAlert:function(msg){
				UIkit.modal.alert(msg);
				//UIkit.notify(msg, {timeout:3000,pos:'top-center',status:'danger'});
				/*
				Method.$errorAlert.text(msg);
				Method.$errorAlert.removeClass('uk-hidden');
				*/
			},
			hideAlert:function(){
				Method.$errorAlert.addClass('uk-hidden');
			},
			createGuestOrderList:function(data){
				var result = data['result'];
				var $listContainer = $('.order-list-container.step-2');
				var list = data['ro'];
				var html = '';

				if( result == true ){
					if( list.length == 0 ){
						Method.showAlert('검색결과가 없습니다. <br />다른 정보를 이용해 다시 검색해 주십시오.');
						$listContainer.addClass('uk-hidden').find('.list-container').empty();
					}else{
						//orderNumber, phoneNumber, emailAddress, 주문자명(name), 뭐가 들어 올지 모름
						//주문자명의 경우 list에 매칭 값이 넘어 오지 않음.
						//넘어 오는 값 정보는
						//customerId, isGuestCustomer, guestOrderDTOs
						//guestOrderDTOS: orderNumber, submitDate, emailAddress, phoneNumber
						Method.$searchKey = Method.$stepContainer.find('input[name="identifier"]').val();
						var orderNumberPattern = /[0-9]{12,30}$/;  //입력값이 orderNum인 경우 orderNum이 일치 하면 목록에 추가
						var orderNumberSearch = false;
						if(orderNumberPattern.test(Method.$searchKey)){
							orderNumberSearch = true;
						}
						var addList = [];
						var addListItem = {};
						$.each( list, function( index, li ){
							for(var i=0; i<li.guestOrderDTOs.length; i++){
								addListItem = li.guestOrderDTOs[i];
								addListItem.isGuestCustomer = li.isGuestCustomer;
								addListItem.customerId = li.customerId;
								addListItem.isItems = (li.guestOrderDTOs[i].orderItemNames.length > 1);
								addListItem.itemLength = li.guestOrderDTOs[i].orderItemNames.length-1;
								addListItem.orderItemName = li.guestOrderDTOs[i].orderItemNames[0];
								addListItem.totalAmount = sandbox.rtnPrice(li.guestOrderDTOs[i].totalAmount.amount);
								addListItem.isPhoneNum = (li.guestOrderDTOs[i].phoneNumber) ? true : false;
								addList.push(addListItem);
							}

							// for(var i=0; i<li.guestOrderDTOs.length; i++){
							// 	// addListItem.guestOrderDTO = li.guestOrderDTOs[i];
							// 	addListItem.num = i;
							// 	addListItem.orderNumber = li.guestOrderDTOs[i].orderNumber;
							// 	// addListItem.guestOrderDTO.orderItemName = li.guestOrderDTOs[i].orderItemNames[0];
							// 	// addListItem.isItems = (li.guestOrderDTOs.length > 1);
							// 	// addListItem.itemLength = li.guestOrderDTOs.length-1;
							// 	// addListItem.totalAmount = sandbox.rtnPrice(li.guestOrderDTOs[i].totalAmount.amount);
							// 	// addListItem.isPhoneNum = (li.guestOrderDTOs[i].phoneNumber) ? true : false;
							// }
						});
						
						html = Handlebars.compile($("#guest-order-list").html())(addList);

						$listContainer.removeClass('uk-hidden').find('.list-container').html( html );
						$listContainer.find('.order-search-confirm').addClass('active');
						sandbox.moduleEventInjection( html );

						// 검색된 리스트중 선택시
						Method.$stepContainer.on('click', '[data-order-select-btn]',  Method.updateSelectOrder );
						//Method.viewStep(2);
					}
				}else{
					Method.showAlert(data['errorMsg']);
				}
			},
			customerLogin:function(){
				var orderNumber = $(this).closest('li').find('input[name="orderNumber"]').val();

				// 회원 주문
				var modal = UIkit.modal('#common-modal');
				var promise = null;
				promise = sandbox.utils.promise({
					url:sandbox.utils.contextPath + '/dynamicformpage',
					type:'GET',
					data:{'name':'login', 'dataType':'model'}
				}).then(function(data){
					var defer = $.Deferred();
					var appendTxt = $(data).find('.content-area').html();
					$('#common-modal').find('.contents').empty().append(appendTxt);
					sandbox.moduleEventInjection(appendTxt, defer);
					modal.show();
					return defer.promise();
				}).then(function(){
					//window.document.location.href = "/account/orders/" + orderNumber
					window.document.location.href = "/account/orders/";
				}).fail(function(msg){
					UIkit.notify(msg, {timeout:3000,pos:'top-center',status:'danger'});
					modal.hide();
				});
			},

			// 비회원 인증 처리
			guestCertify:function(){
				var type = $(this).attr('data-type');
				var orderNumber = $(this).closest('.order-info-item').find('input[name="orderNumber"]').val();
				var customerId = $(this).closest('.order-info-item').find('input[name="customerId"]').val();
				var email = $(this).closest('.order-info-item').find('input[name="email"]').val();
				var phoneNum = $(this).closest('.order-info-item').find('input[name="phonenum"]').val();
				var url = "/guest/orders/requestAuthUrl?customer=" + customerId;

				if( type === 'email'){
					url += '&orderNumber=' + orderNumber + '&messageType=EMAIL';
				}else if( type === 'sms'){
					url += '&orderNumber=' + orderNumber + '&messageType=KAKAO';
				}

				sandbox.utils.ajax(url, 'GET', {}, function(data){
					var responseData = sandbox.rtnJson(data.responseText);
					if(responseData.result == true){
						Method.$search.hide();
						if(type === 'email'){

							Method.viewStep(3);
							Method.$stepContainer.find('.step-2').addClass('uk-hidden');
						}else if(type === 'sms'){
							Method.viewStep(4);
							Method.$stepContainer.find('.step-2').addClass('uk-hidden');
						}
					}else{
						Method.showAlert(responseData['errorMsg']);
					}

				}, true );

				return;
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-guest-order-search]',
					attrName:'data-module-guest-order-search',
					moduleName:'module_guest_order_search',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
