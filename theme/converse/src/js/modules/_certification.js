(function(Core){
	Core.register('module_certification', function(sandbox){
		var args;
		var Method = {
			moduleInit:function(){
				var $this = $(this);
				var certificationYnModal = UIkit.modal('#certification-yn-modal', {center:true, bgclose:false, keyboard:false});
				//certificationYnModal.show();
				args = arguments[0];

				//이전페이지로 이동
				$this.find('#btn-go-back').on('click', function(){
					certificationYnModal.hide();
				});

				//인증화면으로 이동
				$this.find('#btn-go-certification').click(function(){
    	    window.crPop = window.open('/ipinKmcConfirmation/kmcRequest',"crPop","width=430, height=560, resizable=1, scrollbars=no, status=0, titlebar=0, toolbar=0, left=300, top=200","_blank");
    		});

				window.addEventListener( 'message', function( e ) {
          // e.data가 전달받은 메시지
          if(e.data == "isOverByAge"){
          	window.certificationYnModal.hide();
          }
	    	});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-certification]',
					attrName:'data-module-certification',
					moduleName:'module_certification',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
