(function(Core){
	Core.register('module_search', function(sandbox){
		var $this, args, clickIS, endPoint, latestKeywordList, arrLatestKeywordList = [];

		var setSearchKeyword = function(keyword){
			var pattern = new RegExp(keyword, 'g');
			arrLatestKeywordList = sandbox.utils.rtnMatchComma(latestKeywordList.replace(pattern, ''));
			arrLatestKeywordList.unshift(keyword);

			if(arrLatestKeywordList.length >= args.keywordMaxLen){
				arrLatestKeywordList = arrLatestKeywordList.slice(0, -1);
			}
			$.cookie('latestSearchKeyword', arrLatestKeywordList.join(','), {path:'/'});
		}

		var Method = {
			$shown : 'set-search-shown',
			$toggleClass : '.toggle',
			$wrap : $('#wrapper'),
			$utilClass : '.header-utility',
			$dropDownClass : '.header-search-dropdown',
			$dropDown : null,
			$search : null,
			$searchInput : $('.header-search-input'),
			$searchResult: $(".header-search-results-list"),
			$searchLWrap: $('.header-search-search-top-results'),
			$productListWrap: $('.search-suggestions-items'),
			keywordData : null,
			regExp : /[\{\}\[\]\/?.,;:|\)*~`!^\-+<>@\#$%&\\\=\(\'\"]/gi,
			moduleInit : function(){
				Method.$dropDown = $(Method.$dropDownClass);
				Method.$search = $(Method.$utilClass).find('.search');

				Method.$search.find(Method.$toggleClass).on('click', function(){
					if (!Method.$search.hasClass(Method.$shown)){
						//window.headModule.flyoutHidden();
						Core.getModule('module_gnb').flyoutHidden();
						Method.show();
					} else {
						Method.hide();
					}
				});
				Method.$productListWrap.on('click','.header-search-suggestions-text-cta',function(){
					console.log(123);
					Method.$searchInput.closest('form').trigger('submit');
					return false;
				})
				Method.$searchInput.on("keyup paste", function(){
					var thisVal = $(this).val();
					if(thisVal.length >= 2){
						Method.searchResult(thisVal);
					}else{
						Method.$searchResult.empty();
						Method.$searchLWrap.removeClass('hidden');
						Method.$productListWrap.addClass('hidden');
					}
					
				});
				Method.$searchInput.closest('form').on('submit',function(e){
					var form = $(this),
						action = form.attr('action') + '?',
						data = form.serializeArray();
					data.forEach(function(row){
						Method.regExp.test(row.value) && (row.value = $.trim(row.value.replace(Method.regExp, ' ')));
						if (action) {
							action += row.name + '=';
							action += encodeURIComponent(row.value) + '&';
						}
						if (!$.trim(row.value).length) {
							action = null;
						}
					});
					if (action) {
						location.href = action;
					} else {
						UIkit.notify('검색어를 입력해 주세요.', {timeout:3000,pos:'top-center',status:'danger'});
					}
					setTimeout(function(){
						Core.Loading.hide();
					});
					return false;
				});
				// 오늘 날짜 location 대입
				var today = new Date(),
						yyyy = today.getFullYear(),
						mm = today.getMonth() + 1,
						dd = today.getDate();

				if (dd < 10) dd = '0' + dd
				if (mm < 10) mm = '0' + mm
				today = String(yyyy) + String(mm) + String(dd);

				//연관 검색어 키워드
				sandbox.utils.promise({
					isLoadingBar: false,
					url: sandbox.utils.contextPath + '/cmsstatic/autoComplete/file/autoComplete-'+ today +'.json',//location-'+ today +'.json',
					type: 'GET'
				}).then(function (data) {
					var $defer = $.Deferred();
					if(data !== ''){
						Method.keywordData = JSON.parse(data);
						$defer.resolve(data);
					}else{
						$defer.reject('data is empty');
					}
					return $defer.promise();
				})
      		},
			searchResult : function(thisVal){

				Method.keywordResult(Method.keywordData, thisVal);

				//추천상품
				sandbox.utils.promise({
					isLoadingBar: false,
					url: sandbox.utils.contextPath + '/search/recommend',//location-'+ today +'.json',
					type: 'GET',
					data: {
						'q': thisVal,
					}
				}).then(function (data) {
					var jsonData = data;

					if(jsonData.productList.length !== 0){
						Method.$searchLWrap.addClass('hidden');
						Method.$productListWrap.removeClass('hidden');

						var productListTemplate = Handlebars.compile($("#search-product-list").html())(jsonData);
						$(".product-suggestions").empty().append(productListTemplate);

					}else{
						Method.$searchLWrap.removeClass('hidden');
						Method.$productListWrap.addClass('hidden');
					}

				});

			},
			keywordResult : function (jsonData, thisVal){
				$.cookie('autoCompleteList', 'true');

				var val = Method.regExp.test(thisVal) ? $.trim(thisVal.replace(Method.regExp, ' ')) : thisVal;
				var arrVal = val.split(' ');
				var regVal = new RegExp(arrVal.join('|'),'i');
				var keyword = jsonData.filter(function (row, index) {
					var str = row.name;
					var like = false;
					if (str.search(regVal) >= 0) {
						like = true;
						for(var i=0;i<arrVal.length;i++) {
							if (str.indexOf(arrVal[i]) < 0) {
								like = false;
								break;
							}
						}
					}
					return like;
				});
				var str = '';
				keyword.forEach(function (value, index){
					if(index <= 3) { // 4줄까지 노출
						var replaceValue = keyword[index].name;
						for(var i=0;i<arrVal.length;i++) {
							var replaceTag = '<strong class="match">'+ arrVal[i] +'</strong>';
							replaceValue = replaceValue.replace(arrVal[i], replaceTag);
						}
						str += "<li class='header-search-results-result text-upper text-bold'><a class='hit link-underline' href='"+keyword[index].url+"'>" + replaceValue +"</a></li>";
					}
				});
				Method.$searchResult.html(str);
			},
			show : function(){
				$('body').css('overflow','hidden');

				Method.$wrap.addClass('set-overlay');
				Method.$search.addClass(Method.$shown);
				Method.$dropDown.addClass(Method.$shown);

				if ('m' == Core.getModule('module_gnb').deviceMedia()) {
					var scroll = $(window).scrollTop(),
						offset = $('.header-general').offset().top;
					if (scroll < offset) {
						$('html,body').animate({'scrollTop': $('.header-general').offset().top}, 426);
					}
				} else {
					Method.$wrap.off('click.set-overlay').on('click.set-overlay', function(e){
						if (e.target == e.currentTarget){
							$(this).off('click.set-overlay');
							Method.hide();
						}
					});
				}

				$(document).on('keyup' + Method.$dropDownClass, function(e) {
					if (e.keyCode == 27) {
						Method.hide();
					}
				});
			},
			hide : function(){
				$('body').css('overflow','visible');

				Method.$wrap.removeClass('set-overlay');
				Method.$search.removeClass(Method.$shown);
				Method.$dropDown.removeClass(Method.$shown);

				$(document).off('keyup' + Method.$dropDownClass);
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-search]',
					attrName:'data-module-search',
					moduleName:'module_search',
					handler:{context:this, method:Method.moduleInit}
				});
			},
			searchTrigger:function(){
				$('.gnb-search-btn').trigger('click');
			},
			show:function(){
				Method.show();
			},
			hide:function(){
				Method.hide();
			}
		}
	});
})(Core);
