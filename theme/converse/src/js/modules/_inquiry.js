(function(Core){
	'use strict';

	Core.register('module_inquiry', function(sandbox){
		var $appendListWrap = null;
		var fileLoad = null;
		var fileInfoTemplate = '<input type="hidden" name="fileList[{{id}}].fullUrl" class="file-{{id}}" value="{{fullUrl}}" /><input type="hidden" name="fileList[{{id}}].fileName" class="file-{{id}}" value="{{fileName}}" />';
		var Method = {
			moduleInit:function(){
				setTimeout(function(){
					$('.uk-modal-page').removeClass('uk-modal-page');
				}, 2000);
				var $this = $(this);
				var $fileInfo = $this.find('.file-info');
				var args = arguments[0];
				var subInquiryJson = args['data-sub-inquiry'];
				var subInquiryEmpty = false;
				var arrSelectBox = [];
				var modal = {
					'order' : UIkit.modal('#inquiry-order'),
					'product' : UIkit.modal('#inquiry-product'),
				};
				var isOrderInquery = false;
				var objOrderData = {};
				
				var textComponent = sandbox.getComponents('component_textarea', {context:$this}, function(){
					this.addEvent('focusout', function(){
						var value = $(this).val();
						$this.find('#title').val(value);
						$this.find('#detail').val(value);
						objOrderData.value = value;
						if(isOrderInquery){
							$this.find('#detail').val(Handlebars.compile($('#inquery-order-list').html())(objOrderData));
						}
					});
				});

				var selectComponent = sandbox.getComponents('component_select', {context:$this}, function(i){
					var INDEX = i;
					arrSelectBox.push(this);
					this.addEvent('change', function(val){
						if(INDEX === 0){
							isOrderInquery = false;
							for(var key in subInquiryJson){
								if(key === val){
									var obj = {};
									obj.name = 'subInquiryType';
									obj.option = subInquiryJson[key];
									arrSelectBox[1].replaceSelectBox(Handlebars.compile($(args['data-module-inquiry'].template).html())(obj));
									break;
								}
							}
							if (arrSelectBox[1]) {
								subInquiryEmpty = !obj || $.isEmptyObject(obj.option);
								$(arrSelectBox[1].getThis()).find('select').prop('disabled', subInquiryEmpty);
							}

							/* 세금계산서문의 배송문의, 상품문의, A/S, 반품 취소 문의 일떼 자신이 주문한 상품 (orderItemList) 리스트를 불러온다. */
							if(val === 'BILL' || val === 'DELIVERY' || val === 'PRODUCT' || val === 'AS' || val === 'RETURN' || val === 'CANCEL' || val === 'EXCHANGE' || val == 'ADDRESS'){
								var obj = {
									'mode':'template',
									'resultVar':'orderList',
									'proceedOrderList':''
								}

								switch(val){
									case 'PRODUCT' :
										obj.templatePath = '/account/partials/productItemListInquiry';
										obj.needWishList = 'Y';
										obj.modalType = 'product';
										break;
									default :
										obj.templatePath = '/account/partials/orderItemListInquiry';
										obj.needOrderList = 'Y';
										obj.modalType = 'order';
										break;
								}

								sandbox.utils.ajax('/processor/execute/customer_info', 'GET', obj, function(data){
									if (!$.trim(data.responseText).length) return false;
									modal[obj.modalType].element.find('.customer-product').empty().append(data.responseText);
									UIkit.modal.confirm("상품을 선택하시면 빠른 문의가 가능합니다.<br/>상품을 선택하시겠어요?", function(){
										modal[obj.modalType].show();
									});
								});
							}
						}
					});
				});

				
				fileLoad = sandbox.getComponents('component_file', {context:$this}, function(){
					var _self = this;
					this.addEvent('error', function(msg){
						UIkit.notify(msg, {timeout:3000,pos:'top-center',status:'warning'});
					});

					this.addEvent('upload', function(fileName, fileUrl){
						$fileInfo.empty().append(sandbox.utils.replaceTemplate(fileInfoTemplate, function(pattern){
							$this.find('.file-name').text(fileName);
							switch(pattern){
								case 'id' :
									return 0;
									break;
								case 'fileName' :
									return fileName;
									break;
								case 'fullUrl' :
									return fileUrl;
									break;
							}
						}));
					});
				});

				$('.window-modal').on('change','.i-radio > input',function(){
					$(this).closest('form').find('.button').trigger('click');
				});
				$('.window-modal').on('click','.button',function(){
					var $this = $(this).closest('form').find('.i-radio > input:checked'),
						$modalType = $this.closest('.window-modal').data('modalType');
					isOrderInquery = true;
					objOrderData = $this.data();
					if($this.attr('data-order-type') === 'products'){
						objOrderData.isInquery = false;
					}else if($this.attr('data-order-type') === 'orders'){
						objOrderData.isInquery = true;
					}
					$('.selection-product').empty().append(Handlebars.compile($('#inquery-order-item').html())(objOrderData));
					
					$(textComponent.getThis()).trigger('focusout');
					modal[$modalType].hide();
					return false;
				});

				$('.selection-product').on('click','a.remove',function(){
					$(this).closest('.product-item').remove();
					objOrderData = {};
					isOrderInquery = false;
					$(textComponent.getThis()).trigger('focusout');
				});

				$this.find('.submit-btn').click(function(e){
					if(!selectComponent[0].getValidateChk() || (!subInquiryEmpty && !selectComponent[1].getValidateChk()) || !textComponent.getValidateChk()) {
						e.preventDefault();
					}
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-inquiry]',
					attrName:['data-module-inquiry', 'data-sub-inquiry'],
					moduleName:'module_inquiry',
					handler:{context:this, method:Method.moduleInit}
				});
			},
			moduleConnect:function(){
				fileLoad.setToappUploadImage({
					fileName:arguments[0],
					fullUrl:arguments[1],
					result:(arguments[2] === '1') ? true : false
				});
			}
		}
	});
})(Core);
