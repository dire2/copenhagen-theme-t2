(function(Core){
	Core.register('module_returnorder_history', function(sandbox){
		var Method = {
			moduleInit:function(){
				var args = Array.prototype.slice.call(arguments).pop();
				$.extend(Method, args);

				var $this = $(this);

				$this.find('button.return-cancel-item').on("click", Method.returnOrderCancelItem );
			},

			// 반품 취소 요청
			returnOrderCancelItem:function(e){
				e.preventDefault();
				var _$this = $(this);
				var $form = _$this.closest("form");

				UIkit.modal.confirm('반품 신청을 취소 하시겠습니까?', function(){
					Core.Utils.ajax($form.attr("action"), "POST", $form.serialize(), function(data){
						var data = sandbox.rtnJson(data.responseText, true);
						if( data['result'] == true){
							if( marketingData.useGa == true ){
								var parentOrderNumber = _$this.attr('data-parentorder-id'); //원 주문번호
								var orderSubTotal = _$this.attr('data-ordersubtotal'); //반품신청한 금액
								var _numberCut = parentOrderNumber.substr(parentOrderNumber.length -6,6); //orderId
								var itemList = [];
								var cancelItem = $form.find('[data-return-order-item]');
								$.each( cancelItem, function( index, data ){
									itemList.push({
										id : $(this).find("[data-model]").data("model"),
										name : $(this).find("[data-name]").data("name"),
										price : $(this).find("[data-price]").data("price") * -1,
										quantity : $(this).find("[data-quantity]").data("quantity") * -1
									});
								});

								gtag('event', 'refund', {
									"transaction_id": _numberCut,
									"value": orderSubTotal * -1,
									"items": itemList
								});
							}
						}

						//errorMsg에서 성공 및 실패 문구 노출
						UIkit.modal.alert(data['errorMsg']).on('hide.uk.modal', function() {
							window.location.reload();
						});

					},true)
				}, function(){},
				{
					labels: {'Ok': '확인', 'Cancel': '취소'}
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-returnorder-history]',
					attrName:'data-module-returnorder-history',
					moduleName:'module_returnorder_history',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
