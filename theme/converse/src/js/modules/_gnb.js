(function(Core){
	Core.register('module_gnb', function(sandbox){

		var Method = {
        $moveEnd : 'webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',
        $reszieEvent : 'orientationchange' in window ? 'orientationchange.flyout' : 'resize.flyout',
        $flag : null,
        $active : 'active',
        $activeClass : '',
        $anchorClass : '.anchor.level-1',
        $wrap : $('#wrapper'),
        $nav : $('.header-flyout'),
        $toggle : $('.header-flyout-toggle').find('.toggle'),
        $head : null,
        $flyout : null,
        moduleInit : function(){
          Method.$activeClass = '.' + Method.$active;
          Method.$head = Method.$nav.find('.head-overlay');
          Method.$flyout = Method.$nav.find('.header-flyout-container');

          Method.$nav.on('click', Method.$anchorClass, function(){
            var selector = {}
            selector.anchor = $(this);
            selector.item = selector.anchor.parent(),
            selector.flyout = selector.item.children('.header-flyout-container'),
            selector.siblings = selector.item.siblings(Method.$activeClass);
            selector.wide = selector.flyout.data('flyoutWide');
						if(selector.anchor.hasClass('link')){
							return true;
						}
            if (!selector.anchor.hasClass(Method.$active)) {
              if ('m' != Method.deviceMedia()) {
                Method.PCflyoutVisible(selector);
              } else {
                Method.MCflyoutDetailVisible(selector);
              }
            }
            else {
              Method.PCflyoutHidden();
            }
            return false;
          });
          Method.$toggle.off('click').on('click',function(){
            Method.MCflyoutVisible();
            return false;
          });

          $(window).on('scroll.header-fixit',function(){
            var scrollTop = $(this).scrollTop(),
              header = $('.header-general'),
              headerTop = header.offset().top,
              flagText = 'fixit-active';
            if (scrollTop >= headerTop){
              header.addClass(flagText);
            } else {
              header.removeClass(flagText);
            }
          }).trigger('scroll.header-fixit');
        },
        deviceMedia : function(){
          var flag = $('body').css('zIndex');
          if ('1' == flag) {flag = 'm'}
          if ('2' == flag) {flag = 't'}
          if ('3' == flag) {flag = 'p'}
          return flag;
        },
        PCflyoutVisible : function(selector){
          if (selector.wide) { Method.$flyout.addClass('wide'); }
          else { Method.$flyout.removeClass('wide'); }
          if (!Method.$wrap.hasClass('set-overlay')) { Method.$wrap.addClass('set-overlay'); }

          selector.item.add(Method.$head).add(selector.anchor).add(selector.flyout).addClass(Method.$active);
          selector.siblings.removeClass(Method.$active).find(Method.$activeClass).removeClass(Method.$active);
          Method.$flyout.off(Method.$moveEnd);
          selector.flyout.one(Method.$moveEnd, function(){
            selector.anchor.hasClass(Method.$active) && Method.$nav.addClass(Method.$active);
          });

          Method.$wrap.off('click.set-overlay').on('click.set-overlay', function(e){
            if (e.target == e.currentTarget){
              $(this).off('click.set-overlay');
              Method.PCflyoutHidden();
            }
          });
          $('.header-flyout-close').one('click',function(){
            Method.PCflyoutHidden();
          });
          $(window).on( (Method.$reszieEvent + 'PC') , function(){
            if ( 'm' == Method.deviceMedia() ){
              Method.PCflyoutHidden();
            }
          });
        },
        PCflyoutHidden : function(){
          Method.$wrap.removeClass('set-overlay');
          Method.$nav.removeClass(Method.$active).find(Method.$activeClass).removeClass(Method.$active);
          Method.$flyout.removeClass('wide');

          $(window).off( (Method.$reszieEvent + 'PC') );
        },

        MCflyoutVisible : function(){
          if (!Method.$nav.hasClass('set-flyout-shown')) {
            Method.$nav.addClass('set-flyout-shown');
            $('body').css('overflow','hidden');
          }
          $('.header-flyout-close').one('click',function(){
            Method.MCflyoutHidden();
          });
          $(window).on( (Method.$reszieEvent + 'MC') , function(){
            if ( 'm' != Method.deviceMedia() ){
              Method.MCflyoutHidden();
            }
          });
        },
        MCflyoutHidden : function(){
          if (Method.$nav.hasClass('set-flyout-shown')) {
            Method.$nav.removeClass('set-flyout-shown');
            Method.PCflyoutHidden();
            Method.MCflyoutDetailHidden();

            $('body').css('overflow','visible');
            $(window).off( (Method.$reszieEvent + 'MC') );
          }
        },

        MCflyoutDetailVisible : function(selector){
          var method = this,
            resizeContain = function(flyout){
              flyout.closest('.list.level-1').animate({
                'height' : flyout.height()
              }, 213)
            }
					Method.$head.find('.head-title').text(selector.anchor.text());
          selector.item.add(Method.$head).add(selector.anchor).add(selector.flyout).addClass(Method.$active);
          resizeContain(selector.flyout);

          Method.$nav.find('.head-back').on('click',function(){
            Method.MCflyoutDetailHidden();
          })
          $(window).on( (Method.$reszieEvent + 'MCDetail') , function(){
            resizeContain(selector.flyout);
            if ( 'm' != Method.deviceMedia() ){
              Method.MCflyoutDetailHidden();
            }
          });
        },
        MCflyoutDetailHidden : function(){

          Method.$nav.find('.list.level-1').css({'height':'auto'});
          Method.$nav.removeClass(Method.$active).find(Method.$activeClass).removeClass(Method.$active);

          $(window).off( (Method.$reszieEvent + 'MCDetail') );
        }
      }

			$('.column-family-sites').on('click','.column-item-title',function(){
      if ('m' == Method.deviceMedia()){
        var el = $(this),
          parent = el.closest('.column-family-sites'),
          content = parent.find('.column-links'),
          speed = 250;
        if (!parent.hasClass('active')){
          parent.addClass('way');
          content.slideDown(speed, function(){
            parent.removeClass('way').addClass('active');
            content.removeAttr('style');
          });
        } else {
          parent.removeClass('active').addClass('way');
          content.show().slideUp(speed, function(){
            parent.removeClass('way');
            content.removeAttr('style');
          });

        }
      }
      return false;
    });

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-gnb]',
					attrName:'data-module-gnb',
					moduleName:'module_gnb',
					handler:{context:this, method:Method.moduleInit}
				});
			},
			deviceMedia : function(){
        return Method.deviceMedia();
      },
			flyoutHidden : function(){
        Method.PCflyoutHidden();
        Method.MCflyoutHidden();
      },
			addWish : function (){
        var wish = $('.header-favorites');
        wish.addClass('set-changed');
        setTimeout(function(){
          wish.removeClass('set-changed');
        },426);
      }
		}
	});
})(Core);
