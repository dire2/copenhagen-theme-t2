(function(Core){
	Core.register('module_exchanged_history', function(sandbox){
		var Method = {
			moduleInit:function(){
				var args = Array.prototype.slice.call(arguments).pop();
				$.extend(Method, args);

				var $this = $(this);

				$this.find('button.exchanged-cancel-item').on("click", Method.exchangedCancelItem );
			},

			// 교환 취소 요청
			exchangedCancelItem:function(e){
				e.preventDefault();
				var $form = $(this).closest("form");

				UIkit.modal.confirm('교환을 취소 하시겠습니까?', function(){
					Core.Utils.ajax("/account/orders/exchanged/cancel", "POST", $form.serialize(), function(data){

						UIkit.modal.alert("교환이 취소 되었습니다.").on('hide.uk.modal', function() {
							window.location.reload();
						});

					});
				}, function(){},
				{
					labels: {'Ok': '확인', 'Cancel': '취소'}
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-exchanged-history]',
					attrName:'data-module-exchanged-history',
					moduleName:'module_exchanged_history',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
