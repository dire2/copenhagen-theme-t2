(function(Core){
	Core.register('module_cart', function(sandbox){
		var $this, endPoint;
		var Method = {
			moduleInit:function(){
				// modal layer UIkit 사용
				$this = $(this);
				var modal = UIkit.modal('#common-modal');
				endPoint = Core.getComponents('component_endpoint');

				var addonComponents = sandbox.getComponents('component_addon_option', {context:$this, optionTemplate:'#order-addon-sku-option'}, function(i){
					var _self = this;

					this.addEvent('submit', function(data){
						var $this = $(this);

						UIkit.modal.confirm('장바구니에 상품을 담으시겠습니까?', function(){
							var itemRequest = {};
							var addToCartItems = _self.getChildAddToCartItems();
							var keyName='';

							for(var i=0; i<addToCartItems.length; i++){
								keyName = 'childOrderItems[' + i + ']';
								for(var key in addToCartItems[i]){
									itemRequest['childAddToCartItems['+i+'].'+key] = addToCartItems[i][key];
								}
							}

							//애드온 orderId 알아야함
							itemRequest['addOnOrderId'] = _self.getAddOnOrderId();
							itemRequest['isAddOnOrderProduct'] = true;
							itemRequest['csrfToken'] = $this.closest('form').find('input[name=csrfToken]').val();

							BLC.ajax({
								url:$this.closest('form').attr('action'),
								type:"POST",
								dataType:"json",
								data:itemRequest
							}, function(data){
								if(data.error){
									UIkit.modal.alert(data.error);
								}else{
									location.href = sandbox.utils.url.getCurrentUrl();
								}
							});
						});
					});
				});


				//주문하기
				$(this).on('click', '.btn-order', function(e){
					e.preventDefault();
					var href = $(this).attr('href');
					sandbox.utils.promise({
						url:sandbox.utils.contextPath + '/cart/checkSkuInventory',
						type:'GET'
					}).then(function(data){
						if (data.error) {
							UIkit.modal.alert('품절된 상품을 장바구니에서 삭제 후 주문해주세요.');
						} else {
							endPoint.call('checkoutSubmit');
							if (_GLOBAL.DEVICE.IS_KAKAO_INAPP && !_GLOBAL.CUSTOMER.ISSIGNIN) {
								sandbox.getModule('module_kakao_in_app').submit('/checkout');
							} else {
								if(addonComponents){
									e.preventDefault();
									if(sandbox.utils.getValidateChk(addonComponents)){
										var isAddOnOrderNoChoice = $("input[name='isAddOnOrderNoChoice']").is(":checked");
										var param = "";
			
										if( isAddOnOrderNoChoice  == true ){
											param = "?isAddOnOrderNoChoice=true";
										}
			
										location.href = href + param;
									}
								}else{
									location.href = href;
								}
							}
						}
	
					})
					return false;
				});


				//옵션 변경
				$(this).on('click', '.optchange-btn', function(e){
					e.preventDefault();

					var target = $(this).attr('href');
					var $parent = $(this).closest('.product-opt_cart');
					var id = $parent.find('input[name=productId]').attr('value');
					var quantity = $parent.find('input[name=quantity]').attr('value');
					var url = $parent.find('input[name=producturl]').attr('value');
					var orderItemId = $parent.find('input[name=orderItemId]').attr('value');
					var obj = {'qty':quantity, 'orderitemid':orderItemId, 'quickview':false}

					$parent.find('[data-opt]').each(function(i){
						var opt = sandbox.rtnJson($(this).attr('data-opt'), true);
						for(var key in opt){
							obj[key] = opt[key];
						}
					});

					sandbox.utils.ajax(url, 'GET', obj, function(data){
						var domObject = $(data.responseText).find('#quickview-wrap');
						$(target).find('.contents').empty().append(domObject[0].outerHTML);
						$(target).find('#quickview-wrap').addClass('cart_opt_chg');
						$(target).addClass('cart_opt_chg window-modal modal-quickview');
						sandbox.moduleEventInjection(domObject[0].outerHTML);
						UIkit.modal($(target)).show();
					});
				});

				//옵션수량 변경
				quantity = sandbox.getComponents('component_cart_quantity', {context:$(this)}, function(i){
					var INDEX = i;
					this.addEvent('change', function(qty){
						for(var i=0;i<quantity.length;i++){
							if(i !== INDEX){
								quantity[i].setQuantity(qty);
							}
						}
						//Method.changeQuantity(this, qty);

						var $parent = $(this).parents('.product-opt_cart');
						var id = $parent.find('input[name=productId]').attr('value');
						var orderItemId = $parent.find('input[name=orderItemId]').attr('value');
						var quantityValue = $parent.find('input[name=quantity]').attr('value');
						//var sessionId = $parent.find('input[name=csrfToken]').val();
						var sessionId = $parent.find('input[name=csrfToken]').val();
						var obj = {'productId':id, 'orderItemId':orderItemId ,'quantity':qty, 'csrfToken':sessionId};
						var url = $(this).closest('form').attr('action');
						var method = $(this).closest('form').attr('method');

						$parent.parents('.cart-items').find('.cart-item').each(function(i){
							var parentId = $(this).find('input[name=parentId]').val();
							if(parentId == orderItemId){
								obj['childAddToCartItems[0].productId'] = $(this).find('input[name=productId]').attr('value');
								obj['childAddToCartItems[0].quantity'] = $(this).find('input[name=quantity]').attr('value');
							}
						});

						sandbox.utils.ajax(url, method, obj, function(data){
							var jsonData = sandbox.rtnJson(data.responseText, true);
							var url = sandbox.utils.url.removeParamFromURL( sandbox.utils.url.getCurrentUrl(), $(this).attr('name'));
							if(jsonData['result'] == false){
								//$.cookie('pageMsg', jsonData.error);
								UIkit.modal.alert('구매 가능 수량을 초과하였습니다.').on('hide.uk.modal', function() {
									window.location.assign(url);
								});
							}else{
								window.location.assign(url);
							}
						});
					});
				});


				//나중에 구매하기
				$(this).on('click', '.later-btn', function(e){
					e.preventDefault();

					$.cookie('pageMsg', $(this).attr('data-msg'));
					Method.addItem.call(this, {type:'later'});
				});

				//카트에 추가
				$(this).on('click', '.addcart-btn', function(e){
					e.preventDefault();

					$.cookie('pageMsg', $(this).attr('data-msg'));
					Method.addItem.call(this, {type:'addcart'});
				});

				//카트 삭제
				//$(this).on('click', '.trigger-remove', Method.removeItem );
				//마케팅데이터 전송을 위한 수정
				$(this).on('click', '.trigger-remove',  function(e){
					e.preventDefault();
					var model = $(this).parents("[data-product-item]").find("input[name='productModel']").val();
					var name = $(this).parents("[data-product-item]").find("[data-name]").data("name");
					var category = $(this).parents("[data-product-item]").find("[data-category]").data("category");
					Method.removeItem( $(this).attr('href'), model, name, category);
				});

				//카트 전체삭제
				//$(this).on('click', '.trigger-cart-clear', Method.removeAllItem );
				$(this).on('click', '.trigger-cart-clear',  function(e){
					e.preventDefault();
					var itemList = [];
					var $itemList = $(".cart-items").find("[data-product-item]");
					$.each( $itemList, function( index, data ){
						var $item = $(data);
						itemList.push({
							id : $item.find("[data-model]").data("model"),
							name : $item.find("[data-name]").data("name"),
							category : $item.find("[data-category]").data("category")
						});
					});

					Method.removeAllItem( $(this).attr('href'), itemList);
				});

				//페이지 상태 스크립트
				var pageMsg = $.cookie('pageMsg');
				if(pageMsg && pageMsg !== '' && pageMsg !== 'null'){
					$.cookie('pageMsg', null);
					UIkit.notify(pageMsg, {timeout:3000,pos:'top-center',status:'success'});
				}
			},
			addItem:function(opt){

				var $parent = $(this).closest('.product-opt_cart');
				var id = $parent.find('input[name=productId]').attr('value');
				var orderItemId = $parent.find('input[name=orderItemId]').attr('value');
				var quantity = $parent.find('input[name=quantity]').attr('value');
				var sessionId = $(this).siblings().filter('input[name=csrfToken]').val();
				var obj = {'productId':id, 'orderItemId':orderItemId ,'quantity':quantity, 'csrfToken':sessionId}
				var url = $(this).closest('form').attr('action');
				var method = $(this).closest('form').attr('method');

				$parent.find('[data-opt]').each(function(i){
					var opt = sandbox.rtnJson($(this).attr('data-opt'), true);
					for(var key in opt){
						obj['itemAttributes['+ $(this).attr('data-attribute-name') +']'] = opt[key];
					}
				});

				sandbox.utils.ajax(url, method, obj, function(data){
					var jsonData = sandbox.rtnJson(data.responseText, true) || {};
					var url = sandbox.utils.url.removeParamFromURL( sandbox.utils.url.getCurrentUrl(), $(this).attr('name') );

					if(jsonData.hasOwnProperty('error')){
						$.cookie('pageMsg', jsonData.error);
					}
					window.location.assign(url);
				});
			},
			removeItem:function( url, model, name, category ){
				UIkit.modal.confirm('삭제하시겠습니까?', function(){
					Core.Loading.show();
					var param = sandbox.utils.url.getQueryStringParams( url );

					param.model = model;
					param.name = name;
					param.category = category;
					endPoint.call( 'removeFromCart', param );
					
					_.delay(function(){
						window.location.href = url;
					}, 1000);
				}, function(){},
				{
					labels: {'Ok': '확인', 'Cancel': '취소'}
				});
			},
			removeAllItem:function(url, itemList){
				UIkit.modal.confirm('장바구니를 비우시겠습니까?', function(){
					Core.Loading.show();
					sandbox.utils.promise({
						url:url,
						type:'GET'
					}).then(function(data){
						var param = sandbox.utils.url.getQueryStringParams( url );
						param.itemList = itemList;

						endPoint.call( 'removeAllFromCart', param );
						_.delay(function(){
							window.location.href = '/cart';
						}, 1000);
					}).fail(function(msg){
						sandbox.setLoadingBarState(false);
						UIkit.notify(msg, {timeout:3000,pos:'top-center',status:'danger'});
					});
				}, function(){},
				{
					labels: {'Ok': '확인', 'Cancel': '취소'}
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-cart]',
					attrName:'data-module-cart',
					moduleName:'module_cart',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
